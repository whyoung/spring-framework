package why.demo.spring.utils;

import org.springframework.mock.web.MockHttpServletResponse;

import java.nio.charset.StandardCharsets;

public final class Utils {

    public static void printStackTrace() {
        //线程堆栈，从栈底到栈顶构成的数组
        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        //栈顶栈帧即调用方
        StackTraceElement last = elements[elements.length - 1];
//        String className = last.getClassName();   //类
//        String methodName = last.getMethodName(); //方法
//        int lineNumber=last.getLineNumber();      //代码行
        StringBuilder sb = new StringBuilder();
        for (int i = elements.length - 1; i>= 2; i--) {
            StackTraceElement e = elements[i];
            System.out.println(sb + e.getClassName() + "#" + e.getMethodName());
            sb.append("\t");
        }
    }

    public static void print(MockHttpServletResponse response) {
        int status = response.getStatus();
        System.out.println("status: " + status);
        for (String name : response.getHeaderNames()) {
            System.out.println(name + " = " + response.getHeader(name));
        }
        byte[] bytes = response.getContentAsByteArray();
        if (bytes.length > 0) {
            System.out.println("content: \n" + new String(bytes, StandardCharsets.UTF_8));
        }
    }
}
