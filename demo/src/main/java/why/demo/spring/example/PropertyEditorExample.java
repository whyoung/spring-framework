package why.demo.spring.example;


import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.beans.PropertyEditorRegistrySupport;

import java.beans.PropertyEditor;
import java.beans.PropertyEditorManager;
import java.beans.PropertyEditorSupport;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PropertyEditorExample {

    public static void main(String[] args) {
        PropertyEditor editor = PropertyEditorManager.findEditor(int.class);
        editor.setValue(new Date());
        System.out.println(editor.getValue());

        PropertyEditorRegistry registry = new PropertyEditorRegistrySupport();
        registry.registerCustomEditor(int.class, new IntegerEditor());
        registry.registerCustomEditor(Date.class, new DateEditor());

        PropertyEditor editor1 = registry.findCustomEditor(int.class, "");
        if (editor1 != null) {
            editor1.setValue("1234");
            System.out.println(editor1.getValue());
        }

        editor1 = registry.findCustomEditor(Date.class, "");
        if (editor1 != null) {
            editor1.setValue("2012-12-24");
            System.out.println(editor1.getValue());
        }
    }

    static abstract class NumberEditor extends PropertyEditorSupport {
        public String getJavaInitializationString() {
            Object var1 = this.getValue();
            return var1 != null ? var1.toString() : "null";
        }
    }

    static class IntegerEditor extends PropertyEditorSupport  {
        public void setAsText(String var1) throws IllegalArgumentException {
            this.setValue(var1 == null ? null : Integer.decode(var1));
        }
    }

    static class DateEditor extends NumberEditor {
        public void setAsText(String var1) throws IllegalArgumentException {
            try {
                this.setValue(var1 == null ? null : new SimpleDateFormat("yyyy-MM-dd").parse(var1));
            } catch (ParseException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }
}
