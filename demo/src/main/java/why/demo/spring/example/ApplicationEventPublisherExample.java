package why.demo.spring.example;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.DefaultEventListenerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.context.event.EventListenerMethodProcessor;
import org.springframework.context.event.SmartApplicationListener;
import org.springframework.context.support.GenericApplicationContext;

public class ApplicationEventPublisherExample {

    public static void main(String[] args) throws InterruptedException {
        GenericApplicationContext context = new
                GenericApplicationContext();
        context.registerBean(DefaultEventListenerFactory.class);
        context.registerBean(EventListenerMethodProcessor.class);
        context.registerBean(EventListener1.class);
        context.registerBean(EventListener2.class);
        context.refresh();
        context.publishEvent(new MyEvent(context));
    }

    static class MyEvent extends ApplicationEvent {
        private static final long serialVersionUID = 7099057708183571937L;
        public MyEvent(Object source) {
            super(source);
        }
    }

    static class EventListener1 {

        @EventListener
        public void listen(MyEvent e) {
            System.out.println("listen1: " + e);
        }
    }

    static class EventListener2 implements SmartApplicationListener {

        @Override
        public void onApplicationEvent(ApplicationEvent event) {
            System.out.println("listen2: " + event);
        }

        @Override
        public boolean supportsEventType(Class<? extends ApplicationEvent> eventType) {
            return eventType == MyEvent.class;
        }
    }
}
