package why.demo.spring.example;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ConfigurationClassPostProcessor;
import org.springframework.context.support.GenericApplicationContext;

import java.lang.reflect.Method;

public class BeanFactoryPostProcessorExample {

    public static void main(String[] args) {
//        DefaultListableBeanFactory beanFactory =
//                new DefaultListableBeanFactory();
//        BeanDefinition bd = BeanDefinitionBuilder
//                .genericBeanDefinition(AppConfig.class)
//                .getBeanDefinition();
//        beanFactory.registerBeanDefinition("appConfig", bd);
//
//        for (String name : beanFactory.getBeanDefinitionNames()) {
//            System.out.println(name);
//        }

        GenericApplicationContext context
                = new GenericApplicationContext();
        context.registerBean("appConfig", AppConfig.class);
        context.registerBean(ConfigurationClassPostProcessor.class);
//        context.getBeanFactory().registerSingleton("appBeanDefinitionRegistryPostProcessor", new BeanDefinitionRegistryPostProcessor() {
//            @Override
//            public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
//                BeanDefinition bd = registry.getBeanDefinition("appConfig");
//                String clazzName = bd.getBeanClassName();
//                try {
//                    Class<?> clazz = Class.forName(clazzName);
//                    Configuration an = clazz.getAnnotation(Configuration.class);
//                    if (an != null) {
//                        Method[] methods = clazz.getDeclaredMethods();
//                        for (Method m : methods) {
//                            Bean bean = m.getAnnotation(Bean.class);
//                            if (bean != null && m.getReturnType() != Void.class) {
//                                String beanName = m.getName();
//                                if (bean.name().length > 0) {
//                                    beanName = bean.name()[0];
//                                }
//                                BeanDefinition beanDefinition = BeanDefinitionBuilder
//                                        .genericBeanDefinition(m.getReturnType())
//                                        .getBeanDefinition();
//                                registry.registerBeanDefinition(beanName, beanDefinition);
//                            }
//                        }
//                    }
//                } catch (Exception ignore){
//                }
//            }
//
//            @Override
//            public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
//
//            }
//        });


        context.refresh();
        for (String name : context.getBeanDefinitionNames()) {
            BeanDefinition bd = context.getBeanDefinition(name);
            System.out.println(name + "=" + bd.getClass().getSimpleName());
        }
    }

    @Configuration
    static class AppConfig {
        @Bean
        public MyBean myBean() {
            return new MyBean();
        }
    }

    static class MyBean {
    }
}
