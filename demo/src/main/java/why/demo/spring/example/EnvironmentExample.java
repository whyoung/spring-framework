package why.demo.spring.example;

import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.core.env.StandardEnvironment;

public class EnvironmentExample {

    public static void main(String[] args) {
        GenericApplicationContext context =
                new GenericApplicationContext();
        context.refresh();
        Environment env = context.getEnvironment();
        System.out.println(env.getProperty("JAVA_HOME"));
    }
}
