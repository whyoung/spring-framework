package why.demo.spring.example;

import org.springframework.format.Formatter;
import org.springframework.format.support.FormatterPropertyEditorAdapter;

import java.text.ParseException;
import java.util.Locale;

public class FormatterExample {

    public static void main(String[] args) throws Exception {
        Formatter<Integer> formatter = new Formatter<Integer>() {
            @Override
            public Integer parse(String text, Locale locale) throws ParseException {
                return Integer.parseInt(text);
            }

            @Override
            public String print(Integer object, Locale locale) {
                return String.valueOf(object);
            }
        };


//        System.out.println(formatter.parse("123", Locale.CHINA));

        FormatterPropertyEditorAdapter adapter = new FormatterPropertyEditorAdapter(formatter);
        adapter.setAsText("1222");
        System.out.println(adapter.getAsText());
    }
}
