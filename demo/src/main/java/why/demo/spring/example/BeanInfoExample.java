package why.demo.spring.example;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;

public class BeanInfoExample {

    public static void main(String[] args) throws IntrospectionException {
        BeanInfo info = Introspector.getBeanInfo(TestBean.class);
        System.out.println(info);
        for (PropertyDescriptor descriptor : info.getPropertyDescriptors()) {
            System.out.println(descriptor.getName());
            Method writeMethod = descriptor.getWriteMethod();
            if (writeMethod != null) {
                System.out.println(writeMethod.getName());
            }
        }
    }

    static class TestBean {
        private String name;
        private int id;

        public String getName() {
            return name;
        }

//        public void setName(String name) {
//            this.name = name;
//        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}
