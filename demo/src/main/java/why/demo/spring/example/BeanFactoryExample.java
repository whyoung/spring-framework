package why.demo.spring.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.annotation.AnnotationConfigUtils;
import org.springframework.context.support.GenericApplicationContext;

import java.beans.PropertyDescriptor;
import java.util.Date;

public class BeanFactoryExample {

    public static void main(String[] args) {
        DefaultListableBeanFactory beanFactory =
                new DefaultListableBeanFactory();
        AbstractBeanDefinition bd1 = BeanDefinitionBuilder.genericBeanDefinition(Date.class)
                .getBeanDefinition();
        beanFactory.registerBeanDefinition("date", bd1);
        if (beanFactory.containsBean("date")) {
            System.out.println(beanFactory.getBean("date"));
        }

        AbstractBeanDefinition bd2 = BeanDefinitionBuilder.genericBeanDefinition(MyBean.class)
                .setAutowireMode(AbstractBeanDefinition.AUTOWIRE_BY_NAME)
                //需要自己维护bean之间的依赖关系
//                .addPropertyValue("date", new Date())
                .addPropertyReference("date", "date")
                .getBeanDefinition();



        beanFactory.registerBeanDefinition("myBean", bd2);
        MyBean myBean = (MyBean) beanFactory.getBean("myBean");
        System.out.println(myBean);
        System.out.println(beanFactory.getBeanProvider(MyBean.class));

        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean("date", Date.class, () -> new Date());
        context.registerBean(AutowiredAnnotationBeanPostProcessor.class);
        context.registerBean("myBean", MyBean.class);
        context.refresh();
        BeanDefinition bd = context.getBeanDefinition("myBean");
        System.out.println(context.getBean("myBean"));
    }

    static class MyBean {

        public MyBean() {
        }
        public MyBean(Date date) {
            this.date = date;
        }

        @Autowired
        private Date date;

        public void setDate(Date date) {
            this.date = date;
        }

        @Override
        public String toString() {
            return "MyBean{" + "date=" + date + '}';
        }
    }
}
