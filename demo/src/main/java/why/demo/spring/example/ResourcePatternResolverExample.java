package why.demo.spring.example;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.util.Arrays;

public class ResourcePatternResolverExample {

    public static void main(String[] args) throws IOException {
        GenericApplicationContext context =
                new GenericApplicationContext();
        context.refresh();
        Resource[] resources = context.getResources("classpath:WEB-INF/spring.factories");
        System.out.println(resources.length);

        resources = context.getResources("classpath*:WEB-INF/spring.factories");
        System.out.println(resources.length);
    }

    @Configuration
    static class Config {

    }
}
