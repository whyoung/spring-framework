package why.demo.spring.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigUtils;
import org.springframework.context.annotation.CommonAnnotationBeanPostProcessor;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.GenericApplicationContext;

import java.util.Date;

public class BeanDefinitionExample {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("bean-definition-example.xml");
//        AbstractBeanDefinition bd = (AbstractBeanDefinition) context.getBeanFactory().getBeanDefinition("appConfig");
//        System.out.println(bd.getPropertyValues());
//        System.out.println(context.getBean("appConfig"));
//        GenericApplicationContext context =
//                new GenericApplicationContext();
//        context.registerBean(AutowiredAnnotationBeanPostProcessor.class);
//        context.getBeanFactory().registerSingleton("date2", new Date());
//        context.getBeanFactory().registerSingleton("date1", new Date());
//        AnnotationConfigUtils.registerAnnotationConfigProcessors(context);
//        context.registerBean("date1", Date.class);
//        context.registerBean("date2", Date.class);
//        context.registerBean("appConfig", AppConfig.class);
//        AbstractBeanDefinition bd = (AbstractBeanDefinition) context.getBeanFactory().getBeanDefinition("appConfig");
//        bd.setAutowireMode(AbstractBeanDefinition.AUTOWIRE_NO);

//        context.refresh();
        System.out.println(context.getBean("appConfig"));
    }

    public static class AppConfig {

        private Date date;

        public void setDate(Date date) {
            this.date = date;
        }

        @Override
        public String toString() {
            return "AppConfig{" +
                    "date=" + date +
                    '}';
        }
    }
}
