package why.demo.spring.example;

import org.springframework.core.ResolvableType;

public class ResolvableTypeExample {

    public static void main(String[] args) {
        ResolvableType type = ResolvableType.forClass(Clazz.class);
        System.out.println(type.getSuperType());
        System.out.println(type.getGeneric(100));
    }

    interface Iface<T> {
    }

    static class Clazz implements Iface<String> {
    }
}
