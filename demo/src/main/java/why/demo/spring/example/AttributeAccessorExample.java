package why.demo.spring.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Date;

public class AttributeAccessorExample {

    public static void main(String[] args) {
//        AnnotationConfigApplicationContext context =
//                new AnnotationConfigApplicationContext(MyConfig.class);
//        BeanDefinition bd = context.getBeanDefinition("myBean");
//        for (String name : bd.attributeNames()) {
//            System.out.println(name + "=" + bd.getAttribute(name));
//        }
//        System.out.println(bd.getSource());
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        AbstractBeanDefinition bd =
                BeanDefinitionBuilder.genericBeanDefinition(MyBean.class).getBeanDefinition();
        bd.setAbstract(true);
        beanFactory.registerBeanDefinition("myBean", bd);
        for (String name : bd.attributeNames()) {
            System.out.println(name + "=" + bd.getAttribute(name));
        }
//        accessor.setAttribute("name", "hello");

//        System.out.println(bd.getAttribute("name"));
//        System.out.println(bd.getAttribute("create"));
    }

    @Configuration
    static class MyConfig {
        @Bean
        public Date now() {
            return new Date();
        }

        @Bean("myBean")
        public MyBean myBean() {
            return new MyBean();
        }
    }
    static class MyBean {
        private String name;
        @Autowired
        private Date create;
    }
}
