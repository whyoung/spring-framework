package why.demo.spring.example;

import java.beans.BeanDescriptor;
import java.beans.BeanInfo;
import java.beans.EventSetDescriptor;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.MethodDescriptor;
import java.beans.PropertyDescriptor;

public class JavaBeanExample {

    public static void main(String[] args) throws IntrospectionException {
        BeanDescriptor beanDescriptor = new BeanDescriptor(TestJavaBean.class);
        System.out.println(beanDescriptor.getBeanClass());
        System.out.println(beanDescriptor.getName());

        BeanInfo beanInfo = Introspector.getBeanInfo(TestJavaBean.class);
        for (PropertyDescriptor descriptor : beanInfo.getPropertyDescriptors()) {
            System.out.println(descriptor.getName());
        }
        for (MethodDescriptor descriptor : beanInfo.getMethodDescriptors()) {
            System.out.println(descriptor.getName());
        }
        if (beanInfo.getEventSetDescriptors() != null) {
            for (EventSetDescriptor descriptor : beanInfo.getEventSetDescriptors()) {
                System.out.println(descriptor.getName());
            }
        }
        System.out.println();
    }

    static class TestJavaBean {
        private int id;
        private String name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
