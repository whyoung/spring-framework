package why.demo.spring.factory.transaction;

import org.springframework.jdbc.core.JdbcTemplate;

public class TestDaoImpl implements TestDao {

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void save() {
        jdbcTemplate.update("insert into test(id) values (?)", 1);
    }
}
