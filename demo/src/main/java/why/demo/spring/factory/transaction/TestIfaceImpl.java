package why.demo.spring.factory.transaction;

public class TestIfaceImpl implements TestIface {

    private TestDao testDao;

    public void setTestDao(TestDao testDao) {
        this.testDao = testDao;
    }

    @Override
    public void save() {
        testDao.save();
    }
}
