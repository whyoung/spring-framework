package why.demo.spring.factory;

import org.springframework.beans.factory.support.BeanDefinitionReader;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import why.demo.spring.factory.transaction.TestIface;

public class TransactionTest {

    public static void main(String[] args) {

        Resource resource = new ClassPathResource("applicationContext3.xml");
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        BeanDefinitionReader beanDefinitionReader = new XmlBeanDefinitionReader(beanFactory);
        beanDefinitionReader.loadBeanDefinitions(resource);

        TestIface iface = (TestIface) beanFactory.getBean("testService");
        iface.save();
    }
}
