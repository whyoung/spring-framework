package why.demo.spring.aop.test;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.aop.aspectj.annotation.AnnotationAwareAspectJAutoProxyCreator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ConfigurationClassPostProcessor;
import org.springframework.context.support.GenericApplicationContext;

public class AopTest17 {

    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        {
            context.registerBean(ConfigurationClassPostProcessor.class);
            context.registerBean(AnnotationAwareAspectJAutoProxyCreator.class);
            context.registerBean(Config.class);
        }

        context.refresh();

        {
            Bean1 bean1 = context.getBean(Bean1.class);
            bean1.m();

            Bean2 bean2 = context.getBean(Bean2.class);
            bean2.m();
        }
    }

    @Configuration
    static class Config {
        @Bean
        public Bean1 bean1() {
            return new Bean1();
        }

        @Bean
        public Bean2 bean2() {
            return new Bean2();
        }

        @Bean
        public MyAspect myAspect() {
            return new MyAspect();
        }
    }

    @Aspect
    static class MyAspect {

        @Before("execution(* why.demo.spring.aop.test.AopTest17.Bean1.*())")
        public void before() {
            System.out.println("before...");
        }
    }

    static class Bean1 {

        public Bean1() {
            System.out.println("bean1 constructor");
        }
        public void m() {
            System.out.println("bean1 method");
        }
    }

    static class Bean2 {
        public void m() {
            System.out.println("bean2 method");
        }
    }
}
