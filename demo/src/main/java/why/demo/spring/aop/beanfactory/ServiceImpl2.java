package why.demo.spring.aop.beanfactory;

import org.springframework.stereotype.Service;

/**
 * @author why
 * @date 2019-03-26 09:43
 */
@Service
public class ServiceImpl2 {

    public void func() {
        System.out.println("service impl2 func");
    }
}
