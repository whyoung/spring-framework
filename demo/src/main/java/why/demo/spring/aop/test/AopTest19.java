package why.demo.spring.aop.test;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ConfigurationClassPostProcessor;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.io.IOException;

public class AopTest19 {

    /*
    申明式事务：
    入口必须是加注解，至于内部的方法，是可以不加注解的，事务的后续的连接都是从ThreadLocal中获取的
     */
    public static void main(String[] args) throws Exception {
        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean(ConfigurationClassPostProcessor.class);
        context.registerBean(AutowiredAnnotationBeanPostProcessor.class);
        context.registerBean(Config.class);
        context.refresh();

        Bean1 bean1 = context.getBean(Bean1.class);
        bean1.test1();
//        bean1.test2();
//        System.in.read();
    }

    @Configuration
    @EnableTransactionManagement
//    @Import(TransactionManagementConfigurationSelector.class)
    static class Config {

        //事务管理器
        @Bean
        public PlatformTransactionManager platformTransactionManager(DataSource dataSource) {
            return new DataSourceTransactionManager(dataSource);
        }
        //数据库连接池
        @Bean
        public DataSource dataSource() {
            DruidDataSource datasource = new DruidDataSource();
            datasource.setUrl("jdbc:mysql://wsl-ubuntu:3306/test_lock");
            datasource.setUsername("root");
            datasource.setPassword("123456");
            datasource.setDriverClassName("com.mysql.jdbc.Driver");
            return datasource;
        }

        @Bean
        public Bean1 bean1() {
            return new Bean1();
        }

        @Bean
        public Bean2 bean2() {
            return new Bean2();
        }

        @Bean
        public JdbcTemplate jdbcTemplate(DataSource dataSource) {
            return new JdbcTemplate(dataSource);
        }
    }

    static class Bean1 {
        @Autowired
        private JdbcTemplate jdbcTemplate;

        @Autowired
        private Iface bean2;

        @Transactional
        public void test1() {
            jdbcTemplate.execute("insert into test1(`guid`, `name`) values ('1', '1233')");
            bean2.test2();
        }

        public void test2() {
            jdbcTemplate.execute("insert into test1(`guid`, `name`) values ('1', '1233')");
            bean2.test2();
        }
    }

    interface Iface {
//        @Transactional
        void test2();
    }
    static class Bean2 implements Iface {

        @Autowired
        private JdbcTemplate jdbcTemplate;

        @Override
        public void test2() {
            jdbcTemplate.execute("insert into test1(`guid`, `name`) values ('1', '22222')");
            test22();
        }

        @Transactional
        private void test22() {
            jdbcTemplate.execute("insert into test1(`guid`, `name`) values ('1', '2')");
        }
    }
}
