package why.demo.spring.aop.test;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.aop.aspectj.annotation.AnnotationAwareAspectJAutoProxyCreator;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

public class AopTest9 {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.registerBean(Config.class);
        //用于处理aspect注解的bean post processor
//        context.registerBean(AnnotationAwareAspectJAutoProxyCreator.class);
        context.refresh();

        for (String name : context.getBeanDefinitionNames()) {
            System.out.println(name);
        }
        Bean1 bean1 = context.getBean(Bean1.class);
        System.out.println(bean1.getClass());
        bean1.m();
    }


    @Configuration
    static class Config {

        @Bean
        public MyAspect myAspect() {
            return new MyAspect();
        }

        @Bean
        public Bean1 bean1() {
            return new Bean1();
        }
    }
    @Aspect
    static class MyAspect {

        @Before("execution(* m())")
        public void before() {
            System.out.println("before...");
        }
    }

    static class Bean1 {
        public void m() {
            System.out.println("run...");
        }
    }
}
