package why.demo.spring.aop.beanfactory;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * todo desc
 * @author why
 * @date 2019-03-26 09:55
 */
@Configuration
@ComponentScan("why.spring.aop.beanfactory")
@EnableAspectJAutoProxy(exposeProxy = true)
public class AopConfig {
}
