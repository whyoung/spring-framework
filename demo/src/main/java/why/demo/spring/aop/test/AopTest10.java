package why.demo.spring.aop.test;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.Advisor;
import org.springframework.aop.aspectj.AspectJExpressionPointcut;
import org.springframework.aop.framework.AopContext;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.DefaultPointcutAdvisor;

import java.lang.reflect.Method;

public class AopTest10 {

    public static void main(String[] args) throws Exception {
        AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
        pointcut.setExpression("execution(* *())");

        Method m = Test1.class.getMethod("m");
        System.out.println(pointcut.matches(m, Iface.class));

        Method n = Test1.class.getMethod("n", Object.class);
        System.out.println(pointcut.matches(n, Test1.class));

        System.out.println(pointcut.matches(Test1.class));
        System.out.println(pointcut.matches(Iface.class));

        System.out.println(pointcut.matches(Test1.class.getDeclaredMethod("m1"), Test1.class));
        System.out.println(pointcut.matches(Test1.class.getDeclaredMethod("m2"), Test1.class));

        Advisor advisor = new DefaultPointcutAdvisor(pointcut, new MethodInterceptor() {
            @Override
            public Object invoke(MethodInvocation invocation) throws Throwable {
                System.out.println("before..." + invocation.getMethod().getName());
                return invocation.proceed();
            }
        });
        Test1 target = new Test1();
        ProxyFactory factory = new ProxyFactory();
        factory.setExposeProxy(true);  //AopContext.currentProxy() 能获取到当前类的目标类
        factory.setProxyTargetClass(true);
        factory.setInterfaces(AopTest8.Iface.class);  //必须手动指定接口
        factory.setTarget(target);
        factory.addAdvisor(advisor);

        System.out.println(target);
        Iface proxy = (Iface) factory.getProxy();
        proxy.m();
        proxy.n(new Object());
        if (proxy instanceof Test1) {
            Test1 test1 = (Test1) proxy;
            test1.m2();
            Test1.m1();
        }
    }

    static interface Iface {
        void m();

        default Object x() {
            return AopContext.currentProxy();
        }

        default Object n(Object x) {
            return x;
        }
    }

    static class Test1 implements Iface {
        @Override
        public void m() {
            System.out.println("run test1#m...");
            System.out.println(AopContext.currentProxy());
        }

        public static void m1() {
            System.out.println("test1 ");
        }

        private void m2() {
            System.out.println("Test1#m2");
        }
    }
}
