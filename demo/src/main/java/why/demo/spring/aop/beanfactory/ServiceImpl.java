package why.demo.spring.aop.beanfactory;

import org.springframework.stereotype.Service;

/**
 * todo desc
 * @author why
 * @date 2019-03-26 09:42
 */
@Service
public class ServiceImpl implements ServiceIface {

    @Override
    public void func() {
        System.out.println("func");
    }
}
