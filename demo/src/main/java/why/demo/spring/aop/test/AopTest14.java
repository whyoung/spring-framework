package why.demo.spring.aop.test;

import org.aopalliance.intercept.MethodInvocation;
import org.aspectj.lang.annotation.*;
import org.springframework.aop.Advisor;
import org.springframework.aop.aspectj.*;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.framework.ReflectiveMethodInvocation;
import org.springframework.aop.interceptor.ExposeInvocationInterceptor;
import org.springframework.aop.support.DefaultPointcutAdvisor;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.List;

public class AopTest14 {

    /*
    MethodInvocation
     */
    public static void main(String[] args) throws Throwable {

        AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
        pointcut.setExpression("execution(* why.demo.spring.aop.test.AopTest14.Target.*())");
        AspectInstanceFactory instanceFactory = new SingletonAspectInstanceFactory(new MyAspect());
        Advisor advisor1 = new DefaultPointcutAdvisor(pointcut,
                new AspectJMethodBeforeAdvice(MyAspect.class.getDeclaredMethod("before"), pointcut, instanceFactory));

        Advisor advisor2 = new DefaultPointcutAdvisor(pointcut,
                new AspectJAfterAdvice(MyAspect.class.getDeclaredMethod("after"), pointcut, instanceFactory));

        AspectJExpressionPointcut pointcut2 = new AspectJExpressionPointcut();
//        pointcut2.setExpression("execution(* *())");
        pointcut2.setExpression("execution(* x())");
        Advisor advisor3 = new DefaultPointcutAdvisor(pointcut2,
                new AspectJMethodBeforeAdvice(MyAspect.class.getDeclaredMethod("beforeX"), pointcut2, instanceFactory));

        Target target = new Target();
        Method m = Target.class.getDeclaredMethod("m");
        ProxyFactory factory = new ProxyFactory();
        factory.setTarget(target);
        factory.addAdvice(ExposeInvocationInterceptor.INSTANCE); //将invocation对象加入到ThreadLocal中，供后续的通知使用Invocation对象
        factory.addAdvisors(advisor1, advisor2, advisor3);
        List<Object> objects =  factory.getInterceptorsAndDynamicInterceptionAdvice(m, Target.class);
        for (Object obj : objects) {
            System.out.println(obj);
        }

        Constructor<ReflectiveMethodInvocation> constructor = ReflectiveMethodInvocation.class
                .getDeclaredConstructor(Object.class, Object.class, Method.class, Object[].class, Class.class, List.class);
        constructor.setAccessible(true);
        MethodInvocation invocation = constructor.newInstance(factory.getProxy(), target, m, new Object[0], Target.class, objects);
        invocation.proceed();
    }

    static class Target {

        public void m() {
            System.out.println("invoke method m");
        }
    }

    @Aspect
    static class MyAspect {

        @Before("execution(* *())")
        public void before() {
            System.out.println("before...");
        }

        @After("execution(* *())")
        public void after() {
            System.out.println("after...");
        }

        @Before("execution(* x())")
        public void beforeX() {
            System.out.println("beforeX...");
        }
    }
}
