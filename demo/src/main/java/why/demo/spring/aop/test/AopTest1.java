package why.demo.spring.aop.test;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class AopTest1 {

    public static void main(String[] args) {
        Impl target = new Impl();
        Iface iface = (Iface) Proxy.newProxyInstance(AopTest1.class.getClassLoader(), new Class[]{Iface.class}, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                System.out.println("before...");
                Object result = method.invoke(target, args);
                System.out.println("after...");
                return result;
            }
        });
        iface.m();
    }

    interface Iface {
        void m();
    }

    static class Impl implements Iface {

        @Override
        public void m() {
            System.out.println("impl method");
        }
    }
}
