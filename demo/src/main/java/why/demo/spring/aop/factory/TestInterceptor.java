package why.demo.spring.aop.factory;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

/**
 * todo desc
 * @author why
 * @date 2019-03-25 14:57
 */
public class TestInterceptor implements MethodInterceptor {

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        System.out.println("interceptor before 4");
        //String ret = invocation.proceed();
        String ret = "ret";
        System.out.println("interceptor after 4");
        return ret;
    }
}
