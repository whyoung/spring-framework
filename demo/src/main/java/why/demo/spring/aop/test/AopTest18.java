package why.demo.spring.aop.test;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.aop.aspectj.annotation.AnnotationAwareAspectJAutoProxyCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ConfigurationClassPostProcessor;
import org.springframework.context.support.GenericApplicationContext;

import java.io.IOException;

public class AopTest18 {

    public static void main(String[] args) throws IOException {
        GenericApplicationContext context = new GenericApplicationContext();
        {
            context.registerBean(ConfigurationClassPostProcessor.class);
            context.registerBean(AutowiredAnnotationBeanPostProcessor.class);
            context.registerBean(AnnotationAwareAspectJAutoProxyCreator.class);
            //如果直接注册了一个bean实例，不会再创建代理
            context.getDefaultListableBeanFactory().registerSingleton("bean1", new Bean1());
            context.registerBean(Config.class);
        }

        context.refresh();

        {
            Bean1 bean1 = (Bean1) context.getBean("bean1");
            System.out.println(bean1.bean2 == null);  //目标对象属性不会被填充
            //通过get方法实际调用的是目标对象的方法
            System.out.println(bean1.getBean2() == null);
            bean1.m();
        }
        System.in.read();
    }

    @Configuration
    static class Config {
        @Bean("bean1")
        public Bean1 bean1() {
            return new Bean1();
        }

        @Bean
        public Bean2 bean2() {
            return new Bean2();
        }

        @Bean
        public MyAspect myAspect() {
            return new MyAspect();
        }
    }

    @Aspect
    static class MyAspect {

        @Before("execution(* why.demo.spring.aop.test.AopTest18.Bean1.*())")
        public void before() {
            System.out.println("before...");
        }
    }

    static class Bean1 {

        @Autowired
        private Bean2 bean2;

        public Bean1() {
            printStackTrace();
            System.out.println("bean1 constructor");
        }
        public void m() {
            System.out.println("bean1 method");
        }

        public Bean2 getBean2() {
            return bean2;
        }
    }

    static class Bean2 {

    }

    private static void printStackTrace() {
        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        StringBuilder sb = new StringBuilder();
        for (int i = elements.length - 1; i>= 2; i--) {
            StackTraceElement e = elements[i];
            System.out.println(sb + e.getClassName() + "#"  + e.getMethodName() + "#" + e.getLineNumber());
            sb.append("\t");
        }
    }
}
