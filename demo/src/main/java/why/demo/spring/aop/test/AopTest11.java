package why.demo.spring.aop.test;

import org.aopalliance.aop.Advice;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.aop.Advisor;
import org.springframework.aop.aspectj.AspectJExpressionPointcut;
import org.springframework.aop.aspectj.annotation.AnnotationAwareAspectJAutoProxyCreator;
import org.springframework.aop.framework.autoproxy.AbstractAdvisorAutoProxyCreator;
import org.springframework.aop.framework.autoproxy.AbstractAutoProxyCreator;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ConfigurationClassPostProcessor;
import org.springframework.context.support.GenericApplicationContext;

import java.lang.reflect.Method;
import java.util.List;

public class AopTest11 {

    /*
    AnnotationAwareAspectJAutoProxyCreator的作用：
    1.找到全部的Aspect格式的高级切面，并转换成低级的Advisor切面
    2.根据切面创建代理对象
     */
    public static void main(String[] args) throws Exception {
        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean(ConfigurationClassPostProcessor.class);
//        context.registerBean(AnnotationAwareAspectJAutoProxyCreator.class);
        context.registerBean(Config.class);
        context.refresh();

        Bean1 bean1 = (Bean1) context.getBean("bean1");
        System.out.println(bean1); //toString 方法不被代理
        bean1.m();

        for (String name : context.getBeanDefinitionNames()) {
            System.out.println(name);
        }

        AnnotationAwareAspectJAutoProxyCreator proxyCreator = new AnnotationAwareAspectJAutoProxyCreator();
        proxyCreator.setBeanFactory(context.getBeanFactory());

//                context.getBean(AnnotationAwareAspectJAutoProxyCreator.class);

        Method findEligibleAdvisors = AbstractAdvisorAutoProxyCreator.class
                .getDeclaredMethod("findEligibleAdvisors", Class.class, String.class);
        findEligibleAdvisors.setAccessible(true);

        List<Advisor> advisors = (List<Advisor>) findEligibleAdvisors.invoke(proxyCreator, Bean1.class, "bean1");
        for (Advisor advisor : advisors) {
            System.out.println(advisor);
        }

        Method wrapIfNecessary = AbstractAutoProxyCreator.class.
                getDeclaredMethod("wrapIfNecessary", Object.class, String.class, Object.class);
        wrapIfNecessary.setAccessible(true);
        Object proxy = wrapIfNecessary.invoke(proxyCreator, bean1, "bean1", "bean1");

        System.out.println(proxy.getClass());

        Object target = new Object();
        advisors = (List<Advisor>) findEligibleAdvisors.invoke(proxyCreator, Class.class, "bean2");
        System.out.println(advisors); //empty
        context.close();
    }

    @Configuration
    static class Config {

        @Bean
        public Advisor advisor(Advice beforeAdvice) {
            AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
            pointcut.setExpression("execution(* why.demo.spring.aop.test.AopTest11.Bean1.*())");
            return new DefaultPointcutAdvisor(pointcut, beforeAdvice);
        }

        @Bean
        public MethodInterceptor beforeAdvice() {
            return new MethodInterceptor() {
                @Override
                public Object invoke(MethodInvocation invocation) throws Throwable {
                    System.out.println("before invoke " + invocation.getMethod().getName());
                    return invocation.proceed();
                }
            };
        }

        @Bean
        public Bean1 bean1() {
            return new Bean1();
        }

        @Bean
        public MyAspect myAspect() {
            return new MyAspect();
        }
    }

    @Aspect
    static class MyAspect {

        @After("execution(* why.demo.spring.aop.test.AopTest11.Bean1.*())")
        public void after() {
            System.out.println("after invoke");
        }
    }

    static class Bean1 {
        public void m() {
            System.out.println("invoke method m");
        }
    }
}
