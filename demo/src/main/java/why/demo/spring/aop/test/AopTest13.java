package why.demo.spring.aop.test;

import org.aspectj.lang.annotation.*;
import org.springframework.aop.Advisor;
import org.springframework.aop.aspectj.AspectInstanceFactory;
import org.springframework.aop.aspectj.AspectJExpressionPointcut;
import org.springframework.aop.aspectj.AspectJMethodBeforeAdvice;
import org.springframework.aop.aspectj.SingletonAspectInstanceFactory;
import org.springframework.aop.support.DefaultPointcutAdvisor;

import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;

public class AopTest13 {

    /*
    高级切面转低级Advisor
     */
    public static void main(String[] args) {

        AspectInstanceFactory factory = new SingletonAspectInstanceFactory(new MyAspect());

        /*
        AspectJ 通知类型
        AspectJMethodBeforeAdvice -> MethodBeforeAdviceAdapter -> MethodBeforeAdviceInterceptor
        AspectJAfterAdvice：after
        AspectJAfterReturningAdvice：
        AspectJAfterThrowingAdvice：
        AspectJAroundAdvice

        对于非环绕通知，都会转换成环绕通知（适配器模式）
         */
        List<Advisor> advisors = new LinkedList<>();
        for (Method method : MyAspect.class.getDeclaredMethods()) {
            if (method.isAnnotationPresent(Before.class)) {
                Before before = method.getAnnotation(Before.class);
                String expression = before.value();
                //切点
                AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
                pointcut.setExpression(expression);
                //advice

                AspectJMethodBeforeAdvice advice = new AspectJMethodBeforeAdvice(method, pointcut, factory);

                Advisor advisor = new DefaultPointcutAdvisor(pointcut, advice);
                advisors.add(advisor);
            }
        }
        System.out.println(advisors);
    }

    @Aspect
    static class MyAspect {

        @Before("execution(* *())")
        public void before() {
            System.out.println("before...");
        }

        @Around("execution(* *())")
        @AfterReturning
        @AfterThrowing
        @After("execution(* *())")
        public void after() {
            System.out.println("after...");
        }
    }
}
