package why.demo.spring.aop.factory;

import org.springframework.aop.framework.AdvisedSupport;
import org.springframework.aop.framework.AdvisedSupportListener;
import org.springframework.aop.framework.ProxyFactory;

import java.lang.reflect.Method;

/**
 * @author why
 * @date 2019-03-25 11:37
 */
public class ProxyTest {

    public static void main(String[] args) {
        ProxyFactory factory = new ProxyFactory();
        factory.setTarget(new TestService());

        factory.addAdvice(new TestAdvice2());
        factory.addAdvisor(new TestAdvisor(new TestAdvice() {
            @Override
            public void before(Method method, Object[] args, Object target) throws Throwable {
                System.out.println("advisor before 2");
            }

            @Override
            public void afterReturning(Object returnValue, Method method, Object[] args, Object target) throws Throwable {
                System.out.println("advisor after 2");
            }
        }));
        factory.addAdvice(new TestAdvice());

        factory.addAdvice(new TestInterceptor());


        factory.addListener(new AdvisedSupportListener() {
            @Override
            public void activated(AdvisedSupport advised) {
                System.out.println("activated ...");
            }

            @Override
            public void adviceChanged(AdvisedSupport advised) {
                System.out.println("adviceChanged ...");
            }
        });
        TestService proxy = (TestService) factory.getProxy();
        System.out.println("--------start----------");
        System.out.println(proxy.func());
    }
}
