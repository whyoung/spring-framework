package why.demo.spring.aop.test;

import org.springframework.cglib.core.Signature;

import java.lang.reflect.InvocationTargetException;

public class AopTest7 {

    public static void main(String[] args) throws Exception {
        AopTest6.Impl impl = new AopTest6.Impl();

        TargetFastClass targetFastClass = new TargetFastClass();
        int index = targetFastClass.getIndex(new Signature("m", "()V"));
        targetFastClass.invoke(index, impl, new Object[0]);

        ProxyFastClass proxyFastClass = new ProxyFastClass();
        index = proxyFastClass.getIndex(new Signature("mSuper", "(I)I"));
        proxyFastClass.invoke(index, new AopTest6.Proxy(null), new Object[]{100});
    }

    static class TargetFastClass {

        static Signature s1 = new Signature("m", "()V");
        static Signature s2 = new Signature("m", "(I)I");

        public Object invoke(int index, Object target, Object[] args) throws InvocationTargetException {
            AopTest6.Impl impl = (AopTest6.Impl) target;
            if (index == 1) {
                impl.m();
                return null;
            } else if (index == 2) {
                return impl.m((int)args[0]);
            }
            throw new NoSuchMethodError();
        }

        public int getIndex(Signature signature) {
            if (s1.equals(signature)) {
                return 1;
            }
            if (s2.equals(signature)) {
                return 2;
            }
            return -1;
        }
    }

    static class ProxyFastClass {

        static Signature s1 = new Signature("mSuper", "()V");
        static Signature s2 = new Signature("mSuper", "(I)I");

        public Object invoke(int index, Object proxy, Object[] args) throws InvocationTargetException {
            AopTest6.Proxy proxy1 = (AopTest6.Proxy) proxy;
            if (index == 1) {
                proxy1.mSuper();
                return null;
            } else if (index == 2) {
                return proxy1.mSuper((int)args[0]);
            }
            throw new NoSuchMethodError();
        }

        public int getIndex(Signature signature) {
            if (s1.equals(signature)) {
                return 1;
            }
            if (s2.equals(signature)) {
                return 2;
            }
            return -1;
        }
    }
}
