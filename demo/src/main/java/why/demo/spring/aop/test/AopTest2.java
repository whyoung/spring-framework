package why.demo.spring.aop.test;

import java.lang.reflect.Method;

public class AopTest2 {

    public static void main(String[] args) throws Throwable {
        Impl target = new Impl();
        InvocationHandler h = new InvocationHandler() {
            @Override
            public Object invoke(Method method, Object[] args) throws Throwable {
                System.out.println("before...");
                Object result = method.invoke(target, args);
                System.out.println("after...");
                return result;
            }
        };
        Iface iface = new MyProxy(h);
        iface.m();
    }

    interface Iface {
        void m();
    }

    static class Impl implements Iface {

        @Override
        public void m() {
            System.out.println("impl method");
        }
    }

    interface InvocationHandler {

        Object invoke(Method method, Object[] args) throws Throwable;
    }

    static class Proxy {
        final InvocationHandler h;
        public Proxy(InvocationHandler invocationHandler) {
            this.h = invocationHandler;
        }
    }

    static class MyProxy extends Proxy implements Iface {

        static Method m;
        static {
            try {
                m = Iface.class.getDeclaredMethod("m");
            } catch (NoSuchMethodException e) {
                throw new NoSuchMethodError(e.getMessage());
            }
        }
        protected MyProxy(InvocationHandler h) {
            super(h);
        }

        @Override
        public void m() {
            try {
                h.invoke(m, new Object[0]);
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        }
    }
}
