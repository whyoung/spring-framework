package why.demo.spring.aop.test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class AopTest3 {

    //mac 下测试无效果
    public static void main(String[] args) throws Exception{
        Method method = Test.class.getDeclaredMethod("m", int.class);
        for (int i = 0; i<10000; i++) {
            Field field = Method.class.getDeclaredField("methodAccessor");
            field.setAccessible(true);
            Object methodAccessor = field.get(method);
            if (methodAccessor != null) {
                System.out.println(i + "#" + field.get(method).getClass());
            } else {
                System.out.println(i + "#" + null);
            }
            method.invoke(null, i);
        }
    }

    static class Test {
        static void m(int i) {
            System.out.println(i);
        }
    }
}
