package why.demo.spring.aop.test;

import org.aopalliance.aop.Advice;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.aop.Advisor;
import org.springframework.aop.aspectj.AspectJExpressionPointcut;
import org.springframework.aop.aspectj.annotation.AnnotationAwareAspectJAutoProxyCreator;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.CommonAnnotationBeanPostProcessor;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ConfigurationClassPostProcessor;
import org.springframework.context.support.GenericApplicationContext;

import javax.annotation.PostConstruct;

public class AopTest12 {

    /*
    代理对象的创建时机
    构造 -1-> 依赖注入 -2-> 初始化
    如果存在循环依赖，代理对象是在构造和依赖注入之间创建代理
    否则是在依赖注入之后和初始化之前创建代理

    如果Bean1 不注入Bean2（去掉setBean2方法上的Autowired注解），执行顺序：
    Bean1 created
    Bean1 init
    Bean2 created
    代理对象是在init之后被创建的
    autowired bean1 class why.demo.spring.aop.test.AopTest12$Bean1$$EnhancerBySpringCGLIB$$b98eee4
    Bean2 init

    Bean1 注入Bean2
    Bean1 created
    Bean2 created
    Bean1的代理对象是在init方法之前被创建的
    autowired bean1 class why.demo.spring.aop.test.AopTest12$Bean1$$EnhancerBySpringCGLIB$$363cdaca
    Bean2 init
    autowired bean2 class why.demo.spring.aop.test.AopTest12$Bean2
    Bean1 init
     */
    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        {
            context.registerBean(Config.class);
            context.registerBean(ConfigurationClassPostProcessor.class);
            context.registerBean(AutowiredAnnotationBeanPostProcessor.class);
            context.registerBean(CommonAnnotationBeanPostProcessor.class);
            context.registerBean(AnnotationAwareAspectJAutoProxyCreator.class);
        }

        context.refresh();

//        {
//            Bean1 bean1 = context.getBean(Bean1.class);
//            System.out.println(bean1.getClass());
//            bean1.m();
//        }

        context.close();
    }

    @Configuration
    static class Config {

        @Bean
        public MethodInterceptor beforeAdvice() {
            return new MethodInterceptor() {
                @Override
                public Object invoke(MethodInvocation invocation) throws Throwable {
                    System.out.println("before invoke " + invocation.getMethod().getName());
                    return invocation.proceed();
                }
            };
        }

        @Bean
        public Advisor advisor(Advice beforeAdvice) {
            AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
            pointcut.setExpression("execution(* why.demo.spring.aop.test.AopTest12.Bean1.*())");
            return new DefaultPointcutAdvisor(pointcut, beforeAdvice);
        }

        @Bean
        public Bean1 bean1() {
            return new Bean1();
        }

        @Bean
        public Bean2 bean2() {
            return new Bean2();
        }
    }

    @Aspect
    static class MyAspect {

        @After("execution(* why.demo.spring.aop.test.AopTest12.Bean1.*())")
        public void after() {
        }
    }

    static class Bean1 {

        public Bean1() {
            System.out.println("Bean1 created");
        }

//        @Autowired
        public void setBean2(Bean2 bean2) {
            System.out.println("autowired bean2 " + bean2.getClass());
        }

        @PostConstruct
        public void init() {
            System.out.println("Bean1 init");
        }

        public void m() {
            System.out.println("invoke Bean1's method m");
        }
    }

    static class Bean2 {

        public Bean2() {
            System.out.println("Bean2 created");
        }

        @PostConstruct
        public void init() {
            System.out.println("Bean2 init");
        }

        @Autowired
        public void setBean1(Bean1 bean1) {
            System.out.println("autowired bean1 " + bean1.getClass());
        }
    }
}
