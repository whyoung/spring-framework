package why.demo.spring.aop.factory;

import org.aopalliance.aop.Advice;
import org.springframework.aop.Advisor;

/**
 * todo desc
 * @author why
 * @date 2019-03-25 12:30
 */
public class TestAdvisor implements Advisor {

    private Advice advice;
    public TestAdvisor(Advice advice) {
        this.advice = advice;
    }
    @Override
    public Advice getAdvice() {
        return this.advice;
    }

    @Override
    public boolean isPerInstance() {
        return false;
    }
}
