package why.demo.spring.aop.test;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.aop.Advisor;
import org.springframework.aop.aspectj.annotation.AnnotationAwareAspectJAutoProxyCreator;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.framework.autoproxy.AbstractAdvisorAutoProxyCreator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ConfigurationClassPostProcessor;
import org.springframework.context.support.GenericApplicationContext;

import java.lang.reflect.Method;
import java.util.List;

public class AopTest16 {

    /*
        动态通知调用，通知方法需要提供参数
        不带参数绑定的叫静态通知调用，执行时不用切点对象了
        带参数绑定的，调用的时候需要解析参数，执行时还需要切点对象
     */
    public static void main(String[] args) throws Exception {
        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean(ConfigurationClassPostProcessor.class);
        context.registerBean(AnnotationAwareAspectJAutoProxyCreator.class);
        context.registerBean(Config.class);
        context.refresh();

        AnnotationAwareAspectJAutoProxyCreator proxyCreator =
                new AnnotationAwareAspectJAutoProxyCreator();
        proxyCreator.setBeanFactory(context.getBeanFactory());
        Method findEligibleAdvisors = AbstractAdvisorAutoProxyCreator.class
                .getDeclaredMethod("findEligibleAdvisors", Class.class, String.class);
        findEligibleAdvisors.setAccessible(true);

        List<Advisor> advisors = (List<Advisor>) findEligibleAdvisors.invoke(proxyCreator, Target.class, "target");
        for (Advisor advisor : advisors) {
            System.out.println(advisor.getClass());
        }

        Method m = Target.class.getDeclaredMethod("m", int.class);
        Target target = new Target();
        ProxyFactory factory = new ProxyFactory();
        factory.setTarget(target);
        factory.setTargetClass(Target.class);
        factory.addAdvisors(advisors.toArray(new Advisor[0]));
        List<Object> objects = factory.getInterceptorsAndDynamicInterceptionAdvice(m, Target.class);
        for (Object obj : objects) {
            System.out.println(obj);
        }

        Target target1 = context.getBean(Target.class);
        target1.m(100);
        context.close();
    }

    @Configuration
    static class Config {

        @Bean
        public Target target() {
            return new Target();
        }

        @Bean
        public MyAspect myAspect() {
            return new MyAspect();
        }
    }

    @Aspect
    static class MyAspect {

        @After("execution(* why.demo.spring.aop.test.AopTest16.Target.*(..)) && args(x)")
        public void after(int x) {
            System.out.println("after invoke with " + x);
        }

        @Before("execution(* why.demo.spring.aop.test.AopTest16.Target.*(..))")
        public void before(JoinPoint p) {
            System.out.println(p.getSignature());
            System.out.println("before invoke");
        }
    }

    static class Target {
        public void m(int x) {
            System.out.println("invoke method m with " + x);
        }
    }
}
