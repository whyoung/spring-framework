package why.demo.spring.aop.test;

import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class AopTest6 {

    public static void main(String[] args) {
        Impl target = new Impl();
        MethodInterceptor interceptor = new MethodInterceptor() {
            @Override
            public Object intercept(Object o, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
                System.out.println("before...");
                Object result = methodProxy.invoke(target, args);
                System.out.println("after...");
                return result;
            }
        };
        Proxy proxy = new Proxy(interceptor);
        proxy.m();
        System.out.println(proxy.m(2));

        interceptor = new MethodInterceptor() {
            @Override
            public Object intercept(Object o, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
                System.out.println("before...");
                Object result = methodProxy.invokeSuper(o, args);
                System.out.println("after...");
                return result;
            }
        };
        proxy = new Proxy(interceptor);
        proxy.m();
        System.out.println(proxy.m(2));
    }

    static class Impl {
        public void m() {
            System.out.println("run...");
        }

        public int m(int i) {
            System.out.println("run..." + i);
            return i;
        }
    }

    static class Proxy extends Impl {

        static Method m1;
        static Method m2;

        static MethodProxy methodProxy1;
        static MethodProxy methodProxy2;

        static {
            try {
                m1 = Impl.class.getDeclaredMethod("m");
                m2 = Impl.class.getDeclaredMethod("m", int.class);
                methodProxy1 = MethodProxy.create(Impl.class, Proxy.class, "()V", "m", "mSuper");
                methodProxy2 = MethodProxy.create(Impl.class, Proxy.class, "(I)I", "m", "mSuper");
            } catch (NoSuchMethodException e) {
                throw new NoSuchMethodError(e.getMessage());
            }
        }

        private final MethodInterceptor interceptor;

        public Proxy(MethodInterceptor interceptor) {
            this.interceptor = interceptor;
        }

        @Override
        public void m() {
            try {
                interceptor.intercept(this, m1, new Object[0], methodProxy1);
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public int m(int i) {
            try {
                return (int) interceptor.intercept(this, m2, new Object[]{i}, methodProxy2);
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        }

        public void mSuper() {
            super.m();
        }

        public int mSuper(int i) {
            return super.m(i);
        }
    }
}
