package why.demo.spring.aop.test;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.Advisor;
import org.springframework.aop.aspectj.AspectJExpressionPointcut;
import org.springframework.aop.framework.AopContext;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.DefaultPointcutAdvisor;

public class AopTest8 {

    public static void main(String[] args) throws Exception {
        /*
        spring 选择代理类型的策略：
        proxyTargetClass：false并且指定实现了接口，用jdk方式实现代理
        否则用cglib方式
        */
        AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
        pointcut.setExpression("execution(* m())");

        //MethodInterceptor 属于环绕通知，spring底层会将所有的非环绕通知类型用适配器模式
        //转换成环绕通知
        MethodInterceptor advice = new MethodInterceptor() {

            @Override
            public Object invoke(MethodInvocation invocation) throws Throwable {
                System.out.println("before...");
                Object result = invocation.proceed();
                System.out.println("after...");
                return result;
            }
        };
        Advisor advisor = new DefaultPointcutAdvisor(pointcut, advice);

        Object target = new Test1();
        //代理工厂
        ProxyFactory factory = new ProxyFactory();
        factory.setExposeProxy(true);  //AopContext.currentProxy() 能获取到当前类的目标类
        factory.setProxyTargetClass(true); //代理目标类，true根据cglib创建代理，否则看是否实现接口
        factory.setInterfaces(Iface.class);  //必须手动指定接口
        factory.setTarget(target);
        factory.addAdvisor(advisor);

        System.out.println(target);
        Object obj = factory.getProxy();
        if (obj instanceof Iface) {
            Iface proxy = (Iface) obj;
            proxy.m();
            System.out.println(proxy.x() == proxy);
        }

        System.out.println(factory.getProxy().getClass()); //class why.demo.spring.aop.test.$Proxy0


    }

    static interface Iface {
        void m();

        default Object x() {
            return AopContext.currentProxy();
        }
    }

    static class Test1 implements Iface {
        @Override
        public void m() {
            System.out.println("test1 run...");
            System.out.println(AopContext.currentProxy());
        }
    }

    static class Test2 {
        public void m() {
            System.out.println("test2 run...");
        }
    }
}
