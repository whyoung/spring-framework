package why.demo.spring.aop.test;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;

public class AopTest15 {

    /*
    模拟MethodInvocation 的执行流程
     */
    public static void main(String[] args) throws Throwable {

        MethodInterceptor advice1 = new MethodInterceptor() {
            @Override
            public Object invoke(MethodInvocation invocation) throws Throwable {
                System.out.println("Advice1 before...");
                return invocation.proceed();
            }
        };

        MethodInterceptor advice2 = new MethodInterceptor() {
            @Override
            public Object invoke(MethodInvocation invocation) throws Throwable {
                Object result = invocation.proceed();
                System.out.println("Advice2 after...");
                return result;
            }
        };

        MethodInterceptor advice3 = new MethodInterceptor() {
            @Override
            public Object invoke(MethodInvocation invocation) throws Throwable {
                System.out.println("Advice3 before...");
                Object result = invocation.proceed();
                System.out.println("Advice3 after...");
                return result;
            }
        };


        Target target = new Target();
        Method m = Target.class.getDeclaredMethod("m");
        MyMethodInvocation invocation = new MyMethodInvocation();
        invocation.target = target;
        invocation.method = m;
        invocation.args = new Object[0];
        List<MethodInterceptor> interceptors = new LinkedList<>();
        interceptors.add(advice1);
        interceptors.add(advice2);
        interceptors.add(advice3);
        invocation.interceptorList = interceptors;

        invocation.proceed();
    }

    static class MyMethodInvocation implements MethodInvocation {

        private Object target;

        private Method method;

        private Object[] args;

        private List<MethodInterceptor> interceptorList;

        private int count = 1;

        @Override
        public Object[] getArguments() {
            return args;
        }

        @Override
        public Object proceed() throws Throwable {
            if (count ++ <= interceptorList.size()) {
                MethodInterceptor interceptor = interceptorList.get(count - 2);
                return interceptor.invoke(this);
            }
            return method.invoke(target, args);
        }

        @Override
        public Object getThis() {
            return target;
        }

        @Override
        public AccessibleObject getStaticPart() {
            return method;
        }

        @Override
        public Method getMethod() {
            return method;
        }
    }

    static class Target {
        public void m() {
            System.out.println("invoke m");
        }
    }
}
