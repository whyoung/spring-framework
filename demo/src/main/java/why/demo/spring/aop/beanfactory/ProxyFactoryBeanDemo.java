package why.demo.spring.aop.beanfactory;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author why
 * @date 2019-03-25 18:32
 */
public class ProxyFactoryBeanDemo {

    public static void main(String[] args) {


        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(AopConfig.class);

        ServiceIface service1 = (ServiceIface) context.getBean("serviceImpl");

        ServiceImpl2 service2 = (ServiceImpl2) context.getBean("serviceImpl2");

        service1.func();

        System.out.println("---------------------------");
        service2.func();
        System.out.println("---------------------------");
        ServiceImpl3 service3 = new ServiceImpl3();
        service3.func();
    }
}
