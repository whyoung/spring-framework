package why.demo.spring.aop.test;

import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class AopTest4 {

    public static void main(String[] args) {
        Impl target = new Impl();
        Impl impl = (Impl) Enhancer.create(Impl.class, new MethodInterceptor() {
            @Override
            public Object intercept(Object proxy, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
                System.out.println("before...");
                Object result = method.invoke(target, args);
                System.out.println("after...");
                return result;
            }
        });
        impl.m();

        impl = (Impl) Enhancer.create(Impl.class, new MethodInterceptor() {
            @Override
            public Object intercept(Object proxy, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
                System.out.println("before...");
                Object result = methodProxy.invoke(target, args);
                System.out.println("after...");
                return result;
            }
        });
        impl.m();

        impl = (Impl) Enhancer.create(Impl.class, new MethodInterceptor() {
            @Override
            public Object intercept(Object proxy, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
                System.out.println("before...");
                Object result = methodProxy.invokeSuper(proxy, args);
                System.out.println("after...");
                return result;
            }
        });
        impl.m();
    }

    static class Impl {
        public void m() {
            System.out.println("run...");
        }
    }
}
