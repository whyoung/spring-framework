package why.demo.spring.aop.beanfactory;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @author why
 * @date 2019-03-25 18:32
 */
@Component
@Aspect
public class AspectConfig {

    @Pointcut("execution(* * (..))")
    public void aspect() {

    }

    @Before("aspect()")
    public void before() {
        System.out.println("before");
    }

    @Around("aspect()")
    public Object around(ProceedingJoinPoint p) throws Throwable {
        System.out.println("around before");
        Object ret = p.proceed();
        System.out.println("around after");
        return ret;
    }
}
