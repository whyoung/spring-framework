package why.demo.spring.extra.bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import why.demo.spring.demo.bean.DemoBean;

/**
 * @author why
 * @date 2018/11/21 4:11 PM
 * @Description: todo
 */
@Component
public class ExtraDemoBean {

    @Autowired
    private DemoBean demoBean;

    public void m() {
        System.out.println(demoBean);
    }
}