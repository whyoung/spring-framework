package why.demo.spring.demo.config;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import java.util.Date;

/**
 * @author why
 * @date 2019-04-17 09:58
 */
@Configuration
@EnableAspectJAutoProxy(exposeProxy = true)
@Aspect
public class AspectConfig {

    @Pointcut("execution(* * (..))")
    public void aspect() {

    }

    @Before("aspect()")
    public void before() {
        System.out.println("before");
    }

    @Bean("date")
    public Date now() {
        return new Date();
    }
}
