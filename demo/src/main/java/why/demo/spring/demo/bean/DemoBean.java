package why.demo.spring.demo.bean;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author why
 * @date 2018/11/14 4:34 PM
 * @Description:
 */
@Component
public class DemoBean implements EnvironmentAware, ApplicationContextAware {

    private Environment environment;

    private ApplicationContext context;

    /** 用来校验bean是单例的 */
    private static final AtomicInteger AI = new AtomicInteger();

    private int id;

    public DemoBean() {
        this.id = AI.incrementAndGet();
    }

    @Override
    public String toString() {
        return "DemoBean{" +
                "id=" + id +
                '}';
    }

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
        //System.out.println(environment.getProperty("key"));
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }

    public void load() {
        if(this.context.getAutowireCapableBeanFactory() instanceof DefaultListableBeanFactory) {
            BeanDefinition bd = ((DefaultListableBeanFactory) this.context.getAutowireCapableBeanFactory()).getBeanDefinition("deferredDemoBean");
            System.out.println(bd);
        }
    }
}
