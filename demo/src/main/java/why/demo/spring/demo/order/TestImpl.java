package why.demo.spring.demo.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * todo desc
 * @author why
 * @date 2019-07-09 13:04
 */
@Service
public class TestImpl {

    @Autowired(required = false)
    List<Iface> ifaceList;

    public void test() {
        if(ifaceList != null) {
            for(Iface i : ifaceList) {
                i.test();
            }
        }
    }
}
