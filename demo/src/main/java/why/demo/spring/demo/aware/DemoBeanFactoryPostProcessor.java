package why.demo.spring.demo.aware;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.core.PriorityOrdered;
import org.springframework.stereotype.Component;

/**
 * @author why
 * @date 2018/11/14 4:37 PM
 * @Description: bean factory 处理器，如果要生效，必须交由spring容器管理
 * 可以通过 PriorityOrdered 的 order 控制 processor 的加载顺序
 */
@Component
public class DemoBeanFactoryPostProcessor implements BeanFactoryPostProcessor, PriorityOrdered {

    /**
     * bean factory 处理器，把demoBean的作用域由singleton 改成 prototype
     * @param beanFactory the bean factory used by the application context
     * @throws BeansException
     */
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {

        BeanDefinition bd = beanFactory.getBeanDefinition("demoBean");
        if(bd != null) {
            bd.setScope("prototype");
        }
    }

    @Override
    public int getOrder() {
        return 180;
    }
}
