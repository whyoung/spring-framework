package why.demo.spring.demo.bean;

import org.springframework.beans.factory.FactoryBean;

/**
 * @author why
 * @date 2018/11/19 4:48 PM
 * @Description: todo
 */
public class DemoFactoryBean<T> implements FactoryBean<T> {
    @Override
    public T getObject() throws Exception {
        return null;
    }

    @Override
    public Class<?> getObjectType() {
        try {
            Object o = getObject();
            if(o != null) {
                return o.getClass();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
