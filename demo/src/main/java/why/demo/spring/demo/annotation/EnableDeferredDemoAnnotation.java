package why.demo.spring.demo.annotation;

import org.springframework.context.annotation.Import;
import why.demo.spring.demo.aware.DeferredDemoImportSelector;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author why
 * @date 2018/11/21 4:09 PM
 * @Description: todo
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(DeferredDemoImportSelector.class)
public @interface EnableDeferredDemoAnnotation {
}
