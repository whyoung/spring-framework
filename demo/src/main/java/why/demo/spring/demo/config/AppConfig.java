package why.demo.spring.demo.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import why.demo.spring.demo.annotation.EnableDemoAnnotation;

/**
 * @author why
 * @date 2018/11/14 4:29 PM
 * @Description:
 */
@Configuration
@ComponentScan("why.spring.demo")
@PropertySource("classpath:config.properties")
@EnableDemoAnnotation
@Deprecated
public class AppConfig {
}
