package why.demo.spring.demo.bean;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

/**
 * @author why
 * @date 2018/11/19 3:32 PM
 * @Description: todo
 */
public class InitializingDemoBean implements InitializingBean {

    @Autowired
    private DemoBean demoBean;

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("before: InitializingBean");
    }

    @PostConstruct
    public void init() {
        System.out.println("before: post construct");
    }

    @Override
    public String toString() {
        return "InitializingDemoBean{" +
                "demoBean=" + demoBean +
                '}';
    }
}
