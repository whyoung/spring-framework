package why.demo.spring.demo.aware;

import org.springframework.context.annotation.DeferredImportSelector;
import org.springframework.core.type.AnnotationMetadata;
import why.demo.spring.deferred.config.DeferredAppConfig;

/**
 * @author why
 * @date 2018/11/21 4:10 PM
 * @Description: todo
 */
public class DeferredDemoImportSelector implements DeferredImportSelector {

    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        return new String[]{DeferredAppConfig.class.getName()};
    }
}
