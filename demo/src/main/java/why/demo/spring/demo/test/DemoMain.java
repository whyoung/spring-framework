package why.demo.spring.demo.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import why.demo.spring.demo.bean.DisposableDemoBean;
import why.demo.spring.demo.annotation.EnableDeferredDemoAnnotation;
import why.demo.spring.demo.annotation.EnableDemoAnnotation;
import why.demo.spring.demo.bean.DemoBean;
import why.demo.spring.demo.bean.InitializingDemoBean;
import why.demo.spring.demo.bean.TestBean2;
import why.demo.spring.demo.config.AspectConfig;
import why.demo.spring.demo.config.TestConfig;
import why.demo.spring.demo.config.innerpackage.TestService;

import java.util.Date;

/**
 * @author why
 * @date 2018/11/14 4:33 PM
 * @Description: todo
 */
@Configuration
@ComponentScan("why.spring.demo")
@PropertySource("classpath:config.properties")
@EnableDemoAnnotation
@EnableDeferredDemoAnnotation
public class DemoMain {

    public static void main(String[] args) throws InterruptedException {
//        testBeanRegister();
        //testBeanFactoryPostProcessor();
//        testComponentScan();
//        CountDownLatch latch = new CountDownLatch(1);
//        latch.await();
//        testBeanPostProcessor();
        //testComponentScan();
//        testProxy();
//        testConfiguration();
        testXml();
    }

    /**
     * 测试bean注册
     */
    private static void testBeanRegister() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
//        context.register(DemoBean.class, InitializingDemoBean.class, DisposableDemoBean.class);
//        context.refresh();
//        context.getBean("initializingDemoBean");
//
////        context.addBeanFactoryPostProcessor();
//        context.register(DemoBean.class);
    }


    /**
     * 测试bean factory post processor
     */
    private static void testBeanFactoryPostProcessor() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();

        //注册bean factory 处理器
        //context.addBeanFactoryPostProcessor(new DemoBeanFactoryPostProcessor());
        //context.register(DemoBeanFactoryPostProcessor.class);
        context.register(TestBean2.class);
        context.refresh();

        TestBean2 t = context.getBean(TestBean2.class);
//        System.out.println(context.getBean(BeanFactory.class));
        t.test();
//        System.out.println(context.getBean(BeanFactory.class));

//        context.register(DemoBean.class);
//        context.refresh();
//        System.out.println(context.getBean("systemProperties"));
//        System.out.println(context.getBean(DemoBean.class));
//        System.out.println(context.getBean(DemoBean.class));
    }

    /**
     * 测试bean扫描，如果有post processor，也需要交由spring容器管理才能起作用
     */
    private static void testComponentScan() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DemoMain.class);


//        System.out.println(context.getBean(DemoBean.class));
//        System.out.println(context.getBean(DemoBean.class));
//        context.getBean(ExtraDemoBean.class).m();
//        DemoBean demo = context.getBean(DemoBean.class);
//        demo.load();
    }


    private static void testBeanPostProcessor() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(DemoBean.class, InitializingDemoBean.class, DisposableDemoBean.class);
//        context.register(DemoBeanFactoryPostProcessor.class);
//        context.getBeanFactory().addBeanPostProcessor(new DemoBeanPostProcessor());
//        //context.getBeanFactory().addBeanPostProcessor(new DemoInstantiationAwareBeanPostProcessor());
        context.refresh();
//        DemoBean bean = ;
        System.out.println(context.getBean("demoBean"));
        System.out.println(context.getBean(DemoBean.class));
//        bean.toString();
    }

    private static void testProxy() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(AspectConfig.class);
        context.refresh();
        System.out.println(context.getBean("date"));
    }


    private static void testConfiguration() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(TestConfig.class);

        TestService testService = context.getBean(TestService.class);
        testService.test();
    }

    private static void testXml() {
        ApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        Date date = context.getBean(Date.class);
        System.out.println(date);
    }
}
