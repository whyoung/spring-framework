package why.demo.spring.demo.bean;

import org.springframework.beans.factory.DisposableBean;

import javax.annotation.PreDestroy;

/**
 * @author why
 * @date 2018/11/19 3:35 PM
 * @Description: todo
 */
public class DisposableDemoBean implements DisposableBean {

    @Override
    public void destroy() throws Exception {
        System.out.println("after: DisposableBean");
    }

    @PreDestroy
    public void preDestroy() throws Exception {
        System.out.println("after: pre destroy");
    }
}
