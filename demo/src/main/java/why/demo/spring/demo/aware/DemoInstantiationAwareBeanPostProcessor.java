package why.demo.spring.demo.aware;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;
import why.demo.spring.demo.bean.DemoBean;

/**
 * @author why
 * @date 2018/11/19 5:12 PM
 * @Description: todo
 */
public class DemoInstantiationAwareBeanPostProcessor implements InstantiationAwareBeanPostProcessor {

    @Override
    public Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException {
        if(beanClass != null && beanClass == DemoBean.class) {
            System.out.println("before: InstantiationAwareBeanPostProcessor, bean name = " + beanName);
            return new DemoBean();
        }
        return null;
    }
}
