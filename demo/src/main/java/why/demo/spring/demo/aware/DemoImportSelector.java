package why.demo.spring.demo.aware;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;
import why.demo.spring.extra.config.ExtraAppConfig;

/**
 * @author why
 * @date 2018/11/21 4:10 PM
 * @Description: todo
 */
public class DemoImportSelector implements ImportSelector {

    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        return new String[]{ExtraAppConfig.class.getName()};
    }
}
