package why.demo.spring.demo.aware;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import why.demo.spring.demo.bean.DemoBean;

import java.lang.reflect.Proxy;

/**
 * @author why
 * @date 2018/11/19 4:43 PM
 * @Description: todo
 */
public class DemoBeanPostProcessor implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if(bean instanceof DemoBean) {
            System.out.println("before: bean processor");
        }
        return Proxy.newProxyInstance(this.getClass().getClassLoader(), bean.getClass().getInterfaces(), (proxy, method, args) -> {
            Object o = method.invoke(bean, args);
            System.out.println("--------");
            return o;
        });
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if(bean instanceof DemoBean) {
            System.out.println("after: bean processor");
        }
        return bean;
    }
}
