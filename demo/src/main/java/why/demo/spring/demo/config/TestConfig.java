package why.demo.spring.demo.config;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.Date;

/**
 * @author why
 * @date 2019-06-03 13:11
 */
@Configuration
@ComponentScan(basePackages = "why.spring.demo.config.innerpackage")
public class TestConfig {

    @Bean
    public Date now() {
        return new Date();
    }

    @Bean
    public BeanPostProcessor newBeanPostProcessor() {
        System.out.println(now());
        return new BeanPostProcessor() {
            @Override
            public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
                System.out.println("before...");
                return bean;
            }

            @Override
            public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
                System.out.println("after...");
                return bean;
            }
        };
    }
}
