package why.demo.spring.demo.order;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

/**
 * todo desc
 * @author why
 * @date 2019-07-09 13:03
 */
@ComponentScan("why.spring.demo.order")
public class TestMain {

    public static void main(String[] args) {

        ApplicationContext context = new AnnotationConfigApplicationContext(TestMain.class);

        TestImpl impl = context.getBean(TestImpl.class);

        impl.test();
    }

}
