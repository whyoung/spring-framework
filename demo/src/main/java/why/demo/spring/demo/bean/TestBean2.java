package why.demo.spring.demo.bean;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.EnvironmentAware;

/**
 * todo desc
 * @author why
 * @date 2019-04-09 16:54
 */
public class TestBean2 {

    @Autowired
    private BeanFactory factory;

    @Autowired
    private EnvironmentAware environmentAware;

    public void test() {
        System.out.println(factory);
        System.out.println(environmentAware);
    }
}
