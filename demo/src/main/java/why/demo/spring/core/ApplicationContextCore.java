package why.demo.spring.core;

import org.springframework.beans.factory.BeanNameAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.GenericApplicationContext;

public class ApplicationContextCore {

    public static void main(String[] args) {

        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean(Config.class);

        context.refresh();
        System.out.println(context.getBean("config"));
    }

    @Configuration
    static class Config implements BeanNameAware {
        private String beanName;
        @Override
        public void setBeanName(String name) {
            this.beanName = name;
        }
    }
}
