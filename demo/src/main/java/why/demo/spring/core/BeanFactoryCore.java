package why.demo.spring.core;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;

public class BeanFactoryCore {

    public static void main(String[] args) {
        BeanFactory factory = new DefaultListableBeanFactory();
        System.out.println(factory.getBean("aaa"));
    }
}
