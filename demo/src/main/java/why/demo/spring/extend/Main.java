package why.demo.spring.extend;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author why
 * @date 2019-04-12 15:54
 */
public class Main {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(TestConfiguration.class);

        Set<Integer> set = new LinkedHashSet<>();
        set.addAll(Arrays.asList(3,2,1,4,3,1,2));
        set.add(1);
        System.out.println(set);
    }
}
