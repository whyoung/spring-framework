package why.demo.spring.extend;

import org.springframework.context.annotation.ComponentScan;

/**
 * @author why
 * @date 2019-04-12 15:52
 */
@ComponentScan(value = "why.spring.extend")
public class TestConfiguration {
}
