package why.demo.spring.extend;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;

/**
 * todo desc
 * @author why
 * @date 2019-04-12 15:42
 */
public class AppConfig implements BeanFactoryAware {

    private BeanFactory factory;

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.factory = beanFactory;
    }
}
