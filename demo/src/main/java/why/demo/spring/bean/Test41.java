package why.demo.spring.bean;

import org.aopalliance.intercept.MethodInterceptor;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.aop.aspectj.AspectJExpressionPointcut;
import org.springframework.aop.aspectj.annotation.AnnotationAwareAspectJAutoProxyCreator;
import org.springframework.aop.aspectj.autoproxy.AspectJAwareAdvisorAutoProxyCreator;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ConfigurationClassPostProcessor;
import org.springframework.context.support.GenericApplicationContext;

import java.lang.reflect.Field;

public class Test41 {

    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean(ConfigurationClassPostProcessor.class);
        context.registerBean(AspectJAwareAdvisorAutoProxyCreator.class);
        context.registerBean(AnnotationAwareAspectJAutoProxyCreator.class);
        context.registerBean(Config.class);
        context.refresh();

        TestBean iface = (TestBean) context.getBean("testBean");
        for (Field f : iface.getClass().getFields()) {
            System.out.println(f.getName() + ":" + f.getType().getSimpleName());
        }
        iface.test();
        System.out.println("-----------------------------------");
        iface.test2();
    }

    @Configuration
    static class Config {

        @Bean("testBean")
        public TestBean testBean() {
            return new TestBean();
        }

        @Bean
        public DefaultPointcutAdvisor pointcutAdvisor() {

            AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
            pointcut.setExpression("execution(* *.test())");

            MethodInterceptor advice = invocation -> {
                System.out.println("before...");
                Object result = invocation.proceed();
                System.out.println("after...");
                return result;
            };
            return new DefaultPointcutAdvisor(pointcut, advice);
        }

        @Bean
        public MyAspect myAspect() {
            return new MyAspect();
        }
    }

    interface Iface {
        void test();

        default void test2() {}
    }

    static class TestBean implements Iface {

        @Override
        public void test() {
            System.out.println("test");
        }

        @Override
        public void test2() {
            System.out.println("test2");
        }
    }

    @Aspect
    static class MyAspect {

        @Before("execution(* *.test2())")
        public void before() {
            System.out.println("before...");
        }
    }
}

