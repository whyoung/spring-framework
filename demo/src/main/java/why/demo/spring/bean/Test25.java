package why.demo.spring.bean;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test25 {

    /*
    ApplicationContext注册bean的方式：
    1.通过BeanFactoryPostProcessor
    2.Refreshable类型的ApplicationContext（通常是非GenericApplicationContext 及其子类） 可以再refresh的时候，通过bean definition reader加载bean definition到application
     */
    public static void main(String[] args) {
//        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
//        for (String name : context.getBeanDefinitionNames()) {
//            System.out.println(name);
//        }
//        context.close();

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        System.out.println(context.getBean("date"));
    }

    @Configuration
    @ComponentScan(basePackages = "why.demo.spring.bean.inner")
    static class Config {
    }
}
