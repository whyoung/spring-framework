package why.demo.spring.bean;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ConfigurationClassPostProcessor;
import org.springframework.context.support.GenericApplicationContext;

import java.util.Date;

public class Test9 {

    public static void main(String[] args) {

        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean(ConfigurationClassPostProcessor.class);
        context.registerBean(Config.class);
        for (String name : context.getBeanDefinitionNames()) {
            System.out.println(name);
        }
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        context.refresh();
        for (String name : context.getBeanDefinitionNames()) {
            System.out.println(name);
        }

        context.registerBean(Config2.class);
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        for (String name : context.getBeanDefinitionNames()) {
            System.out.println(name);
        }
        //refresh 之后不会再执行bean factory post processor
        //Configuration bean name 格式是完整的类名
        //        System.out.println(context.getBean("config2"));
        System.out.println(context.getBean(Config2.class));
        //NoSuchBeanDefinitionException
        ConfigurationClassPostProcessor processor = context.getBean(ConfigurationClassPostProcessor.class);
        processor.postProcessBeanDefinitionRegistry(context);
        System.out.println(context.getBean("date2"));
    }

    @Configuration
    static class Config {

        @Bean
        public Date date() {
            return new Date();
        }
    }


    @Configuration
    static class Config2 {

        @Bean("date2")
        public Date date() {
            return new Date();
        }
    }
}
