package why.demo.spring.bean;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ConfigurationClassPostProcessor;
import org.springframework.context.support.GenericApplicationContext;

public class Test43 {

    /*
    Configuration中的full模式下，同一个配置类里多个方法都返回同一个类型对象，
    会走正常的代码流程。
     */
    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean(ConfigurationClassPostProcessor.class);
        context.registerBean(Config.class);
        context.refresh();

        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

        for (String name : context.getBeanDefinitionNames()) {
            System.out.println(name + " = " + context.getBean(name));
        }

    }

    @Configuration
    static class Config {

        @Bean("testBean1")
        public TestBean testBean1() {
            return new TestBean();
        }

        @Bean("testBean2")
        protected TestBean testBean2(TestBean testBean1) {
            System.out.println(">>>>>>>  = " + testBean1);
            return new TestBean();
        }

        @Bean("testBean3")
        protected TestBean testBean3() {
            return testBean1();
        }
    }

    static class TestBean {
    }
}
