package why.demo.spring.bean;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.io.support.SpringFactoriesLoader;

public class Test7 {

    public static void main(String[] args) {
//        System.out.println(SpringFactoriesLoader.loadFactories(Iface.class, null));
//        System.out.println(SpringFactoriesLoader.loadFactories(Iface.class, null));
//        System.out.println(SpringFactoriesLoader.loadFactoryNames(Iface.class, null));

        GenericApplicationContext context = new GenericApplicationContext();

//        ConfigurableApplicationContext applicationContext = (ConfigurableApplicationContext) context;

        context.registerBean("bean1", Bean1.class);

        ApplicationContextInitializer<ConfigurableApplicationContext> initializer = applicationContext -> {
            GenericApplicationContext ctx = (GenericApplicationContext) applicationContext;
            ctx.registerBean("bean2", Bean1.class);
        };

        for (String name : context.getBeanDefinitionNames()) {
            System.out.println(name);
        }
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        context.refresh();
        initializer.initialize(context);
        for (String name : context.getBeanDefinitionNames()) {
            System.out.println(name);
        }
        System.out.println(context.getBean("bean2"));
    }

    static class Bean1 {

    }
}
