package why.demo.spring.bean;

import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.CommonAnnotationBeanPostProcessor;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ConfigurationClassPostProcessor;
import org.springframework.context.support.GenericApplicationContext;

public class Test45 {

    /*
    如果bean提供了close或者shutdown（无参），即使没有指定destroyMethod，在销毁前
    也会调用close或者shutdown方法，同时存在只会调用close
     */
    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean(ConfigurationClassPostProcessor.class);
        context.registerBean(AutowiredAnnotationBeanPostProcessor.class);
        context.registerBean(CommonAnnotationBeanPostProcessor.class);
        context.registerBean(Config.class);
        context.refresh();
        context.close();
    }

    @Configuration
    static class Config {

        @Bean
        public Bean1_1 bean1() {
            return new Bean1_1();
        }

        @Bean(destroyMethod = "")
        public Bean1_2 bean2() {
            return new Bean1_2();
        }
    }

    static class Bean1_1 {
        public void shutdown() {
            System.out.println("shot");
        }

        public void close() {
            System.out.println("closing");
        }

        public void close(int x) {
            System.out.println("closing " + x);
        }
    }

    static class Bean1_2 {

        public void shutdown() {
            System.out.println("shutdown...");
        }
    }
}
