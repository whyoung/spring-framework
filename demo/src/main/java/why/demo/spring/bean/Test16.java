package why.demo.spring.bean;

import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.annotation.InjectionMetadata;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.DependencyDescriptor;
import org.springframework.beans.factory.config.EmbeddedValueResolver;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ContextAnnotationAutowireCandidateResolver;
import org.springframework.core.MethodParameter;
import org.springframework.core.env.StandardEnvironment;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Test16 {
    public static void main(String[] args) throws Throwable {
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        beanFactory.setAutowireCandidateResolver(new ContextAnnotationAutowireCandidateResolver());
        beanFactory.registerSingleton("bean2", new Bean2());
        AutowiredAnnotationBeanPostProcessor processor = new AutowiredAnnotationBeanPostProcessor();
        processor.setBeanFactory(beanFactory);
//        beanFactory.addEmbeddedValueResolver(new StandardEnvironment()::resolvePlaceholders);
        PropertyValues values = new MutablePropertyValues();
        Bean1 bean1 = new Bean1();
        //解析Autowired 和 Value

//        Method method = AutowiredAnnotationBeanPostProcessor.class.getDeclaredMethod("findAutowiringMetadata", String.class, Class.class, PropertyValues.class);
//        method.setAccessible(true);
//        InjectionMetadata metadata = (InjectionMetadata) method.invoke(processor, "bean1", Bean1.class, values);
//        metadata.inject(bean1, "bean1", values);

        System.out.println(beanFactory.getBean("bean2"));
        DependencyDescriptor dd1 = new DependencyDescriptor(Bean1.class.getDeclaredField("bean2"), false);
        Bean2 bean2 = (Bean2) beanFactory.doResolveDependency(dd1, "bean1", null, null);
        System.out.println(bean2);


        Method method = Bean1.class.getDeclaredMethod("xxx", Bean2.class);
        DependencyDescriptor dd2 = new DependencyDescriptor(new MethodParameter(method, 0), false);
        bean2 = (Bean2) beanFactory.doResolveDependency(dd2, "bean2", null, null);
        System.out.println(bean2);

//        processor.postProcessProperties(values, bean1, "bean1");
//        System.out.println(bean1.bean2);
//        System.out.println(bean1.javaHome);

        DependencyDescriptor dd3 = new DependencyDescriptor(Bean1.class.getDeclaredField("javaHome"), false);
        System.out.println(beanFactory.doResolveDependency(dd3, null, null, null));
    }

    static class Bean1 {
        @Autowired
        private Bean2 bean2;

        @Value("${JAVA_HOME}")
        private String javaHome;

        @Autowired
        public void xxx(Bean2 bean2) {
            System.out.println(bean2);
        }
    }

    static class Bean2 {

    }
}
