package why.demo.spring.bean;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.BeanNameGenerator;
import org.springframework.context.annotation.*;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.Ordered;
import org.springframework.core.type.AnnotationMetadata;
import why.demo.spring.bean.inner.InnerBean1;
import why.demo.spring.bean.inner.InnerBean2;
import why.demo.spring.bean.inner.InnerBean3;
import why.demo.spring.bean.inner.InnerBean4;

public class Test22 {

    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean(Config.class);
        context.registerBean(ConfigurationClassPostProcessor.class);
        context.registerBean(MyBeanDefinitionPostProcessor.class);
        context.refresh();
//        for (String name : context.getBeanDefinitionNames()) {
//            System.out.println(name);
//        }
        context.close();
    }

    @Configuration
    @Import({MyImportSelector1.class, MyImportSelector2.class, MyImportSelector3.class, MyImportSelector5.class,
            MyBeanDefinitionRegistrar.class, Bean1.class})
    static class Config {

    }

    static class Bean1 {

    }

    @Configuration
    static class Config2 {

    }

    static class MyImportSelector1 implements ImportSelector {

        @Override
        public String[] selectImports(AnnotationMetadata importingClassMetadata) {
            System.out.println("注册一个普通 Configuration");
            return new String[]{Object.class.getName()};
        }
    }

    static class MyImportSelector2 implements ImportSelector {

        @Override
        public String[] selectImports(AnnotationMetadata importingClassMetadata) {
            System.out.println("注册ImportSelector");
            return new String[]{MyImportSelector4.class.getName()};
        }
    }

    static class MyImportSelector3 implements ImportSelector {

        @Override
        public String[] selectImports(AnnotationMetadata importingClassMetadata) {
            System.out.println("ImportSelector注册ImportBeanDefinitionRegistrar");
            return new String[]{MyBeanDefinitionRegistrar2.class.getName()};
        }
    }

    static class MyImportSelector4 implements ImportSelector, ImportBeanDefinitionRegistrar {

        @Override
        public String[] selectImports(AnnotationMetadata importingClassMetadata) {
            System.out.println("不注册，测试ImportSelector调用");
            return new String[0];
        }

        @Override
        public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
            System.out.println("这个方法不会执行");
        }
    }

    static class MyImportSelector5 implements DeferredImportSelector {

        @Override
        public String[] selectImports(AnnotationMetadata importingClassMetadata) {
            System.out.println("延迟注册DeferredImportSelector");
            return new String[0];
        }
    }

    static class MyBeanDefinitionRegistrar implements ImportBeanDefinitionRegistrar {

        @Override
        public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
            System.out.println("ImportBeanDefinitionRegistrar直接注册 Config2");
            BeanDefinition bd = BeanDefinitionBuilder.genericBeanDefinition(Config2.class).getBeanDefinition();
            registry.registerBeanDefinition("config2", bd);
        }
    }

    static class MyBeanDefinitionRegistrar2 implements ImportBeanDefinitionRegistrar {

        private final BeanNameGenerator beanNameGenerator = new AnnotationBeanNameGenerator();
        @Override
        public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
            System.out.println("通过ImportSelector间接注册ImportBeanDefinitionRegistrar");
            BeanDefinition bd = BeanDefinitionBuilder.genericBeanDefinition(InnerBean2.class).getBeanDefinition();
            registry.registerBeanDefinition(beanNameGenerator.generateBeanName(bd, registry), bd);
        }
    }

    static class MyBeanDefinitionPostProcessor implements BeanDefinitionRegistryPostProcessor, Ordered {

        @Override
        public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {

        }

        @Override
        public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
            System.out.println("register bean4");
            BeanDefinition bd = BeanDefinitionBuilder.genericBeanDefinition(InnerBean4.class).getBeanDefinition();
            registry.registerBeanDefinition(new AnnotationBeanNameGenerator().generateBeanName(bd, registry), bd);
        }

        @Override
        public int getOrder() {
            return 1;
        }
    }
}
