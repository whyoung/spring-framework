package why.demo.spring.bean;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class Test3 {

    public static void main(String[] args) {
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        beanFactory.registerBeanDefinition("bean1", BeanDefinitionBuilder.genericBeanDefinition(MyFactoryBean.class).addConstructorArgValue(Iface.class).getBeanDefinition());
        beanFactory.registerBeanDefinition("bean2", BeanDefinitionBuilder.genericBeanDefinition(MyFactoryBean.class).addConstructorArgValue(Iface2.class).getBeanDefinition());
        System.out.println(beanFactory.getBean("&bean1"));
        System.out.println(beanFactory.getBean("&bean2"));
        Iface iface = (Iface) beanFactory.getBean("bean1");
        Iface2 iface2 = (Iface2) beanFactory.getBean("bean2");
        iface.test(123, "test");
        iface2.test("1233");
        for (String name : beanFactory.getBeanDefinitionNames()) {
            System.out.println(name);
        }
//        System.out.println(beanFactory.getBean(MyFactoryBean.class));
    }

    static class MyFactoryBean<T> implements FactoryBean<T> {
        private final Class<T> clazz;

        public MyFactoryBean(Class<T> clazz) {
            this.clazz = clazz;
        }

        @Override
        @SuppressWarnings("uncheked")
        public T getObject() throws Exception {
            Object o = Proxy.newProxyInstance
                    (MyFactoryBean.class.getClassLoader(), new Class<?>[]{clazz}, new MyInvocationHandler(clazz));
            if (clazz.isAssignableFrom(o.getClass())) {
                return clazz.cast(o);
            }
            return null;
        }

        @Override
        public Class<?> getObjectType() {
            return clazz;
        }
    }

    interface Iface {
        void test(int i, String test);
    }

    interface Iface2 {
        void test(String test);
    }

    static class MyInvocationHandler implements InvocationHandler {

        private final Object o = new Object();
        private final Class<?> clazz;
        public MyInvocationHandler(Class<?> clazz) {
            this.clazz = clazz;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            try {
                clazz.getDeclaredMethod(method.getName(), method.getParameterTypes());
                System.out.println("proxy method:" + method.getName());
                if (args != null) {
                    for (Object arg : args) {
                        System.out.println(arg);
                    }
                }
                return null;
            } catch (Exception ignore) {
                return method.invoke(o, args);
            }
        }
    }
}

