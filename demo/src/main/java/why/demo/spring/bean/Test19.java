package why.demo.spring.bean;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.context.annotation.AnnotationBeanNameGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ConfigurationClassPostProcessor;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.type.ClassMetadata;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import why.demo.spring.bean.mapper.Mapper1;

import java.io.IOException;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

public class Test19 {

    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean(Config.class);
        context.registerBean(ConfigurationClassPostProcessor.class);
        for (String name : context.getBeanDefinitionNames()) {
            System.out.println(name);
        }
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//        context.registerBean(MapperScanBeanFactoryPostProcessor.class);
        context.refresh();

        for (String name : context.getBeanDefinitionNames()) {
            System.out.println(name);
        }
    }

    @Configuration
    @MapperScan("why.demo.spring.bean.mapper")
    static class Config {

        @Bean
        public SqlSessionFactory sqlSessionFactory() {
            return new SqlSessionFactory();
        }

        @Bean
        public MapperFactoryBean mapper1() {
            MapperFactoryBean mapper1 = new MapperFactoryBean(Mapper1.class);
            return mapper1;
        }
    }

    static class SqlSessionFactory {

    }

    static class MapperScanBeanFactoryPostProcessor implements BeanDefinitionRegistryPostProcessor {

        @Override
        public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {

        }

        @Override
        public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
            MapperScan annotation = AnnotationUtils.findAnnotation(Config.class, MapperScan.class);
            if (annotation != null) {
                String basePackage = annotation.value();
                PathMatchingResourcePatternResolver matchingResourcePatternResolver =
                        new PathMatchingResourcePatternResolver();
                AnnotationBeanNameGenerator nameGenerator = new AnnotationBeanNameGenerator();
                CachingMetadataReaderFactory readerFactory = new CachingMetadataReaderFactory();
                String path = "classpath*:" + basePackage.replace(".", "/") + "/**/*.class";

                try {
                    Resource[] resources = matchingResourcePatternResolver.getResources(path);
                    for (Resource resource : resources) {
                        MetadataReader reader = readerFactory.getMetadataReader(resource);
                        ClassMetadata classMetadata = reader.getClassMetadata();
                        if (classMetadata.isInterface()) {
                            AbstractBeanDefinition bd = BeanDefinitionBuilder.genericBeanDefinition(MapperFactoryBean.class)
                                    .addConstructorArgValue(classMetadata.getClassName())
                                    .setAutowireMode(AbstractBeanDefinition.AUTOWIRE_BY_TYPE)
                                    .getBeanDefinition();

                            AbstractBeanDefinition nameBd = BeanDefinitionBuilder.genericBeanDefinition(classMetadata.getClassName()).getBeanDefinition();
                            String name = nameGenerator.generateBeanName(nameBd, registry);
                            registry.registerBeanDefinition(name, bd);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    static class MapperFactoryBean implements FactoryBean<Object> {

        private Class<?> iface;

        private SqlSessionFactory sqlSessionFactory;

        public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
            this.sqlSessionFactory = sqlSessionFactory;
        }

        public MapperFactoryBean(Class<?> iface) {
            this.iface = iface;
        }

        @Override
        public Object getObject() throws Exception {
            return new Object();
        }

        @Override
        public Class<?> getObjectType() {
            return iface;
        }
    }

    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    @interface MapperScan {
        String value();
    }
}
