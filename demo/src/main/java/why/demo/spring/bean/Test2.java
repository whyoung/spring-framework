package why.demo.spring.bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.config.DependencyDescriptor;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;

import javax.annotation.Resource;
import java.lang.reflect.Field;

public class Test2 {

    public static void main(String[] args) throws Exception {
        DefaultListableBeanFactory factory = new DefaultListableBeanFactory();
        factory.registerSingleton("bean1", new Bean2_1());
        factory.registerSingleton("bean2", new Bean2_2());

        AutowiredAnnotationBeanPostProcessor processor = new AutowiredAnnotationBeanPostProcessor();
        processor.setBeanFactory(factory);
//
//        Bean2_1 bean1 = factory.getBean(Bean2_1.class);
//        processor.postProcessProperties(null, bean1, "bean1");

        Field field = Bean2_1.class.getDeclaredField("bean2");
        field.setAccessible(true);
        DependencyDescriptor dd1 = new DependencyDescriptor(field, true);
        Object bean2 = factory.doResolveDependency(dd1, null, null, null);
        System.out.println(bean2);
    }

    static class Bean2_1 {

        @Autowired
        private Bean2_2 bean2;
    }

    static class Bean2_2 {

        @Resource
        private Bean2_1 bean1;
    }
}
