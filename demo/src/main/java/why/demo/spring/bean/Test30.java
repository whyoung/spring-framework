package why.demo.spring.bean;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.CommonAnnotationBeanPostProcessor;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ConfigurationClassPostProcessor;
import org.springframework.context.support.GenericApplicationContext;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

public class Test30 {

    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean(Config.class);
        context.registerBean(MyBeanPostProcessor.class);
        context.registerBean(ConfigurationClassPostProcessor.class);
        context.registerBean(AutowiredAnnotationBeanPostProcessor.class);
        context.registerBean(CommonAnnotationBeanPostProcessor.class);
        context.refresh();
    }

    @Configuration
    static class Config {
        @Bean
        public Bean1 bean1() {
            return new Bean1();
        }

        @Bean
        public Bean3 bean3() {
            return new Bean3();
        }

        @Bean(initMethod = "init2")
        public Bean2 bean2() {
            return new Bean2();
        }
    }

    static class Bean1 {
    }

    static class Bean3 {
    }

    static class Bean2 implements ApplicationContextAware, InitializingBean {
        public Bean2() {
            System.out.println("Bean2 Constructor");
        }

        @Autowired
        private void setBean1(Bean1 bean1) {
            System.out.println("inject bean1");
        }

        @Resource
        private void setBean3(Bean3 bean3) {
            System.out.println("inject bean3");
        }

        @Override
        public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
            System.out.println("ApplicationContextAware");
        }

        @Override
        public void afterPropertiesSet() throws Exception {
            System.out.println("afterPropertiesSet");
        }

        @PostConstruct
        public void init() {
            System.out.println("PostConstruct");
        }

        public void init2() {
            System.out.println("@Bean#initMethod");
        }
    }

    static class MyBeanPostProcessor implements BeanPostProcessor {
        @Override
        public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
            if (bean instanceof Bean2)
                System.out.println("postProcessBeforeInitialization");
            return bean;
        }

        @Override
        public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
            if (bean instanceof Bean2)
                System.out.println("postProcessAfterInitialization");
            return bean;
        }
    }
}
