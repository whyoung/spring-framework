package why.demo.spring.bean;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.*;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;
import org.springframework.core.type.AnnotationMetadata;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Date;
import java.util.Map;

public class Test27 {

    /*
    Conditional条件注解的使用
     */
    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
        applicationContext.registerBean(ConfigurationClassPostProcessor.class);
        applicationContext.registerBean(Config.class);
        applicationContext.refresh();
        for (String name : applicationContext.getBeanDefinitionNames()) {
            System.out.println(name);
        }
    }

    static class MySelector implements ImportSelector {

        @Override
        public String[] selectImports(AnnotationMetadata importingClassMetadata) {
            return new String[]{Config1.class.getName(), Config2.class.getName(), Config3.class.getName()};
//            return new String[]{Config2.class.getName(), Config3.class.getName()};
        }
    }

    @Configuration
    @Import(MySelector.class)
    static class Config {
    }

    @Configuration
    static class Config1 {

        @Bean
        public Object bean1() {
            return new Object();
        }
    }

    @Configuration
    @Conditional(MyCondition.class)
    static class Config2 {

        @Bean
        public Object bean2() {
            return new Object();
        }
    }

    @Target({ElementType.TYPE, ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    @Conditional(MyCondition.class)
    @interface ConditionOnBeanMissing {
        String name() default "";
        //alias for name
        String value() default "";
    }

    @Configuration
    static class Config3 {

        @Bean
        @ConditionOnBeanMissing("bean1")
        public Date bean3() {
            return new Date();
        }
    }

    static class MyCondition implements Condition {

        @Override
        public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
            /*
            获取 bean factory 和 environment相关信息
             */
            ConfigurableBeanFactory beanFactory = context.getBeanFactory();
            Environment environment = context.getEnvironment();

            //获取注解相关的信息
            Map<String, Object> attributes = metadata.getAnnotationAttributes(ConditionOnBeanMissing.class.getName());
            if (attributes == null) {
                return true;
            }
            String name = (String) attributes.get("name");
            if (name == null) {
                return true;
            }
            if ("".equals(name)) {
                name = (String) attributes.get("value");
            }
            if ("".equals(name)) return true;
            assert beanFactory != null;
            return !beanFactory.containsBean(name);
        }
    }
}
