package why.demo.spring.bean;

import org.springframework.core.env.SimpleCommandLinePropertySource;

public class Test8 {

    public static void main(String[] args) {
        SimpleCommandLinePropertySource propertySource = new SimpleCommandLinePropertySource("--abc=ABC debug=true");
        System.out.println(propertySource.getProperty("abc"));
        System.out.println(propertySource.getProperty("debug"));

    }
}
