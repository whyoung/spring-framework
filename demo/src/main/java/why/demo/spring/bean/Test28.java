package why.demo.spring.bean;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.CommonAnnotationBeanPostProcessor;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ConfigurationClassPostProcessor;
import org.springframework.context.support.GenericApplicationContext;

import javax.annotation.PostConstruct;


public class Test28 {

    /*
    FactoryBean
     */
    public static void main(String[] args) {

        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean(ConfigurationClassPostProcessor.class);
        context.registerBean(CommonAnnotationBeanPostProcessor.class);
        context.registerBean(MyBeanPostProcessor.class);
        context.registerBean(Config.class);
        context.refresh();

        for (String name : context.getBeanDefinitionNames()) {
            System.out.println(name + "=" + context.getBean(name));
        }
        System.out.println(context.getBean("&bean1"));
        Bean1 bean1 = (Bean1) context.getBean("bean1");
        bean1 = context.getBean(Bean1.class);
        context.close();
    }

    @Configuration
    static class Config {
        /*
        FactoryBean在注册到bean factory和普通的bean没有区别，初始化流程也都会执行
        @Bean 注解的 initMethod 是FactoryBean 类定义的方法
         */
        @Bean(value = "bean1", initMethod = "init2")
        public MyFactoryBean myFactoryBean() {
            return new MyFactoryBean();
        }

        @Bean
        public Bean2 bean2() {
            return new Bean2();
        }
    }

    static class MyFactoryBean implements FactoryBean<Bean1> {

        @Override
        public Bean1 getObject() throws Exception {
            return new Bean1();
        }

        @Override
        public Class<?> getObjectType() {
            //类型返回null，对根据名字获取bean无影响，但对根据类型获取bean去不到
            return Bean1.class;
        }

        public void init2() {
            System.out.println("factory bean init method");
        }
    }

    /*
    对于由FactoryBean创建出来的bean，

     */
    static class Bean1 implements InitializingBean {

        public Bean1() {
            System.out.println("bean1 实例化...");
//            printStackTrace();
        }

        @PostConstruct
        public void init() {
            System.out.println("PostConstruct");
        }

        public void init2() {
            System.out.println("bean init method");
        }

        @Override
        public void afterPropertiesSet() throws Exception {
            System.out.println("InitializingBean");
        }
    }

    static class Bean2 {
        public Bean2() {
            System.out.println("bean2 实例化...");
        }
    }

    static class MyBeanPostProcessor implements InstantiationAwareBeanPostProcessor {

        @Override
        public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
            if (beanName.startsWith("bean"))
                System.out.println(beanName + " 初始化前...");
            return bean;
        }

        @Override
        public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
            if (beanName.startsWith("bean"))
                System.out.println(beanName + " 初始化后...");
            return InstantiationAwareBeanPostProcessor.super.postProcessAfterInitialization(bean, beanName);
        }

        @Override
        public Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException {
            if (beanName.startsWith("bean"))
                System.out.println(beanName + " 实例化前...");
            return InstantiationAwareBeanPostProcessor.super.postProcessBeforeInstantiation(beanClass, beanName);
        }

        @Override
        public boolean postProcessAfterInstantiation(Object bean, String beanName) throws BeansException {
            if (beanName.startsWith("bean"))
                System.out.println(beanName + " 实例化后...");
            return InstantiationAwareBeanPostProcessor.super.postProcessAfterInstantiation(bean, beanName);
        }
    }
}
