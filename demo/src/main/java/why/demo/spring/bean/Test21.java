package why.demo.spring.bean;

import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ConfigurationClassPostProcessor;
import org.springframework.context.annotation.Import;
import why.demo.spring.bean.inner.InnerBean1;

import java.util.Map;

public class Test21 {

    public static void main(String[] args) {
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        beanFactory.registerBeanDefinition(ConfigurationClassPostProcessor.class.getSimpleName(), BeanDefinitionBuilder.genericBeanDefinition(ConfigurationClassPostProcessor.class)
                .getBeanDefinition());
        beanFactory.registerBeanDefinition("config", BeanDefinitionBuilder.genericBeanDefinition(Config.class).getBeanDefinition());
        for (String name : beanFactory.getBeanDefinitionNames()) {
            System.out.println(name);
        }
        Map<String, BeanFactoryPostProcessor> beanFactoryPostProcessorMap = beanFactory.getBeansOfType(BeanFactoryPostProcessor.class);
        beanFactoryPostProcessorMap.values().forEach(processor -> {
            processor.postProcessBeanFactory(beanFactory);
        });
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

        for (String name : beanFactory.getBeanDefinitionNames()) {
            System.out.println(name);
        }
    }

    @Configuration
    static class Config {
        @Bean
        public InnerBean1 bean1() {
            return new InnerBean1();
        }
    }


}
