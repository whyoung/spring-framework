package why.demo.spring.bean;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.CommonAnnotationBeanPostProcessor;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ConfigurationClassPostProcessor;
import org.springframework.context.support.GenericApplicationContext;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * 初始化/销毁顺序
 */
public class Test5 {

    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean(ConfigurationClassPostProcessor.class);  //Configuration & Bean
        context.registerBean(CommonAnnotationBeanPostProcessor.class); //PostConstruct
        context.registerBean(Config.class);
        context.refresh();
        context.close();
    }


    @Configuration
    static class Config {

        /*
        初始化/销毁顺序
        1. jsr注解
        2. 实现 InitializingBean/DisposableBean 接口，如果类还实现了aware接口，aware的回调是在此之前完成调用
        3. Bean 注解上的 initMethod 和 destroyMethod 属性
         */
        @Bean(initMethod = "beanInit", destroyMethod = "beanDestroy")
        public Bean5_1 bean1() {
            return new Bean5_1();
        }
    }
    static class Bean5_1 implements InitializingBean, DisposableBean {

        public void beanInit() {
            System.out.println("bean init");  //3
        }

        @PostConstruct
        public void postConstructInit() {
            System.out.println("post construct init");  //1
        }

        @Override
        public void afterPropertiesSet() throws Exception {
            System.out.println("initializing bean init");  //2
        }

        @Override
        public void destroy() throws Exception {
            System.out.println("disposable bean");
        }

        @PreDestroy
        public void preDestroy() {
            System.out.println("destroy bean");
        }

        public void beanDestroy() {
            System.out.println("bean destroy");
        }
    }
}
