package why.demo.spring.bean;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.type.MethodMetadata;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import why.demo.spring.bean.inner.InnerBean1;
import why.demo.spring.bean.inner.InnerBean2;

import java.io.IOException;
import java.util.Set;

public class Test18 {

    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean("config", Config.class);
        context.registerBean(MyBeanFactoryPostProcessor.class);
        context.refresh();

        for (String name : context.getBeanDefinitionNames()) {
            System.out.println(name);
        }


    }

    @Configuration
    static class Config {

        @Bean(initMethod = "init")
        public InnerBean1 bean1() {
            return new InnerBean1();
        }

        @Bean
        public InnerBean2 bean2(InnerBean1 bean1) {
            return new InnerBean2();
        }
    }


    static class MyBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

        @Override
        public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
            CachingMetadataReaderFactory readerFactory = new CachingMetadataReaderFactory();
            try {
                DefaultListableBeanFactory listableBeanFactory = (DefaultListableBeanFactory) beanFactory;
                MetadataReader reader = readerFactory.getMetadataReader(new ClassPathResource("why/demo/spring/bean/Test18$Config.class"));
                Set<MethodMetadata> methodsMetadata = reader.getAnnotationMetadata().getAnnotatedMethods(Bean.class.getName());
                for (MethodMetadata metadata : methodsMetadata) {
                    BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition();
                    builder.setFactoryMethodOnBean(metadata.getMethodName(), "config");
                    //构造方法和工厂方法都自动注入模式都是 AUTOWIRE_CONSTRUCTOR
                    builder.setAutowireMode(AbstractBeanDefinition.AUTOWIRE_CONSTRUCTOR);
                    String initMethod = metadata.getAnnotationAttributes(Bean.class.getName()).get("initMethod").toString();
                    if (!"".equals(initMethod)) {
                        builder.setInitMethodName(initMethod);
                    }
                    BeanDefinition bd = builder.getBeanDefinition();
                    listableBeanFactory.registerBeanDefinition(metadata.getMethodName(), bd);

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
