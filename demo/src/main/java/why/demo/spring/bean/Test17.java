package why.demo.spring.bean;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.EmbeddedValueResolverAware;
import org.springframework.context.annotation.AnnotationBeanNameGenerator;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.stereotype.Component;
import org.springframework.util.StringValueResolver;

import java.io.IOException;

public class Test17 {

    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean(Config.class);
        context.registerBean(MyComponentScanBeanFactoryPostProcessor.class);
        context.refresh();

        for (String name : context.getBeanDefinitionNames()) {
            System.out.println(name);
        }


    }

    @Configuration
    @ComponentScan(basePackages = "why.demo.spring.bean.inner")
    static class Config implements EmbeddedValueResolverAware {

        @Override
        public void setEmbeddedValueResolver(StringValueResolver resolver) {
            System.out.println(resolver.resolveStringValue("${JAVA_HOME}"));
            System.out.println(resolver.getClass());
        }
    }


    static class MyComponentScanBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

        @Override
        public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
            ComponentScan annotation = AnnotationUtils.findAnnotation(Config.class, ComponentScan.class);
            if (annotation != null) {
                String[] basePackages = annotation.basePackages();
                PathMatchingResourcePatternResolver matchingResourcePatternResolver =
                        new PathMatchingResourcePatternResolver();
                AnnotationBeanNameGenerator nameGenerator = new AnnotationBeanNameGenerator();
                CachingMetadataReaderFactory readerFactory = new CachingMetadataReaderFactory();
                for (String basePackage : basePackages) {
                    String path = "classpath*:" + basePackage.replace(".", "/") + "/**/*.class";
                    try {
                        Resource[] resources = matchingResourcePatternResolver.getResources(path);
                        for (Resource resource : resources) {
                            MetadataReader reader = readerFactory.getMetadataReader(resource);
                            AnnotationMetadata metadata = reader.getAnnotationMetadata();
                            if (metadata.hasAnnotation(Component.class.getName())
                                    || metadata.hasMetaAnnotation(Component.class.getName())) {
                                BeanDefinition bd = BeanDefinitionBuilder.genericBeanDefinition(metadata.getClassName()).getBeanDefinition();
                                DefaultListableBeanFactory listableBeanFactory = (DefaultListableBeanFactory) beanFactory;
                                listableBeanFactory.registerBeanDefinition(nameGenerator.generateBeanName(bd, listableBeanFactory), bd);
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
