package why.demo.spring.bean;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.Resource;

import java.io.IOException;

public class Test13 {

    public static void main(String[] args) throws IOException {

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
        context.publishEvent(new MyEvent("event"));


        Resource[] resources = context.getResources("classpath*:META-INF/spring.factories");
        for (Resource resource : resources) {
            System.out.println(resource.getURL());
        }

        System.out.println(context.getEnvironment().getProperty("JAVA_HOME"));
        System.out.println(context.getEnvironment().getProperty("abc"));
    }

    @Configuration
    static class Config {

        @Bean
        public ApplicationListener<MyEvent> listener() {
            return System.out::println;
        }

        @EventListener
        public void listen(MyEvent event) {
            System.out.println("config=" + event);
        }
    }

    static class MyEvent extends ApplicationEvent {
        private static final long serialVersionUID = 7099057708183571937L;
        public MyEvent(Object source) {
            super(source);
        }
    }

}
