package why.demo.spring.bean;

import org.springframework.beans.BeansException;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.DestructionAwareBeanPostProcessor;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.annotation.*;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.function.Predicate;

public class Test15 {

    public static void main(String[] args) {
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        AbstractBeanDefinition beanDefinition = BeanDefinitionBuilder.genericBeanDefinition(Config.class).getBeanDefinition();
        beanFactory.registerBeanDefinition("config", beanDefinition);

        BeanFactoryPostProcessor processor = new ConfigurationClassPostProcessor();
        processor.postProcessBeanFactory(beanFactory);

        beanFactory.addBeanPostProcessor(new MyBeanPostProcessor());
        AutowiredAnnotationBeanPostProcessor autowiredAnnotationBeanPostProcessor
                = new AutowiredAnnotationBeanPostProcessor();
        autowiredAnnotationBeanPostProcessor.setBeanFactory(beanFactory);
        beanFactory.addBeanPostProcessor(autowiredAnnotationBeanPostProcessor);

        //构造 —> 依赖注入 -> 初始化 -> 销毁
        //初始化和销毁阶段顺序

        /*
        1.jsr annotation（PostConstruct/PreDestroy）
        2.InitializingBean/DisposableBean
        3.Bean Annotation initMethod 和 destroyMethod
         */
//        context.close();
//        System.out.println(context.getBean(LifeCycleBean.class));
//        context.close();
        System.out.println(beanFactory.getBean("bean1"));
        System.out.println(beanFactory.getBean(LifeCycleBean.class));


    }

    @Configuration
    static class Config {
        @Bean
        public MyBeanPostProcessor myBeanPostProcessor() {
            return new MyBeanPostProcessor();
        }

        @Bean(initMethod = "init", destroyMethod = "destroy3")
        public LifeCycleBean lifeCycleBean() {
            return new LifeCycleBean();
        }

        @Bean
        public Bean1 bean1() {
            return new Bean1();
        }
    }

    static class Bean1 {
        public Bean1() {
            System.out.println("create bean1");
        }
    }

    static class LifeCycleBean implements InitializingBean, DisposableBean {

        public LifeCycleBean() {
            System.out.println("constructor");
        }

        @Autowired
        public void setBean(Bean1 bean) {
            System.out.println("autowired " + bean);
        }

        @Autowired
        public void setJavaHome(@Value("${JAVA_HOME}") String home) {
            System.out.println("autowired " + home);
        }

        public void init() {
            System.out.println("init");
        }

        @Override
        public void afterPropertiesSet() throws Exception {
            System.out.println("InitializingBean");
        }

        @PostConstruct
        public void init2() {
            System.out.println("post construct");
        }

        @PreDestroy
        public void destroy2() {
            System.out.println("pre destroy");
        }


        @Override
        public void destroy() throws Exception {
            System.out.println("DisposableBean");
        }

        public void destroy3() {
            System.out.println("destroy");
        }
    }

    static class MyBeanPostProcessor implements InstantiationAwareBeanPostProcessor, DestructionAwareBeanPostProcessor {

        private static final Predicate<String> predicate = "lifeCycleBean"::equals;

        @Override
        public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
            if (predicate.test(beanName)) {
                System.out.println("初始化前。。。");
            }
            return InstantiationAwareBeanPostProcessor.super.postProcessBeforeInitialization(bean, beanName);
        }

        @Override
        public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
            if (predicate.test(beanName)) {
                System.out.println("初始化后。。。");
            }
            return InstantiationAwareBeanPostProcessor.super.postProcessAfterInitialization(bean, beanName);
        }

        @Override
        public void postProcessBeforeDestruction(Object bean, String beanName) throws BeansException {
            if (predicate.test(beanName))
                System.out.println("销毁前。。。");
        }


        @Override
        public Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException {
            if (predicate.test(beanName))
                System.out.println("实例化前。。。");
            return InstantiationAwareBeanPostProcessor.super.postProcessBeforeInstantiation(beanClass, beanName);
        }

        @Override
        public boolean postProcessAfterInstantiation(Object bean, String beanName) throws BeansException {
            if (predicate.test(beanName)) {
                System.out.println("实例化后。。。");
//                return false;
            }
            //如果返回false将不执行依赖注入
            return InstantiationAwareBeanPostProcessor.super.postProcessAfterInstantiation(bean, beanName);
        }


        @Override
        public PropertyValues postProcessProperties(PropertyValues pvs, Object bean, String beanName) throws BeansException {
            if (predicate.test(beanName)) {
                System.out.println("依赖注入。。。");
            }
            return InstantiationAwareBeanPostProcessor.super.postProcessProperties(pvs, bean, beanName);
        }
    }
}
