package why.demo.spring.bean;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Date;

public class Test10 {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext10.xml");
        for (String name : context.getBeanDefinitionNames()) {
            System.out.println(name);
        }
//
//        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext();
//        System.out.println(context.getBean("abc"));

//        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
//        context.getBean("abc");
//        context = new GenericApplicationContext();
//        //IllegalStateException
//        context.getBean("abc");
//        ConfigurableListableBeanFactory beanFactory = context.getBeanFactory();
//        beanFactory.getBean("abc");
    }

    @Configuration
    static class Config {

        @Bean
        public Date date() {
            return new Date();
        }
    }

}
