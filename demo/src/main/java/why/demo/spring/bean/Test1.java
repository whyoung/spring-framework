package why.demo.spring.bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.CommonAnnotationBeanPostProcessor;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ConfigurationClassPostProcessor;
import org.springframework.context.support.GenericApplicationContext;

import javax.annotation.Resource;

public class Test1 {

    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean(ConfigurationClassPostProcessor.class);
        context.registerBean(AutowiredAnnotationBeanPostProcessor.class);
        context.registerBean(CommonAnnotationBeanPostProcessor.class);
        context.registerBean(Config.class);

        context.refresh();

        System.out.println(context.getBean(Bean1_2.class).bean1);
        System.out.println(context.getBean(Bean1_1.class).bean2);
        context.close();
    }

    @Configuration
    static class Config {

        @Bean
        public Bean1_1 bean1() {
            return new Bean1_1();
        }

        @Bean
        public Bean1_2 bean2() {
            return new Bean1_2();
        }
    }

    static class Bean1_1 {

        @Autowired
        private Bean1_2 bean2;
    }

    static class Bean1_2 {

        @Resource
        private Bean1_1 bean1;
    }
}
