package why.demo.spring.bean;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.aspectj.AspectJExpressionPointcut;
import org.springframework.aop.aspectj.autoproxy.AspectJAwareAdvisorAutoProxyCreator;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.beans.BeansException;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.*;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.Ordered;
import org.springframework.core.PriorityOrdered;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

public class Test34 {

    static int step = 0;

    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean(ConfigurationClassPostProcessor.class);
        context.registerBean(AutowiredAnnotationBeanPostProcessor.class);
        context.registerBean(CommonAnnotationBeanPostProcessor.class);
        context.registerBean(MyBeanPostProcessor.class);
        context.registerBean(MyBeanPostProcessor2.class);
        context.registerBean(MyBeanFactoryPostProcessor.class);
        context.registerBean(AspectJAwareAdvisorAutoProxyCreator.class);
        context.registerBean(Config.class);
        context.getDefaultListableBeanFactory().setAutowireCandidateResolver(new ContextAnnotationAutowireCandidateResolver());

        context.refresh();

        Bean1 bean1 = context.getBean(Bean1.class);
        System.out.println(bean1.bean3);
        System.out.println(bean1.javaHome);
        bean1.test();
    }

    @Configuration
    static class Config {

        @Bean(value = "bean1", initMethod = "init1")
        public Bean1 bean1() {
            return new Bean1();
        }

        @Bean(value = "bean2")
        public Bean2 bean2() {
            return new Bean2();
        }

        @Bean(value = "bean3")
        public Bean3 bean3() {
            return new Bean3();
        }

        @Bean
        public DefaultPointcutAdvisor pointcutAdvisor() {
            AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
            pointcut.setExpression("execution(* *.test())");

            MethodInterceptor advice = new MethodInterceptor() {
                @Override
                public Object invoke(MethodInvocation invocation) throws Throwable {
                    System.out.println("before...");
                    Object result = invocation.proceed();
                    System.out.println("after...");
                    return result;
                }
            };
            return new DefaultPointcutAdvisor(pointcut, advice);
        }
    }

    static class Bean1 implements InitializingBean, BeanFactoryAware, ApplicationContextAware {
        public Bean1() {
            printStep("实例化");
        }

        @Resource
        private Bean3 bean3;

        @Value("${JAVA_HOME}")
        private String javaHome;

        @Autowired
        public void setValue(@Value("${JAVA_HOME}") String home) {
            printStep(this, "@Autowired + @Value 注入配置");
        }

        @Autowired
        public void setBean(Bean2 bean2) {
            printStep(this, "@Autowired 注入bean");
        }

        public void init1() {
            printStep(this,"@Bean init-method 初始化");
        }

        @PostConstruct
        public void init2() {
            printStep(this, "@PostConstruct 初始化");
        }

        @Override
        public void afterPropertiesSet() {
            printStep(this,"@InitializingBean 初始化");
        }

        @Override
        public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
            printStep(this, "BeanFactoryAware 回调");
        }

        @Override
        public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
            printStep(this, "ApplicationContextAware 回调");
        }

        public void test() {
            System.out.println("test");
        }
    }

    static class Bean2 {
    }

    static class Bean3 {
    }

    static class MyBeanPostProcessor implements InstantiationAwareBeanPostProcessor, PriorityOrdered {
        @Override
        public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
            if ("bean1".equals(beanName))
                printStep(bean, "BeanPostProcessor bean初始化前");
            return bean;
        }

        @Override
        public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
            if ("bean1".equals(beanName))
                printStep(bean, "BeanPostProcessor bean初始化后");
            return bean;
        }

        @Override
        public Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException {
            if ("bean1".equals(beanName)) {
                printStep("BeanPostProcessor bean实例化前");
//                return new Bean1();  //即使在这一步返回，依然能创建代理对象
            }
            return null;
        }

        @Override
        public boolean postProcessAfterInstantiation(Object bean, String beanName) throws BeansException {
            if ("bean1".equals(beanName))
                printStep(bean,"BeanPostProcessor bean实例化后");
            return true;
        }

        @Override
        public PropertyValues postProcessProperties(PropertyValues pvs, Object bean, String beanName) throws BeansException {
            if ("bean1".equals(beanName))
                printStep(bean, "BeanPostProcessor bean 属性后置处理");
            return pvs;
        }

        @Override
        public int getOrder() {
            return Integer.MAX_VALUE;
        }
    }

    static class MyBeanPostProcessor2 implements BeanPostProcessor {
        @Override
        public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
            if ("bean1".equals(beanName)) {
                printStep(bean, "BeanPostProcessor bean初始化前");
            }
            return bean;
        }

        @Override
        public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
            if ("bean1".equals(beanName)) {
                printStep(bean,"BeanPostProcessor bean初始化后");
            }
            return bean;
        }
    }

    static class MyBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

        @Override
        public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
            printStep("BeanFactoryPostProcessor 后置处理器");
        }
    }
    static void printStep(String msg) {
        System.out.println("第【" + (++step) + "】步：" + msg);
    }

    static void printStep(Object bean, String msg) {
        System.out.println("第【" + (++step) + "】步：" + msg + "，当前bean类型是" + bean.getClass().getSimpleName());
    }

}
