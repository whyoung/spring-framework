package why.demo.spring.bean;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.context.annotation.ConfigurationClassPostProcessor;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.support.GenericApplicationContext;

public class Test23 {

    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean(ConfigurationClassPostProcessor.class);
        context.registerBean(AutowiredAnnotationBeanPostProcessor.class);
        context.registerBean(Bean2.class);
        BeanDefinition bd = BeanDefinitionBuilder.genericBeanDefinition(Bean1.class)
                .setScope("prototype").getBeanDefinition();
        context.registerBeanDefinition("bean1", bd);
        context.refresh();
        System.out.println(context.getBean(Bean2.class));
        System.out.println(context.getBean(Bean2.class));


    }

    static class Bean1 {

    }

    static class Bean2 {
        @Autowired
        @Lazy
        private Bean1 bean1;

        @Autowired
        private ObjectFactory<Bean1> bean1Factory;

        @Override
        public String toString() {
            return "Bean2{" +
                    "bean1=" + bean1 +
                    ", bean1=" + bean1Factory.getObject() +
                    '}';
        }
    }

}
