package why.demo.spring.bean;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.DependencyDescriptor;
import org.springframework.beans.factory.support.SimpleAutowireCandidateResolver;
import org.springframework.context.annotation.CommonAnnotationBeanPostProcessor;
import org.springframework.context.annotation.ConfigurationClassPostProcessor;
import org.springframework.context.annotation.ContextAnnotationAutowireCandidateResolver;
import org.springframework.context.support.GenericApplicationContext;

import javax.annotation.Resource;

public class Test44 {

    /*
    data binder
     */
    public static void main(String[] args) throws NoSuchFieldException {
        //@ConstructorProperties()
//        StandardEnvironment environment = new StandardEnvironment();
        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean(Config.class);
        context.registerBean(Bean1.class);
        context.registerBean(ConfigurationClassPostProcessor.class);
        context.registerBean(CommonAnnotationBeanPostProcessor.class);
        context.refresh();

        ContextAnnotationAutowireCandidateResolver resolver
                = new ContextAnnotationAutowireCandidateResolver();
        resolver.setBeanFactory(context.getBeanFactory());
//        context.getDefaultListableBeanFactory().setAutowireCandidateResolver();
        DependencyDescriptor dd = new DependencyDescriptor(Config.class.getDeclaredField("home"), false);

        SimpleAutowireCandidateResolver simpleResolver = new SimpleAutowireCandidateResolver();
        System.out.println(simpleResolver.getSuggestedValue(dd));

        Object value = resolver.getSuggestedValue(dd);
        System.out.println(value);

        String home = context.getEnvironment().resolvePlaceholders((String) value);
        System.out.println(home);
    }
//
//    @ConstructorProperties("")
    static class Config {

        @Value("${JAVA_HOME}")
        private String home;

        @Resource
        private Bean1 bean1;
    }

    static class Bean1 {

    }
}
