package why.demo.spring.bean;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ConfigurationClassPostProcessor;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.stereotype.Component;

public class Test42 {

    /*
    Configuration中的full和lite模式
     */
    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean(ConfigurationClassPostProcessor.class);
        context.registerBean(Config.class);
        context.refresh();

        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        System.out.println(context.getBean("testBean12"));
        TestBean1 testBean11 = (TestBean1) context.getBean("testBean11");
        System.out.println(testBean11);

        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        System.out.println(context.getBean("testBean22"));
        TestBean1 testBean21 = (TestBean1) context.getBean("testBean21");
        System.out.println(testBean21);

        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        Config config = context.getBean(Config.class);
        System.out.println(config.getClass());  //会创建cglib代理
        System.out.println(config.testBean1() == testBean11); //方法被代理，会先从bean factory中获取对象

        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        Config.InnerComponent component = (Config.InnerComponent) context.getBean("innerComponent");
        System.out.println(component.getClass()); //不会创建配置类的代理
        System.out.println(component.testBean1() == testBean21); //方法不会被代理

        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        for (String name : context.getBeanDefinitionNames()) {
            System.out.println(name + " = " + context.getBean(name));
        }

    }

    /*
        full模式下会生成代理，@Bean注解的方法会被代理，先从bean factory中获取
        方法不能是private和final修饰
    */
    @Configuration
    static class Config {

        @Bean("testBean11")
        public TestBean1 testBean1() {
            System.out.println(">>>>>>> 12 = " + testBean2());
            TestBean1 testBean1 = new TestBean1();
            System.out.println(">>>>>>> 11 = " + testBean1);
            return testBean1;
        }

        @Bean("testBean12")
        TestBean2 testBean2() {
            //方法会被代理
            return new TestBean2();
        }

        @Bean("testBean13")
        protected TestBean1 testBean13(TestBean2 testBean12) {
            System.out.println(">>>>>>> 13 = " + testBean12);
            return new TestBean1();
        }

        @Bean("testBean14")
        protected TestBean1 testBean14() {
            return testBean1();
        }

        @Component("innerComponent")
        static class InnerComponent {
            /*
            lite模式不会生成代理
             */
            @Bean("testBean21")
            private TestBean1 testBean1() {
                System.out.println(">>>>>>> 22 = " + testBean2());
                TestBean1 testBean1 = new TestBean1();
                System.out.println(">>>>>>> 21 = " + testBean1);
                return testBean1;
            }

            @Bean("testBean22")
            final TestBean2 testBean2() {
                return new TestBean2();
            }

            @Bean("testBean23")
            final TestBean1 testBean23(TestBean2 testBean22) {
                System.out.println(">>>>>>> 23 = " + testBean22);
                return new TestBean1();
            }

            @Bean("testBean24")
            protected TestBean1 testBean24() {
                return testBean1();
            }
        }
    }

    static class TestBean1 {
    }

    static class TestBean2 {
    }
}
