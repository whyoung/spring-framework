package why.demo.spring.bean;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.CommonAnnotationBeanPostProcessor;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ConfigurationClassPostProcessor;
import org.springframework.context.support.GenericApplicationContext;

import javax.annotation.PostConstruct;

/**
 * 注解失效 & aware接口方式解决
 */
public class Test4 {

    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean(Config.class);
        context.registerBean(Bean4_2.class);
        context.registerBean(ConfigurationClassPostProcessor.class);
        context.registerBean(AutowiredAnnotationBeanPostProcessor.class);
        context.registerBean(CommonAnnotationBeanPostProcessor.class);
        context.refresh();

        System.out.println(context.getBean(Config.class).bean2 == null);
        context.close();
    }

    @Configuration
    static class Config implements InitializingBean {

        @Autowired
        private Bean4_2 bean2;

        @PostConstruct
        public void init() {
            System.out.println(this.getClass() + " post construct init");
        }

        @Override
        public void afterPropertiesSet() throws Exception {
            System.out.println("initializing bean init");
        }

        @Bean
        public Bean4_1 bean1() {
            return new Bean4_1();
        }

        @Bean
        public BeanFactoryPostProcessor processor() {
            //如果添加了这个bean，Config的先于BeanPostProcessor实例化
            //PostConstruct、Autowired等依赖 BeanPostProcessor 的注解不会被解析，实例化方法也就不会被执行，也无法注入依赖
            //这个时候，需要借助Aware或者 InitializingBean 等方式来代替注解的方式
            /*
            application context refresh:
            1. 执行 bean factory post processor（可以修改bean definition）
            2. 注册 bean post processor（影响bean的）
            3. 初始化单例bean
             */
            return beanFactory -> System.out.println("create bean factory post processor");
        }
    }

    static class Bean4_1 {
        @PostConstruct
        public void init() {
            System.out.println(this.getClass() + " post construct init");
        }
    }

    static class Bean4_2 {
        @PostConstruct
        public void init() {
            System.out.println(this.getClass() + " post construct init");
        }
    }
}
