package why.demo.spring.bean;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.CommonAnnotationBeanPostProcessor;
import org.springframework.context.support.GenericApplicationContext;

import javax.annotation.PostConstruct;

public class Test20 {


    public static void main(String[] args) {

        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean(CommonAnnotationBeanPostProcessor.class);
        context.registerBean(MyBean.class);

        context.refresh();

//        System.out.println(context.getBean(MyBean.class));
        context.close();

//        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
//        BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(MyBean.class);
//        beanFactory.registerBeanDefinition("myBean", builder.getBeanDefinition());
////        beanFactory.registerBeanDefinition("ApplicationContextAwareProcessor", BeanDefinitionBuilder.genericBeanDefinition(ApplicationContextAwareProcessor.class).getBeanDefinition());
//        beanFactory.getBean(MyBean.class);


    }

    /*
    BeanNameAware、InitializingBean 这些 BeanFactory也会执行
    ApplicationContextAware 只有用ApplicationContext才会执行

    有些是ApplicationContext才通过的Aware 接口
    org.springframework.context.EnvironmentAware
    org.springframework.context.EmbeddedValueResolverAware
    org.springframework.context.ResourceLoaderAware
    org.springframework.context.ApplicationEventPublisherAware
    org.springframework.context.MessageSourceAware
    org.springframework.context.ApplicationContextAware
    是通过 ApplicationContextAwareProcessor 这个bean post processor 实现的，注意这个类是未暴露的，只用于内部使用
     */
    static class MyBean implements BeanNameAware, ApplicationContextAware, InitializingBean {

        @PostConstruct
        public void init() {
            printStackTrace();
            System.out.println("PostConstruct init");
        }

        @Override
        public void setBeanName(String name) {
            printStackTrace();
            System.out.println("name of MyBean is " + name);
        }

        @Override
        public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
            printStackTrace();
            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            for (String name : applicationContext.getBeanDefinitionNames()) {
                System.out.println(name);
            }
        }

        @Override
        public void afterPropertiesSet() throws Exception {
            printStackTrace();
            System.out.println("init...");
        }
    }

    private static void printStackTrace() {
        StackTraceElement[] elements = Thread.currentThread().getStackTrace();  //线程堆栈，从栈底到栈顶构成的数组
        //栈顶栈帧即调用方
        StackTraceElement last = elements[elements.length - 1];
//        String className = last.getClassName();   //类
//        String methodName = last.getMethodName(); //方法
//        int lineNumber=last.getLineNumber();      //代码行
        StringBuilder sb = new StringBuilder();
        for (int i = elements.length - 1; i>= 0; i--) {
            StackTraceElement e = elements[i];
            System.out.println(sb + e.getClassName() + "#" + e.getMethodName());
            sb.append("\t");
        }
    }


}
