package why.demo.spring.bean;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.aspectj.AspectJExpressionPointcut;
import org.springframework.aop.aspectj.autoproxy.AspectJAwareAdvisorAutoProxyCreator;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ConfigurationClassPostProcessor;
import org.springframework.context.annotation.ContextAnnotationAutowireCandidateResolver;
import org.springframework.context.support.GenericApplicationContext;

public class Test39 {

    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean(ConfigurationClassPostProcessor.class);
        context.registerBean(AspectJAwareAdvisorAutoProxyCreator.class);
        context.registerBean(MyBeanPostProcessor.class);
        context.registerBean(Config.class);
        context.getDefaultListableBeanFactory().setAutowireCandidateResolver(new ContextAnnotationAutowireCandidateResolver());

        context.refresh();

        Bean1 bean1 = context.getBean(Bean1.class);
        /*
        对于代理bean，如果要访问目标对象的属性，必须通过方法获取
        bean的属性注入是针对目标对象的，代理对象持有目标对象的引用
        方法调用实际执行的super.xxx，实际上还是调用的目标对象的方法，而对于属性是获取的代理对象的属性
         */
        System.out.println(bean1.javaHome); //直接访问代理对象的属性，获取不到
        System.out.println(bean1.getJavaHome()); //直接访问代理对象的属性，获取不到
        bean1.test();
    }

    @Configuration
    static class Config {

        @Bean(value = "bean1")
        public Bean1 Bean1() {
            return new Bean1();
        }

        @Bean
        public DefaultPointcutAdvisor pointcutAdvisor() {
            AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
            pointcut.setExpression("execution(* *.test())");

            MethodInterceptor advice = new MethodInterceptor() {
                @Override
                public Object invoke(MethodInvocation invocation) throws Throwable {
                    System.out.println("before...");
                    Object result = invocation.proceed();
                    System.out.println("after...");
                    return result;
                }
            };
            return new DefaultPointcutAdvisor(pointcut, advice);
        }
    }

    static class Bean1 {
        public Bean1() {
        }

        @Value("${JAVA_HOME}")
        public String javaHome;

        public String getJavaHome() {
            return javaHome;
        }

        public void test() {
            System.out.println("从属性获取：" + javaHome);
            System.out.println("通过get方法获取：" + getJavaHome());
        }
    }

    //优先级设置最低（不实现Ordered接口将最后执行，确保位于Proxy之后）
    static class MyBeanPostProcessor implements BeanPostProcessor {
        @Override
        public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
            if ("bean1".equals(beanName)) {
                System.out.println(bean.getClass().getSimpleName() + " 初始化前");
            }
            return bean;
        }

        @Override
        public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
            if ("bean1".equals(beanName)) {
                //这里传入的实际就是代理对象了
                System.out.println(bean.getClass().getSimpleName() + " 初始化后");
            }
            return bean;
        }
    }

}
