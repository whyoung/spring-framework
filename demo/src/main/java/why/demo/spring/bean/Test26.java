package why.demo.spring.bean;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.Aware;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

public class Test26 {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
    }

    @Configuration
    static class Config implements InitializingBean {

        public Config() {
            //实例化在工厂方法的Bean之前
            System.out.println("Config Instance");
        }
        @PostConstruct
        public void init() {
            //不会执行
            System.out.println("Config Post Construct init");
        }


        @Bean
        public BeanFactoryPostProcessor myFactoryPostProcessor() {
            {
                System.out.println("Bean Factory Post Processor init");
            }

            return new BeanFactoryPostProcessor() {
                @Override
                public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
                    System.out.println(beanFactory);
                }
            };
        }

        @Bean
        public BeanPostProcessor myBeanPostProcessor() {
            return new BeanPostProcessor() {
                {
                    System.out.println("Bean Post Processor init");
                }
                @Override
                public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
                    System.out.println("post process" + beanName);
                    return bean;
                }
            };
        }

        @Bean
        public Bean1 bean1() {
            return new Bean1();
        }

        @Override
        public void afterPropertiesSet() throws Exception {
            //会执行
            System.out.println("config initializing ");
        }
    }

    static class Bean1 implements InitializingBean  {

        public Bean1() {
            System.out.println("Bean1 created");
        }

        @PostConstruct
        public void init() {
            System.out.println("Post Construct init");
        }

        @Override
        public void afterPropertiesSet() throws Exception {
            System.out.println("Initializing init");
        }
    }
}
