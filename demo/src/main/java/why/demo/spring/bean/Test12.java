package why.demo.spring.bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Date;

public class Test12 {

    public static void main(String[] args) {

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(Config.class);

        for (String name : context.getBeanDefinitionNames()) {
            System.out.println(name);
        }
        context.registerBean("date", Date.class);
        for (String name : context.getBeanDefinitionNames()) {
            System.out.println(name);
        }
        //不允许同名，同名会抛 BeanDefinitionStoreException异常
//        context.setAllowBeanDefinitionOverriding(false);

        //不允许循环依赖，否则抛出 BeanCurrentlyInCreationException
//        context.setAllowCircularReferences(false);
        context.refresh();

        //如果能覆盖，新的bean定义会直接替换旧的（无论类型是否一致）
        context.registerBean("date", Object.class);
        System.out.println(context.getBean("date"));

        for (String name : context.getBeanDefinitionNames()) {
            System.out.println(name);
        }
    }

    @Configuration
    static class Config {

        @Bean
        public Date date() {
            return new Date();
        }

        @Bean
        public Bean1 bean1() {
            return new Bean1();
        }

        @Bean
        public Bean2 bean2() {
            return new Bean2();
        }
    }

    static class Bean1 {
        @Autowired
        private Bean2 bean2;
    }

    static class Bean2 {
        @Autowired
        private Bean1 bean1;
    }
}
