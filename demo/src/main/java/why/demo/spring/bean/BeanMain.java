package why.demo.spring.bean;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.BeanFactoryAnnotationUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.*;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.annotation.AnnotationConfigUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ConfigurationClassPostProcessor;
import org.springframework.context.event.EventListener;
import org.springframework.core.Ordered;

import java.util.concurrent.TimeUnit;

public class BeanMain {

    public static void main(String[] args) {
        DefaultListableBeanFactory factory = new DefaultListableBeanFactory();
        newLine("empty bean factory");
        for (String beanDefinitionName : factory.getBeanDefinitionNames()) {
            System.out.println(beanDefinitionName);
        }
        factory.registerBeanDefinition("config", BeanDefinitionBuilder.genericBeanDefinition(Config.class).getBeanDefinition());
        factory.registerBeanDefinition("myBeanFactoryPostProcessor", BeanDefinitionBuilder.genericBeanDefinition(MyBeanFactoryPostProcessor.class).getBeanDefinition());

        AnnotationConfigUtils.registerAnnotationConfigProcessors(factory);

        factory.getBeansOfType(BeanFactoryPostProcessor.class).forEach((k ,v) -> {
//            System.out.println("bean factory后置处理器名称：" + k);
            v.postProcessBeanFactory(factory);
        });

        newLine("parse Configuration");
        for (String beanDefinitionName : factory.getBeanDefinitionNames()) {
            System.out.println(beanDefinitionName);
        }

        System.out.println(factory.getBean(Bean2.class));
    }

    @Configuration
    static class Config {

        @Bean
        public Bean1 bean1() {
            return new Bean1();
        }

        @Bean
        public Bean2 bean2() {
            return bean2();
        }
    }

    static class TestEvent extends ApplicationEvent {

        /** use serialVersionUID from Spring 1.2 for interoperability. */
        private static final long serialVersionUID = 7099057708183571937L;

        public TestEvent(Object source) {
            super(source);
        }
    }
    static class Bean1 {

        @EventListener
        public void listen(TestEvent event) {
            System.out.println(event.getSource());
        }
    }

    static class Bean2 {
        @Autowired
        private Bean1 bean1;
    }

    static class MyBeanFactoryPostProcessor implements BeanDefinitionRegistryPostProcessor, Ordered {

        @Override
        public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
            if (beanFactory instanceof BeanDefinitionRegistry) {
                postProcessBeanDefinitionRegistry((BeanDefinitionRegistry) beanFactory);
            }
        }

        @Override
        public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
            BeanDefinition bd = BeanDefinitionBuilder.genericBeanDefinition(Bean2.class).getBeanDefinition();
            registry.registerBeanDefinition("bean22", bd);
//
//            registry.removeBeanDefinition("org.springframework.context.annotation.internalConfigurationAnnotationProcessor");
//            registry.removeBeanDefinition("org.springframework.context.annotation.internalAutowiredAnnotationProcessor");
//            registry.removeBeanDefinition("org.springframework.context.annotation.internalCommonAnnotationProcessor");
//            registry.removeBeanDefinition("org.springframework.context.event.internalEventListenerProcessor");
//            registry.removeBeanDefinition("org.springframework.context.event.internalEventListenerFactory");
        }

        @Override
        public int getOrder() {
            return Integer.MAX_VALUE;
        }
    }

    private static void newLine(String title) {
        System.out.println(title);
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    }
}
