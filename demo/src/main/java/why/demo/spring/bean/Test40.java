package why.demo.spring.bean;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;

public class Test40 {

    /*
    bean实例化的三种方式
    1.构造方法
    2.静态工厂
    3.实例工厂
     */
    public static void main(String[] args) {
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        BeanDefinition bd0 = BeanDefinitionBuilder.genericBeanDefinition(MyBeanFactory.class).getBeanDefinition();
        beanFactory.registerBeanDefinition("myBeanFactory", bd0);

        //构造方法
        BeanDefinition bd1 = BeanDefinitionBuilder.genericBeanDefinition(Bean1.class).getBeanDefinition();
        beanFactory.registerBeanDefinition("bean1", bd1);

        //静态工厂方法
        BeanDefinition bd2 = BeanDefinitionBuilder.genericBeanDefinition(MyBeanFactory.class)
                .setFactoryMethod("staticFactoryMethod").getBeanDefinition();
        beanFactory.registerBeanDefinition("bean2", bd2);

        //实例工厂方法，实例本身也需要注册到容器中
        BeanDefinition bd3 = BeanDefinitionBuilder.genericBeanDefinition()
                .setFactoryMethodOnBean("instanceFactoryMethod", "myBeanFactory").addConstructorArgValue("bean3").getBeanDefinition();
        beanFactory.registerBeanDefinition("bean3", bd3);

        System.out.println(beanFactory.getBean("myBeanFactory"));
        System.out.println(beanFactory.getBean("bean1"));
        System.out.println(beanFactory.getBean("bean2"));
        System.out.println(beanFactory.getBean("bean3"));
    }

    static class MyBeanFactory {
        //方法必须是静态的，可以指定参数，
        public static Bean1 staticFactoryMethod(){
            return new Bean1();
        }

        public Bean1 instanceFactoryMethod(String name){
            return new Bean1(name);
        }
    }

    static class Bean1 {
        private String name;
        public Bean1() {
            this("bean1");
        }
        public Bean1(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "Bean1{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }
}
