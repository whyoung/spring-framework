package why.demo.spring.bean;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.context.annotation.*;
import org.springframework.context.support.GenericApplicationContext;

/**
 * scope
 *
 * singleton, prototype, request, session, application
 * prototype 域bean的销毁，不由容器控制
 *
 * request、session、application针对web
 * request: 生命周期，每次请求
 * session: 一次会话（浏览器窗口），时间大概一分钟, server.servlet.session.timeout 配置控制
 * application: 应用程序
 */
public class Test6 {

    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean(ConfigurationClassPostProcessor.class);  //Configuration & Bean
        context.registerBean(AutowiredAnnotationBeanPostProcessor.class); //Autowired
        context.registerBean(CommonAnnotationBeanPostProcessor.class); //Autowired
        context.registerBean(Config.class);
        context.registerBean(Bean6_1.class);
        context.refresh();
        Bean6_1 bean1 = context.getBean(Bean6_1.class);
        for (int i = 0; i<3; i++) {
            System.out.println(bean1.getBean2());
            System.out.println(bean1.getBean3());
            System.out.println(bean1.getBean4());

            System.out.println(bean1.bean4);  //不能直接访问成员变量，需要访问lookup注解的方法，否则拿到的还是null
            bean1.test();
        }

        context.close();
    }

    @Configuration
    static class Config {

        @Bean
        @Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
        public Bean6_2 bean2() {
            return new Bean6_2();
        }

        @Bean
        @Scope(value = "prototype")
        public Bean6_3 bean3() {
            return new Bean6_3();
        }

        @Bean
        @Scope(value = "prototype")
        public Bean6_4 bean4() {
            return new Bean6_4();
        }
    }

    abstract static class Bean6_1 {
        @Lazy
        @Autowired
        private Bean6_2 bean2;

        @Autowired
        private ObjectFactory<Bean6_3> bean3;

        private Bean6_4 bean4;

        public Bean6_2 getBean2() {
            return bean2;
        }

        public Bean6_3 getBean3() {
            return bean3.getObject();
        }

        @Lookup("bean4")
        abstract Bean6_4 getBean4();

        public void test() {
            System.out.println(getBean4());
        }
    }
    static class Bean6_2 {
    }

    static class Bean6_3 {
    }

    static class Bean6_4 {
    }
}
