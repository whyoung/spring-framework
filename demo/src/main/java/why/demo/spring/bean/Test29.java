package why.demo.spring.bean;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.CommonAnnotationBeanPostProcessor;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ConfigurationClassPostProcessor;
import org.springframework.context.annotation.ContextAnnotationAutowireCandidateResolver;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.env.StandardEnvironment;

import javax.annotation.PostConstruct;
import java.lang.reflect.Method;


public class Test29 {

    /*
    bean的创建过程：
    正常的流程：
    1.实例化前
        InstantiationAwareBeanPostProcessor#postProcessBeforeInstantiation 实例化前调用
        如果postProcessBeforeInstantiation 方法返回了一个非null对象，后续的InstantiationAwareBeanPostProcessor#postProcessBeforeInstantiation将不会再执行
        直接跳到第7步，执行BeanPostFactory#postProcessAfterInitialization方法
    2.实例化

    3.实例化后
        InstantiationAwareBeanPostProcessor#postProcessAfterInstantiation 实例化后调用

    4.依赖注入

    5.初始化前
        BeanPostProcessor#postProcessBeforeInitialization 初始化前调用

    6.初始化
        Aware回调，init方法调用

    7.初始化后
        BeanPostFactory#postProcessAfterInitialization 初始化后调用，正常情况下，代理对象的创建是在这一步生成
        如果是FactoryBean方式创建的bean，不会执行前面的初始化流程，从第二步直接跳到这一步
     */
    public static void main(String[] args) {

        GenericApplicationContext context = new GenericApplicationContext();
        context.setEnvironment(new StandardEnvironment());
        context.registerBean(ConfigurationClassPostProcessor.class);
        context.registerBean(AutowiredAnnotationBeanPostProcessor.class);
        context.registerBean(CommonAnnotationBeanPostProcessor.class);
        context.registerBean(MyBeanPostProcessor.class);
        context.registerBean(Config.class);
        context.getDefaultListableBeanFactory().setAutowireCandidateResolver(new ContextAnnotationAutowireCandidateResolver());

        context.refresh();

        for (String name : context.getBeanDefinitionNames()) {
            System.out.println(name + "=" + context.getBean(name));
        }
        Bean1 bean1 = context.getBean(Bean1.class);
        bean1.test();
        context.close();
    }

    @Configuration
    static class Config {

        @Bean("bean1")
        public Bean1 bea1() {
            return new Bean1();
        }

        @Bean("bean2")
        public Bean2 bean2() {
            return new Bean2();
        }

        @Bean("bean3")
        public Bean3 bean3() {
            return new Bean3();
        }


    }

    static class Bean1 implements InitializingBean {

        @Autowired
        private void setBean2(Bean2 bean2) {
            System.out.println("bean 属性注入...");
        }

        public Bean1() {
            System.out.println("bean1 实例化...");
        }

        public void test() {
            System.out.println("test");
        }

        @PostConstruct
        public void init1() {
            System.out.println("bean1 PostConstruct 初始化...");
        }

        public void init2() {
            System.out.println("bean1 @Bean inti-method 初始化...");
        }

        @Override
        public void afterPropertiesSet() throws Exception {

        }
    }

    static class Bean2 {
        @Autowired
        public void setBean3(@Value("${JAVA_HOME}") String home) {
            System.out.println("bean2 依赖注入");
        }
    }

    static class Bean3 {
    }

    static class MyBeanPostProcessor implements InstantiationAwareBeanPostProcessor {

        @Override
        public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
            if (beanName.startsWith("bean"))
                System.out.println(beanName + " 初始化前...");
            return bean;
        }

        @Override
        public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
            if (beanName.startsWith("bean"))
                System.out.println(beanName + " 初始化后...");
            return InstantiationAwareBeanPostProcessor.super.postProcessAfterInitialization(bean, beanName);
        }

        @Override
        public Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException {
            if (beanName.equals("bean1")) {
                return Enhancer.create(Bean1.class, new MethodInterceptor() {
                    private final Bean1 target = new Bean1();

                    @Override
                    public Object intercept(Object proxy, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
                        if (!"test".equals(method.getName())) {
                            return methodProxy.invoke(target, args);
                        }
                        System.out.println("before...");
                        Object result = methodProxy.invoke(target, args);
                        System.out.println("after...");
                        return result;
                    }
                });
            } else if (beanName.startsWith("bean")) {
                System.out.println(beanName);
            }
            return InstantiationAwareBeanPostProcessor.super.postProcessBeforeInstantiation(beanClass, beanName);
        }

        @Override
        public boolean postProcessAfterInstantiation(Object bean, String beanName) throws BeansException {
            if (beanName.startsWith("bean"))
                System.out.println(beanName + " 实例化后...");
            return true;
        }
    }
}
