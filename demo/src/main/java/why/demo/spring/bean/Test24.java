package why.demo.spring.bean;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.core.env.StandardEnvironment;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

public class Test24 {

    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean(MyAnnotationBeanPostProcessor.class);
        context.registerBeanDefinition("bean2", BeanDefinitionBuilder.genericBeanDefinition(Bean2.class).getBeanDefinition());
        context.registerBean(Bean1.class);
        context.refresh();
        System.out.println(context.getBean(Bean1.class));
    }

    static class Bean1 {
        @Autowired
        private Bean2 bean2;

        private Bean2 bean22;

        @Autowired
        public void xxx(Bean2 bean2, @Value("${JAVA_HOME}") String javaHome) {
            this.bean22 = bean22;
            System.out.println(javaHome);
        }

        @Override
        public String toString() {
            return "Bean1{" +
                    "bean2=" + bean2 +
                    ", bean22=" + bean22 +
                    '}';
        }
    }

    static class Bean2 {

    }

    static class MyAnnotationBeanPostProcessor implements BeanPostProcessor {

        private final Environment environment = new StandardEnvironment();

        @Override
        public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
            return BeanPostProcessor.super.postProcessBeforeInitialization(bean, beanName);
        }

        @Override
        public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
            System.out.println("after init " + beanName);
            try {
                Class<?> clazz = bean.getClass();
                Field[] fields = clazz.getDeclaredFields();
                for (Field field : fields) {
                    if (field.isAnnotationPresent(Autowired.class)) {
                        System.out.println(field.getName());
                    }
                }

                Method[] methods = clazz.getDeclaredMethods();
                for (Method method : methods) {
                    if (method.isAnnotationPresent(Autowired.class)) {
                        Parameter[] parameters = method.getParameters();
                        for (Parameter param : parameters) {
                            Value value = param.getAnnotation(Value.class);
                            if (value != null) {
                                System.out.println(value.value() + "#" + environment.resolvePlaceholders(value.value()));
                            }

                            System.out.println(param.getClass().getSimpleName() + "#" + param.getName());
                        }
                    }
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            return BeanPostProcessor.super.postProcessAfterInitialization(bean, beanName);
        }
    }
}
