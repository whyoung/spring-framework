package why.demo.spring.deferred.config;

import org.springframework.context.annotation.ComponentScan;

/**
 * @author why
 * @date 2018/11/14 4:29 PM
 * @Description:
 */
@ComponentScan("why.spring.deferred")
public class DeferredAppConfig {
}
