package why.demo.spring.web;

import com.google.common.collect.Lists;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.method.support.HandlerMethodReturnValueHandlerComposite;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.servlet.mvc.method.annotation.*;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static why.demo.spring.utils.Utils.print;

public class MvcTest12 {

    /*
    ControllerAdvice 实现拦截 ResponseBodyAdvice，对返回值重写

     */
    public static void main(String[] args) throws Exception {

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);

        System.out.println(context.getBean(Config.MyResponseBodyAdvice.class));

        Config.MyController myController = context.getBean(Config.MyController.class);
        ServletInvocableHandlerMethod handlerMethod = new ServletInvocableHandlerMethod(myController,
                Config.MyController.class.getDeclaredMethod("testResponseBody"));

        handlerMethod.setHandlerMethodReturnValueHandlers(returnValueHandlerComposite(context));
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();

        ModelAndViewContainer modelAndViewContainer = new ModelAndViewContainer();
        handlerMethod.invokeAndHandle(new ServletWebRequest(request, response), modelAndViewContainer);

        print(response);


    }

    static HandlerMethodReturnValueHandlerComposite returnValueHandlerComposite(AnnotationConfigApplicationContext applicationContext) {
        HandlerMethodReturnValueHandlerComposite composite = new HandlerMethodReturnValueHandlerComposite();
        //ResponseBody 注解处理

        MappingJackson2HttpMessageConverter jsonMessageConverter = new MappingJackson2HttpMessageConverter();

        Map<String, ResponseBodyAdvice> adviceMap = applicationContext.getBeansOfType(ResponseBodyAdvice.class);
        RequestResponseBodyMethodProcessor methodProcessor =
                new RequestResponseBodyMethodProcessor(Lists.newArrayList(jsonMessageConverter, new MappingJackson2XmlHttpMessageConverter()),
                       new ArrayList<>(adviceMap.values()));

        composite.addHandler(methodProcessor);
        return composite;
    }

    @Configuration
    static class Config {

        @ControllerAdvice
        static class MyResponseBodyAdvice implements ResponseBodyAdvice<Object> {

            @Override
            public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
//                AnnotationUtils.f(ResponseBody.class, returnType.getContainingClass());
                return true;
            }

            @Override
            public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
                Map<String, Object> wrap = new HashMap<>();
                wrap.put("code", 200);
                wrap.put("data", body);
                return wrap;
            }
        }

        @ControllerAdvice
        static class MyRequestBodyAdvice implements RequestBodyAdvice {

            @Override
            public boolean supports(MethodParameter methodParameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
                return methodParameter.hasParameterAnnotation(ResponseBody.class);
            }

            @Override
            public HttpInputMessage beforeBodyRead(HttpInputMessage inputMessage, MethodParameter parameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) throws IOException {
                return null;
            }

            @Override
            public Object afterBodyRead(Object body, HttpInputMessage inputMessage, MethodParameter parameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
                return null;
            }

            @Override
            public Object handleEmptyBody(Object body, HttpInputMessage inputMessage, MethodParameter parameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
                return null;
            }
        }


        @Controller
        static class MyController {

            @ResponseBody
            public Model testResponseBody() {
                Model model = new Model();
                model.id = 872;
                model.name = "model-" + model.id;
                return model;
            }
        }

    }


    public static class Model {
        private int id;
        private String name;

        public void setId(int id) {
            this.id = id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return "Model{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    '}';
        }
    }
}
