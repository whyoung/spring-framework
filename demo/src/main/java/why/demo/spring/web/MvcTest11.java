package why.demo.spring.web;

import com.google.common.collect.Lists;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.mock.http.MockHttpInputMessage;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.servlet.mvc.method.annotation.RequestResponseBodyMethodProcessor;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class MvcTest11 {

    /*
    HttpMessageConverter
    java 对象和消息转换
     */
    @SuppressWarnings("all")
    public static void main(String[] args) throws IOException, NoSuchMethodException, HttpMediaTypeNotAcceptableException {

        MockHttpInputMessage inputMessage = new MockHttpInputMessage("{\"id\":1, \"name\":\"model\"}".getBytes(StandardCharsets.UTF_8));

        MappingJackson2XmlHttpMessageConverter xmlHttpMessageConverter =
                new MappingJackson2XmlHttpMessageConverter();

        MappingJackson2HttpMessageConverter jsonMessageConverter =
                new MappingJackson2HttpMessageConverter();

        if (xmlHttpMessageConverter.canRead(Model.class, MediaType.APPLICATION_JSON)) {
            Model model = (Model) xmlHttpMessageConverter.read(Model.class, inputMessage);
            System.out.println(model);
        }

        Model model = null;
        if (jsonMessageConverter.canRead(Model.class, MediaType.APPLICATION_JSON)) {
            model = (Model) jsonMessageConverter.read(Model.class, inputMessage);
            System.out.println(model);
        }

        MockHttpOutputMessage outputMessage = new MockHttpOutputMessage();
        if (xmlHttpMessageConverter.canWrite(Model.class, MediaType.APPLICATION_ATOM_XML)) {

            xmlHttpMessageConverter.write(model, MediaType.APPLICATION_ATOM_XML, outputMessage);
            System.out.println(outputMessage.getBodyAsString());
        }

        outputMessage = new MockHttpOutputMessage();
        if (jsonMessageConverter.canWrite(Model.class, MediaType.APPLICATION_JSON)) {
            jsonMessageConverter.write(model, MediaType.APPLICATION_JSON, outputMessage);
            System.out.println(outputMessage.getBodyAsString());
        }

        RequestResponseBodyMethodProcessor processor =
                new RequestResponseBodyMethodProcessor(Lists.newArrayList(xmlHttpMessageConverter,
                        jsonMessageConverter));

        ModelAndViewContainer modelAndViewContainer = new ModelAndViewContainer();

        MockHttpServletRequest request = new MockHttpServletRequest();

        /*
        优先级：
        1.方法上RequestMapping 指定了produces
        2.response设置了content-type
        3.request设置了accept
        4.message converter的顺序
         */
        request.addHeader("accept", MediaType.APPLICATION_JSON_VALUE);
        MockHttpServletResponse response = new MockHttpServletResponse();
        response.addHeader("content-type", MediaType.APPLICATION_XML_VALUE);


        processor.handleReturnValue(model,
                new HandlerMethod(new MyController(), MyController.class.getMethod("model")).getReturnType(),
                modelAndViewContainer, new ServletWebRequest(request, response));
        System.out.println(new String(response.getContentAsByteArray(), StandardCharsets.UTF_8));
    }

    static class MyController {

        public Model model() {
            Model model = new Model();
            model.id = 10;
            model.name = "model" + model.id;
            return model;
        }
    }
    static class Model {
        private int id;
        private String name;

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return "Model{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    '}';
        }
    }
}
