package why.demo.spring.web;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static why.demo.spring.utils.Utils.printStackTrace;

public class MvcTest8 {

    /*
        @ControllerAdvice 和代理无关
        增强控制器
        1.InitBinder 补充自定义类型转换器
        2.ExceptionHandler 异常处理
        3.ModelAttribute 返回值处理
     */
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);

        RequestMappingHandlerAdapter handlerAdapter = new RequestMappingHandlerAdapter();
        handlerAdapter.setApplicationContext(context);
        handlerAdapter.afterPropertiesSet();

        Field field = RequestMappingHandlerAdapter.class.getDeclaredField("initBinderAdviceCache");
        field.setAccessible(true);
        System.out.println(field.get(handlerAdapter));

        MyController controller = new MyController();
        Method method = RequestMappingHandlerAdapter.class.getDeclaredMethod("getDataBinderFactory", HandlerMethod.class);
        method.setAccessible(true);
        method.invoke(handlerAdapter, new HandlerMethod(controller, MyController.class.getMethod("test")));

        Field field2 = RequestMappingHandlerAdapter.class.getDeclaredField("initBinderCache");
        field2.setAccessible(true);
        System.out.println(field2.get(handlerAdapter));

    }

    @Configuration
    static class Config {

        @Bean
        public MyControllerAdvice myControllerAdvice() {
            return new MyControllerAdvice();
        }

        @Bean
        public MyController myController() {
            return new MyController();
        }
    }

    @ControllerAdvice
    static class MyControllerAdvice {
        @InitBinder
        public void add(WebDataBinder dataBinder) {
            printStackTrace();
            System.out.println("ControllerAdvice init");
        }
    }

    @Controller
    static class MyController {
        @InitBinder
        public void add(WebDataBinder dataBinder) {
            printStackTrace();
            System.out.println("HandlerMethod init");
        }

        @RequestMapping("/test")
        public void test() {

        }
    }
}
