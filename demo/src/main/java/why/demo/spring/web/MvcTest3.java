package why.demo.spring.web;

import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestParameterPropertyValues;

import javax.servlet.ServletRequest;
import java.util.Date;

public class MvcTest3 {

    /*
    web环境下的参数绑定
    ServletModelAttributeMethodProcessor 类中的参数绑定就是由 ServletRequestDataBinder 提供
     */
    public static void main(String[] args) {
        Target target = new Target();
        ServletRequestDataBinder dataBinder = new ServletRequestDataBinder(target);
        dataBinder.initDirectFieldAccess(); //设置只会，会走反射绑定，否则必须提供set方法绑定，没有set方法不会抛异常

        MockHttpServletRequest request = new MockHttpServletRequest("GET", "/test");
        request.setParameter("id", "123");
        request.setParameter("name", "1");
        request.setParameter("version", "123");
        request.setParameter("create", "1991/01/01");
        ServletRequestParameterPropertyValues propertyValues = new ServletRequestParameterPropertyValues(request);
        dataBinder.bind(propertyValues);
        System.out.println(target);
    }

    static class Target {
        private Integer id;
        private String name;
        private Long version;
        private Date create;
        @Override
        public String toString() {
            return "Target{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", version=" + version +
                    ", create=" + create +
                    '}';
        }
    }
}
