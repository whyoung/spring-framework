package why.demo.spring.web;

import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.DirectFieldAccessor;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.SimpleTypeConverter;
import org.springframework.validation.DataBinder;

import java.util.Date;

public class MvcTest2 {

    /*
    数据绑定和类型转换
     */
    public static void main(String[] args) {

        //仅具备类型转换功能
        SimpleTypeConverter converter = new SimpleTypeConverter();
        System.out.println(converter.convertIfNecessary("123", Integer.class));
        System.out.println(converter.convertIfNecessary("2022/05/15", Date.class));

        //通过property赋值，需要提供set方法，无set方法会抛出异常
        Target target = new Target();
        BeanWrapperImpl wrapper = new BeanWrapperImpl(target);
//        wrapper.setPropertyValue("id", "2");
//        wrapper.setPropertyValue("name", 2);
//        wrapper.setPropertyValue("version", "200");
//        wrapper.setPropertyValue("create", "1991/01/01");
//        System.out.println(target);

        //通过反射的方式给属性赋值，不需要set方法
        DirectFieldAccessor accessor = new DirectFieldAccessor(target);
//        accessor.setPropertyValue("id", "2");
//        accessor.setPropertyValue("name", 2);
//        accessor.setPropertyValue("version", "200");
//        accessor.setPropertyValue("create", "1991/01/01");
//        System.out.println(target);

        //如果是web环境，可以使用 ServletRequestDataBinder 子类型
        DataBinder dataBinder = new DataBinder(target);
        dataBinder.initDirectFieldAccess(); //设置只会，会走反射绑定，否则必须提供set方法绑定，没有set方法不会抛异常

        //web环境替换成ServletRequestParameterPropertyValues类型，这个类型能够从ServletRequest对象中提取参数
        MutablePropertyValues propertyValues = new MutablePropertyValues();
        propertyValues.add("id", "2");
        propertyValues.addPropertyValue("id", "2");
        propertyValues.add("name", 2);
        propertyValues.add("version", "2");
        propertyValues.add("create", "1991/01/00");
        dataBinder.bind(propertyValues);
        System.out.println(target);

    }

    static class Target {
        private Integer id;
        private String name;
        private Long version;
        private Date create;

//        public void setId(Integer id) {
//            this.id = id;
//        }
//
//        public void setName(String name) {
//            this.name = name;
//        }
//
//        public void setVersion(Long version) {
//            this.version = version;
//        }
//
//        public void setCreate(Date create) {
//            this.create = create;
//        }

        @Override
        public String toString() {
            return "Target{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", version=" + version +
                    ", create=" + create +
                    '}';
        }
    }
}
