package why.demo.spring.web;

import com.google.common.collect.Lists;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;
import org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import static why.demo.spring.utils.Utils.print;

public class MvcTest14 {

    /*
    ControllerAdvice 异常处理，全局的异常处理Handler
     */
    public static void main(String[] args) throws Exception {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
        Config.MyController myController = context.getBean(Config.MyController.class);
        Method method = Config.MyController.class.getDeclaredMethod("test");

        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        ServletInvocableHandlerMethod handlerMethod =
                new ServletInvocableHandlerMethod(myController, method);

        try {
            handlerMethod.invokeAndHandle(new ServletWebRequest(request, response),
                    new ModelAndViewContainer());
        } catch (Exception e) {
            context.getBean(ExceptionHandlerExceptionResolver.class).resolveException(request, response, handlerMethod.getResolvedFromHandlerMethod() ,e);
        }
        print(response);
    }

    @Configuration
    static class Config {
        @Bean
        public ExceptionHandlerExceptionResolver exceptionHandlerExceptionResolver() {
            ExceptionHandlerExceptionResolver resolver = new ExceptionHandlerExceptionResolver();
            resolver.setMessageConverters(Lists.newArrayList(new MappingJackson2HttpMessageConverter()));
            return resolver;
        }

        /*
        定义全局的异常处理器
         */
        @ControllerAdvice
        public static class MyControllerAdvice {

            @ExceptionHandler
            @ResponseBody
            public Map<String, Object> exception(Exception exception) {
                Map<String, Object> map = new HashMap<>();
                map.put("code", 500);
                map.put("msg", exception.getMessage());
                return map;
            }
        }

        @Controller
        public static class MyController {
            public Object test() {
                throw new NullPointerException("null pointer");
            }
        }
    }
}
