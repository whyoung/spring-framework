package why.demo.spring.web;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.support.DefaultFormattingConversionService;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.bind.ServletRequestParameterPropertyValues;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.support.ConfigurableWebBindingInitializer;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ServletRequestDataBinderFactory;

import java.util.Date;

public class MvcTest6 {

    public static void main(String[] args) throws Exception {
        Target target = new Target();
        MockHttpServletRequest request = new MockHttpServletRequest("GET", "/test");
        request.setParameter("id", "123");
        request.setParameter("name", "1");
        request.setParameter("address.location", "sc");
        request.setParameter("create", "1991|01|01");

        //使用自定义的FormattingConversionService 配合 DateTimeFormat 注解使用
        //springboot 中可以使用 ApplicationConversionService
        DefaultFormattingConversionService conversionService =
                new DefaultFormattingConversionService();

        ConfigurableWebBindingInitializer webBindingInitializer = new ConfigurableWebBindingInitializer();
        webBindingInitializer.setConversionService(conversionService);

        ServletRequestDataBinderFactory binderFactory = new ServletRequestDataBinderFactory(null, webBindingInitializer);
        WebDataBinder dataBinder = binderFactory.createBinder(new ServletWebRequest(request),target, "target");
        dataBinder.initDirectFieldAccess();
        dataBinder.bind(new ServletRequestParameterPropertyValues(request));
        System.out.println(target);
    }

    static class Target {
        private Integer id;
        private String name;
        @DateTimeFormat(pattern = "yyyy|MM|dd")
        private Date create;

        private MvcTest5.Address address;

        @Override
        public String toString() {
            return "Target{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", create=" + create +
                    ", address=" + address +
                    '}';
        }
    }

    static class Address {
        private String location;

        @Override
        public String toString() {
            return "Address{" +
                    "location='" + location + '\'' +
                    '}';
        }
    }
}
