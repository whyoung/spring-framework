package why.demo.spring.web;

import org.springframework.format.Formatter;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestParameterPropertyValues;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.support.ConfigurableWebBindingInitializer;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ServletRequestDataBinderFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MvcTest5 {

    /*
    复杂参数绑定， FormattingConversionService方式
     */
    public static void main(String[] args) throws Exception {
        Target target = new Target();
        WebDataBinder dataBinder = new ServletRequestDataBinder(target);
        dataBinder.initDirectFieldAccess();
        MockHttpServletRequest request = new MockHttpServletRequest("GET", "/test");
        //自定义日期格式默认情况下无法转换，需要定制转换器
        request.setParameter("create", "1991|01|01");
        request.setParameter("id", "123");
        request.setParameter("name", "1");
        request.setParameter("version", "123");
        //嵌套属性可以绑定成功
        request.setParameter("address.location", "sc");

        ServletRequestParameterPropertyValues propertyValues = new ServletRequestParameterPropertyValues(request);
        dataBinder.bind(propertyValues);
        System.out.println(target);

        FormattingConversionService conversionService = new FormattingConversionService();
        //添加自定义formatter
        conversionService.addFormatter(new MyDateFormatter());

        //ConfigurableWebBindingInitializer是WebBindingInitializer类型
        ConfigurableWebBindingInitializer webBindingInitializer = new ConfigurableWebBindingInitializer();
        webBindingInitializer.setConversionService(conversionService);

        ServletRequestDataBinderFactory dataBinderFactory =
                new ServletRequestDataBinderFactory(null, webBindingInitializer);
        dataBinder = dataBinderFactory.createBinder(new ServletWebRequest(request), target, "target");
        dataBinder.initDirectFieldAccess();
        dataBinder.bind(propertyValues);
        System.out.println(target);
    }

    static class Target {
        private Integer id;
        private String name;
        private Date create;

        private Address address;

        @Override
        public String toString() {
            return "Target{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", create=" + create +
                    ", address=" + address +
                    '}';
        }
    }

    static class Address {
        private String location;

        @Override
        public String toString() {
            return "Address{" +
                    "location='" + location + '\'' +
                    '}';
        }
    }

    static class MyDateFormatter implements Formatter<Date> {

        @Override
        public Date parse(String text, Locale locale) throws ParseException {
            return new SimpleDateFormat("yyyy|MM|dd").parse(text);
        }

        @Override
        public String print(Date object, Locale locale) {
            return new SimpleDateFormat("yyyy|MM|dd").format(object);
        }
    }
}
