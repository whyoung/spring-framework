package why.demo.spring.web;

import com.google.common.collect.Lists;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.HandlerExecutionChain;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.AbstractHandlerMapping;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.springframework.web.servlet.mvc.method.annotation.RequestResponseBodyMethodProcessor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import static why.demo.spring.utils.Utils.print;

public class MvcTest15 {

    /*
    spring 拦截器
     */
    public static void main(String[] args) throws Exception {

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);

        RequestMappingHandlerMapping handlerMapping = context.getBean(RequestMappingHandlerMapping.class);
        handlerMapping.setInterceptors(context.getBeansOfType(HandlerInterceptor.class).values().toArray(new Object[0]));

        Method initInterceptors = AbstractHandlerMapping.class.getDeclaredMethod("initInterceptors");
        initInterceptors.setAccessible(true);
        initInterceptors.invoke(handlerMapping);

        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setRequestURI("/test");

        MockHttpServletResponse response = new MockHttpServletResponse();

        HandlerExecutionChain chain = handlerMapping.getHandler(request);
        Method applyPreHandle = HandlerExecutionChain.class.getDeclaredMethod("applyPreHandle", HttpServletRequest.class, HttpServletResponse.class);
        applyPreHandle.setAccessible(true);
        applyPreHandle.invoke(chain, request, response);

        RequestMappingHandlerAdapter handlerAdapter = context.getBean(RequestMappingHandlerAdapter.class);
        handlerAdapter.setReturnValueHandlers(Lists.newArrayList(new RequestResponseBodyMethodProcessor(Lists.newArrayList(new MappingJackson2HttpMessageConverter()))));
        ModelAndView mv = handlerAdapter.handle(request, response, chain.getHandler());
        Method applyPostHandle = HandlerExecutionChain.class.getDeclaredMethod("applyPostHandle", HttpServletRequest.class, HttpServletResponse.class, ModelAndView.class);

        applyPostHandle.setAccessible(true);
        applyPostHandle.invoke(chain,  request, response, mv);
        print(response);
    }

    @Configuration
    static class Config {

        @Bean
        public RequestMappingHandlerMapping requestMappingHandlerMapping() {
            return new RequestMappingHandlerMapping();
        }

        @Bean
        public RequestMappingHandlerAdapter requestMappingHandlerAdapter() {
            return new RequestMappingHandlerAdapter();
        }

        @Controller
        static class MyController {

            @RequestMapping("/test")
            @ResponseBody
            public Object test() {
                Map<String, Object> map = new HashMap<>();
                map.put("foo", "bar");
                return map;
            }
        }

        @Bean
        public HandlerInterceptor handlerInterceptor1() {
            return new HandlerInterceptor() {
                @Override
                public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
                    System.out.println("pre handle1");
                    return true;
                }

                @Override
                public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
                    System.out.println("post handle1");
                }

                @Override
                public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
                    System.out.println("after completion1");
                }
            };
        }

        @Bean
        public HandlerInterceptor handlerInterceptor2() {
            return new HandlerInterceptor() {
                @Override
                public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
                    System.out.println("pre handle2");
                    return true;
                }

                @Override
                public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
                    System.out.println("post handle2");
                }

                @Override
                public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
                    System.out.println("after completion2");
                }
            };
        }

    }
}
