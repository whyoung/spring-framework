package why.demo.spring.web;


import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.DefaultParameterNameDiscoverer;
import org.springframework.core.MethodParameter;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.DefaultDataBinderFactory;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.method.annotation.ExpressionValueMethodArgumentResolver;
import org.springframework.web.method.annotation.RequestHeaderMethodArgumentResolver;
import org.springframework.web.method.annotation.RequestParamMethodArgumentResolver;
import org.springframework.web.method.support.HandlerMethodArgumentResolverComposite;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.mvc.method.annotation.*;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Map;

public class MvcTest1 {

    private static final Object slot = new Object();
    public static void main(String[] args) throws Exception {

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(WebConfig.class);

        MockHttpServletRequest request = new MockHttpServletRequest("GET", "/test/123");
        request.setParameter("s", "hello");
        request.setParameter("x", "5");
        request.setParameter("raw", "java");
        request.setParameter("id", "123");
        request.setParameter("id2", "111");
        request.setParameter("name", "test123");
        request.setParameter("name2", "test111");
        request.setContent("{\"id\":1234, \"name\":\"test1234\"}".getBytes(StandardCharsets.UTF_8));
        Map<String, String> map = new AntPathMatcher().extractUriTemplateVariables("/test/{uid}", "/test/123");
        request.setAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE, map);
        //控制器方法封装成HandlerMethod, SpringMVC 中 RequestMappingHandlerMapping 做转换
        Method method = Arrays.stream(Controller.class.getDeclaredMethods()).filter(m -> "test".equals(m.getName())).findFirst().get();
        HandlerMethod handlerMethod = new HandlerMethod(new Controller(), method);

        //参数类型转换
        DefaultDataBinderFactory dataBinderFactory = new ServletRequestDataBinderFactory(null, null);

        //准备ModelAndViewContainer
        ModelAndViewContainer container =  new ModelAndViewContainer();

        //多个解析器的组合
        HandlerMethodArgumentResolverComposite resolverComposite = new HandlerMethodArgumentResolverComposite();

        /*
        注意解析器的顺序
        如果 RequestParamMethodArgumentResolver 的解析是在PathVariableMethodArgumentResolver 之前
        并且RequestParamMethodArgumentResolver 的 useDefaultResolution 设置成了true，后续的其他的解析器将不执行
         */
        ConfigurableBeanFactory beanFactory = context.getBeanFactory();
        resolverComposite.addResolver(new RequestParamMethodArgumentResolver(beanFactory, false));
        resolverComposite.addResolver(new PathVariableMethodArgumentResolver());
        resolverComposite.addResolver(new RequestHeaderMethodArgumentResolver(beanFactory));  //请求头
        resolverComposite.addResolver(new ServletCookieValueMethodArgumentResolver(beanFactory));  //cookie
        resolverComposite.addResolver(new ExpressionValueMethodArgumentResolver(beanFactory)); //Value 注解解析
        resolverComposite.addResolver(new ServletRequestMethodArgumentResolver()); //解析ServletRequest类型参数
        //ServletModelAttributeMethodProcessor 会将解析的结果加入到 ModelAndViewContainer 里
        resolverComposite.addResolver(new ServletModelAttributeMethodProcessor(false));  //参数和Model映射
        //RequestBody解析器，既是ArgumentResolver，也是ReturnValueHandler，需要设置转换器

        //注意这里的顺序，ServletModelAttributeMethodProcessor 如果 annotationNotRequired 设置成了true，会先于 RequestResponseBodyMethodProcessor 执行
        resolverComposite.addResolver(new RequestResponseBodyMethodProcessor(Lists.newArrayList(new FastJsonHttpMessageConverter())));
        resolverComposite.addResolver(new ServletModelAttributeMethodProcessor(true));


        //解析参数
        for (MethodParameter parameter : handlerMethod.getMethodParameters()) {
            parameter.initParameterNameDiscovery(new DefaultParameterNameDiscoverer());
            boolean required = required(parameter.getParameterAnnotations());
            if (resolverComposite.supportsParameter(parameter)) {
                Object v = resolverComposite.resolveArgument(parameter, container, new ServletWebRequest(request), dataBinderFactory);
                print(parameter, v, required);
                System.out.println("model in container: " + container.getModel());
            } else {
                print(parameter);
            }
        }
    }

    static void print(MethodParameter parameter) {
        print(parameter, slot, false);
    }

    static void print(MethodParameter parameter, Object v, boolean required) {
        StringBuilder sb = new StringBuilder("【").append(parameter.getParameterIndex()).append("】").append(" ");
        Annotation[] annotations = parameter.getParameterAnnotations();
        for (Annotation anno : annotations) {
            sb.append("@").append(anno.annotationType().getSimpleName()).append(" ");
        }
        sb.append(parameter.getParameterType().getSimpleName()).append(" ");
        sb.append(parameter.getParameterName());
        if (v != slot) {
            if (required) {
                sb.append(" (required) ");
            }
            sb.append(" -> ").append(v);
            if (v != null) {
                sb.append("(").append(v.getClass().getSimpleName()).append(")");
            }
        }
        System.out.println(sb);
    }

    static boolean required(Annotation[] annotations) {
        if (annotations.length == 0) {
            return false;
        }
        RequestParam anno = null;
        for (Annotation annotation : annotations) {
            if (annotation.annotationType() == RequestParam.class) {
                anno = (RequestParam) annotation;
                break;
            }
        }
        return anno != null && anno.required();
    }
    @Configuration
    static class WebConfig {

    }

    static class Controller {
        public void test(@RequestParam String s,
                         @RequestParam(value = "x", required = false) int i,
                         @RequestParam(required = false) Long j,
                         @RequestParam(name = "home", defaultValue = "${JAVA_HOME}") String javaHome,
                         String raw,
                         @PathVariable("uid") Integer uid,
                         @ModelAttribute(name = "model1") Model model1,
                         Model2 model2,
                         @RequestBody Model model3) {
        }
    }

    static class Model {
        Integer id;
        String name;
//
        public Model(Integer id, String name) {
            this.id = id;
            this.name = name;
        }

//        public void setName(String name) {
//            this.name = name;
//        }
//
//        public void setId(Integer id) {
//            this.id = id;
//        }

        @Override
        public String toString() {
            return "{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    '}';
        }
    }

    static class Model2 {
        Integer id2;
        String name2;

        //是根据参数名称映射，而不是属性名称
        public Model2(Integer id2, String name2) {
            this.id2 = id2;
            this.name2 = name2;
        }

        @Override
        public String toString() {
            return "{" +
                    "id=" + id2 +
                    ", name='" + name2 + '\'' +
                    '}';
        }
    }
}
