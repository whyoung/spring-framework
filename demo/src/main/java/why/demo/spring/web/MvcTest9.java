package why.demo.spring.web;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.DefaultParameterNameDiscoverer;
import org.springframework.core.MethodParameter;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.support.DefaultDataBinderFactory;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolverComposite;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod;
import org.springframework.web.servlet.mvc.method.annotation.ServletModelAttributeMethodProcessor;
import org.springframework.web.servlet.mvc.method.annotation.ServletRequestDataBinderFactory;

import java.lang.reflect.Method;
import java.util.Date;

public class MvcTest9 {

    public static void main(String[] args) throws Exception {

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
        {
            Config.MyController controller = new Config.MyController();
            Method method = Config.MyController.class.getDeclaredMethod("test", Model.class);
            ServletInvocableHandlerMethod handlerMethod = new ServletInvocableHandlerMethod(controller, method);


            ServletRequestDataBinderFactory dataBinderFactory = new ServletRequestDataBinderFactory(null, null);

            HandlerMethodArgumentResolverComposite composite = customArgumentResolverComposite();
            handlerMethod.setHandlerMethodArgumentResolvers(composite);
            handlerMethod.setParameterNameDiscoverer(new DefaultParameterNameDiscoverer());
            handlerMethod.setDataBinderFactory(dataBinderFactory);

            MockHttpServletRequest request = new MockHttpServletRequest();
            request.setParameter("id", "33");
            request.setParameter("name", "haha");

//            Model model = new Model();
//            WebDataBinder dataBinder = dataBinderFactory.createBinder(new ServletWebRequest(request), model, "model");
//            dataBinder.bind(new ServletRequestParameterPropertyValues(request));
//            System.out.println(model);
            ModelAndViewContainer modelAndViewContainer = new ModelAndViewContainer();
//            Object obj = composite.resolveArgument(new MethodParameter(method, 0), modelAndViewContainer,
//                    new ServletWebRequest(request), dataBinderFactory);
            handlerMethod.invokeForRequest(new ServletWebRequest(request), modelAndViewContainer);
            System.out.println(modelAndViewContainer);
        }
    }

    private static HandlerMethodArgumentResolverComposite customArgumentResolverComposite() {
        HandlerMethodArgumentResolverComposite composite =
                new HandlerMethodArgumentResolverComposite();
        composite.addResolver(new ServletModelAttributeMethodProcessor(false));
        composite.addResolver(new ServletModelAttributeMethodProcessor(true));
        return composite;
    }

    @Configuration
    static class Config {
        @Controller
        static class MyController {

            @ModelAttribute(name = "date")
            public Date newDate() {
                System.out.println("a12");
                return new Date();
            }

            public void test(@ModelAttribute Model model) {
                System.out.println(model);
            }
        }

    }



    static class Model {
        private Integer id;
        private String name;

        public void setId(Integer id) {
            this.id = id;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "Model{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    '}';
        }
    }
}
