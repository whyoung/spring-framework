package why.demo.spring.web;

import com.google.common.collect.Lists;
import org.springframework.format.Formatter;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.validation.DataBinder;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestParameterPropertyValues;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.support.WebBindingInitializer;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.method.support.InvocableHandlerMethod;
import org.springframework.web.servlet.mvc.method.annotation.ServletRequestDataBinderFactory;

import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MvcTest4 {

    /*
    复杂参数绑定,@InitBinder 方式
     */
    public static void main(String[] args) throws Exception {
        Target target = new Target();
        WebDataBinder dataBinder = new ServletRequestDataBinder(target);
        dataBinder.initDirectFieldAccess(); //设置只会，会走反射绑定，否则必须提供set方法绑定，没有set方法不会抛异常

        MockHttpServletRequest request = new MockHttpServletRequest("GET", "/test");
        //自定义日期格式默认情况下无法转换
        request.setParameter("create", "1991|01|01");
        request.setParameter("id", "123");
        request.setParameter("name", "1");
        request.setParameter("version", "123");
        //嵌套属性可以绑定成功
        request.setParameter("address.location", "sc");

        ServletRequestParameterPropertyValues propertyValues = new ServletRequestParameterPropertyValues(request);
        dataBinder.bind(propertyValues);
        System.out.println(target);

        /*
        使用工厂创建 dataBinder
        第一个参数是全部的加了@InitBinder 注解的控制器方法（HandlerMethod），由RequestMappingHandlerMapping 实现
         */
        MyController controller = new MyController();
        Method method = MyController.class.getMethod("test", WebDataBinder.class);

        target = new Target();

        ServletRequestDataBinderFactory dataBinderFactory = new ServletRequestDataBinderFactory(null, null);
        dataBinder = dataBinderFactory.createBinder(new ServletWebRequest(request), target, "target");
        dataBinder.bind(propertyValues);
        System.out.println(target);

        //使用@InitBinder注解，需要配合控制器方法使用 HandlerMethod
        target = new Target();
        HandlerMethod handlerMethod = new HandlerMethod(controller, method);
        InvocableHandlerMethod invocableHandlerMethod = new InvocableHandlerMethod(handlerMethod);
        invocableHandlerMethod =  new InvocableHandlerMethod(controller, method);

        //注意这里，默认是用property方式，必须要提供set方法，或者在创建工厂的时候设置 WebBindingInitializer
        WebBindingInitializer webBindingInitializer = DataBinder::initDirectFieldAccess;
        dataBinderFactory =
                new ServletRequestDataBinderFactory(Lists.newArrayList(invocableHandlerMethod), webBindingInitializer);
        dataBinder = dataBinderFactory.createBinder(new ServletWebRequest(request), target, "target");
        dataBinder.bind(new ServletRequestParameterPropertyValues(request));
        System.out.println(target);
    }

    static class Target {
        private Integer id;
        private String name;
        private Date create;

        private Address address;

        @Override
        public String toString() {
            return "Target{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", create=" + create +
                    ", address=" + address +
                    '}';
        }
    }

    static class Address {
        private String location;

        @Override
        public String toString() {
            return "Address{" +
                    "location='" + location + '\'' +
                    '}';
        }
    }

    static class MyController {
        @InitBinder
        public void test(WebDataBinder dataBinder) {
            //createBinder 会回调这个方法
//            printStackTrace();
            dataBinder.addCustomFormatter(new MyDateFormatter());
        }
    }

    static class MyDateFormatter implements Formatter<Date> {

        @Override
        public Date parse(String text, Locale locale) throws ParseException {
            return new SimpleDateFormat("yyyy|MM|dd").parse(text);
        }

        @Override
        public String print(Date object, Locale locale) {
            return new SimpleDateFormat("yyyy|MM|dd").format(object);
        }
    }
}
