package why.demo.spring.web;

import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.google.common.collect.Lists;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.method.support.HandlerMethodReturnValueHandlerComposite;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.*;
import org.springframework.web.util.UrlPathHelper;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.function.Consumer;

import static why.demo.spring.utils.Utils.print;

public class MvcTest10 {

    /*
    返回值处理
    return value handle
     */
    static AnnotationConfigApplicationContext context;
    static MyController controller = new MyController();

    public static void main(String[] args) throws Exception {
        context = new AnnotationConfigApplicationContext(Config.class);
        testReturnValueHandle(MyController.class.getDeclaredMethod("modelAndView"));
        testReturnValueHandle(MyController.class.getDeclaredMethod("viewName"));
        testReturnValueHandle(MyController.class.getDeclaredMethod("modelAttribute"), Optional.of(modelAndViewContainer -> modelAndViewContainer.setViewName("test3")));
        testReturnValueHandle(MyController.class.getDeclaredMethod("modelAttribute2"), Optional.of(modelAndViewContainer -> modelAndViewContainer.setViewName("test4")));

        testReturnValueHandle(MyController.class.getDeclaredMethod("httpEntity"));
        testReturnValueHandle(MyController.class.getDeclaredMethod("httpHeaders"));
        testReturnValueHandle(MyController.class.getDeclaredMethod("responseBody"));

    }

    static void testReturnValueHandle(Method method) throws Exception {
        testReturnValueHandle(method, Optional.empty());
    }
    static void testReturnValueHandle(Method method, Optional<Consumer<ModelAndViewContainer>> consumer) throws Exception {
        Object returnValue = method.invoke(controller);
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        ServletWebRequest wrapRequest = new ServletWebRequest(request, response);
        HandlerMethod handlerMethod = new HandlerMethod(controller, method);
        HandlerMethodReturnValueHandlerComposite composite = returnValueHandlerComposite();
        ModelAndViewContainer modelAndViewContainer = new ModelAndViewContainer();
        consumer.ifPresent(mockHttpServletRequestConsumer -> mockHttpServletRequestConsumer.accept(modelAndViewContainer));
        if (composite.supportsReturnType(handlerMethod.getReturnType())) {
            composite.handleReturnValue(returnValue, handlerMethod.getReturnType(), modelAndViewContainer, wrapRequest);
            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            System.out.println(modelAndViewContainer.getModel());
            if (!modelAndViewContainer.isRequestHandled()) {
                render(context, modelAndViewContainer, wrapRequest);
            }
            print(response);
        }
    }

    static HandlerMethodReturnValueHandlerComposite returnValueHandlerComposite() {
        HandlerMethodReturnValueHandlerComposite composite = new HandlerMethodReturnValueHandlerComposite();
        composite.addHandler(new ModelAndViewMethodReturnValueHandler()); //处理返回值是ModelAndView类型的
        composite.addHandler(new ViewNameMethodReturnValueHandler());  //处理返回值是string类型的，将返回值作为view name
        composite.addHandler(new ServletModelAttributeMethodProcessor(false));  //处理加了ModelAttribute注解的

        {
            /*
            通过设置 ModelAndViewContainer的 requestHandled标记，使得响应不会继续往下处理（不需要视图解析）
             */
            //处理HttpEntity类型的返回值，必须提供 HttpMessageConverter
            //fastjson 默认 content-type 是 *
            FastJsonHttpMessageConverter messageConverter = new FastJsonHttpMessageConverter();
            messageConverter.setSupportedMediaTypes(Lists.newArrayList(MediaType.APPLICATION_JSON_UTF8));
            composite.addHandler(new HttpEntityMethodProcessor(Lists.newArrayList(messageConverter)));

            //处理HttpHeader类型
            composite.addHandler(new HttpHeadersReturnValueHandler());

            //ResponseBody 注解处理
            composite.addHandler(new RequestResponseBodyMethodProcessor(Lists.newArrayList(messageConverter)));
        }
//        composite.addHandler(new ServletModelAttributeMethodProcessor(true));   //处理未加ModelAttribute注解的，注意要放到后面，否则ResponseBody这些注解不会生效
        return composite;
    }

    @org.springframework.context.annotation.Configuration
    static class Config {

        @Bean
        public Configuration configuration() throws IOException {
            Configuration configuration = new Configuration(Configuration.getVersion());
            configuration.setDefaultEncoding(StandardCharsets.UTF_8.name());
            configuration.setDirectoryForTemplateLoading(new File("/Users/why/Public/code/spring-framework/demo/src/main/resources/template/"));
            return configuration;
        }
    }

    private static void render(AnnotationConfigApplicationContext context, ModelAndViewContainer modelAndViewContainer,
                               ServletWebRequest request) throws IOException, TemplateException {

        Configuration configuration = context.getBean(Configuration.class);
        Template template = configuration.getTemplate(modelAndViewContainer.getViewName() + ".ftl");
        template.process(modelAndViewContainer.getModel(), request.getResponse().getWriter());
    }

    static class MyController {

        //返回ModelAndView
        public ModelAndView modelAndView() {
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("test1");
            modelAndView.addObject("name", "test1");
            return modelAndView;
        }

        //返回字符串，返回值作为视图名字
        public String viewName() {
            return "test2";
        }

        //这种情况下，会将RequestMapping映射的路径名称作为视图名
        @RequestMapping("/test3")
        @ModelAttribute("user")
        public Model modelAttribute() {
            Model model = new Model();
            model.id = 100;
            model.name = "test3";
            return model;
        }

        public Model modelAttribute2() {
            Model model = new Model();
            model.id = 100;
            model.name = "test3";
            return model;
        }

        public HttpEntity<Model> httpEntity() {
            Model model = new Model();
            model.id = 100;
            model.name = "test3";
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
            return new HttpEntity<>(model, headers);
        }

        public HttpHeaders httpHeaders() {
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "application/json");
            return headers;
        }

        @ResponseBody
        public Model responseBody() {
            Model model = new Model();
            model.id = 100;
            model.name = "test3";
            return model;
        }
    }

    //如果使用freemarker，注意这个类必须是public，并且提供get方法，否则ftl中无法引用属性
    public static class Model {
        private int id;
        private String name;

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return "Model{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    '}';
        }
    }
}
