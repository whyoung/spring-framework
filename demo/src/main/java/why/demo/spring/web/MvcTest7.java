package why.demo.spring.web;


import org.springframework.core.GenericTypeResolver;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;

public class MvcTest7 {

    /*
    获取泛型信息
     */
    public static void main(String[] args) {
        //jdk api
        Type type = Sub.class.getGenericSuperclass();
        System.out.println(type);
        if (type instanceof ParameterizedType) {
            ParameterizedType pType = (ParameterizedType) type;
            System.out.println(Arrays.toString(pType.getActualTypeArguments()));
        }

        //spring api
        System.out.println(Arrays.toString(GenericTypeResolver.resolveTypeArguments(Sub.class, Super.class)));

    }

    static class Super<T> {
    }

    static class Sub extends Super<String> {

    }
}
