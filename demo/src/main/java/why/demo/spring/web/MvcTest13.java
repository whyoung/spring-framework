package why.demo.spring.web;

import com.google.common.collect.Lists;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.method.support.HandlerMethodReturnValueHandlerComposite;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;
import org.springframework.web.servlet.mvc.method.annotation.RequestResponseBodyMethodProcessor;
import org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

import static why.demo.spring.utils.Utils.print;

public class MvcTest13 {

    /*
    异常处理
    HandlerExceptionResolver
    注意：HandlerExceptionResolver只能处理控制器中的异常，filter中抛出的异常或者未处理的异常将由servlet容器处理
     */
    public static void main(String[] args) throws Exception {
        ExceptionHandlerExceptionResolver exceptionResolver = new ExceptionHandlerExceptionResolver();
        exceptionResolver.setMessageConverters(Lists.newArrayList(new MappingJackson2HttpMessageConverter()));
        exceptionResolver.afterPropertiesSet();

        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();

        HandlerMethod handlerMethod = new HandlerMethod(new MyController(), MyController.class.getDeclaredMethod("test"));

        ServletInvocableHandlerMethod invocableHandlerMethod = new ServletInvocableHandlerMethod(handlerMethod);
        invocableHandlerMethod.setHandlerMethodReturnValueHandlers(returnValueHandlerComposite());
        try {
            invocableHandlerMethod.invokeAndHandle(new ServletWebRequest(request, response), new ModelAndViewContainer());
        } catch (Exception e) {
            exceptionResolver.resolveException(request, response, handlerMethod, e);
        }
        print(response);
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        handlerMethod = new HandlerMethod(new MyController2(), MyController2.class.getDeclaredMethod("test"));
        invocableHandlerMethod = new ServletInvocableHandlerMethod(handlerMethod);
        invocableHandlerMethod.setHandlerMethodReturnValueHandlers(returnValueHandlerComposite());
        response = new MockHttpServletResponse();
        try {
            invocableHandlerMethod.invokeAndHandle(new ServletWebRequest(request, response), new ModelAndViewContainer());
        } catch (Exception e) {
            exceptionResolver.resolveException(request, response, handlerMethod, e);
        }
        print(response);
    }

    static HandlerMethodReturnValueHandlerComposite returnValueHandlerComposite() {
        HandlerMethodReturnValueHandlerComposite composite = new HandlerMethodReturnValueHandlerComposite();
        //ResponseBody 注解处理
        MappingJackson2HttpMessageConverter jsonMessageConverter = new MappingJackson2HttpMessageConverter();
        RequestResponseBodyMethodProcessor methodProcessor =
                new RequestResponseBodyMethodProcessor(Lists.newArrayList(jsonMessageConverter, new MappingJackson2XmlHttpMessageConverter()));
        composite.addHandler(methodProcessor);
        return composite;
    }

    static class MyController {
        @ResponseBody
        public Object test() {
            throw new NullPointerException("null pointer");
        }

        /*
        会从当前类中查找加了 @ExceptionHandler 注解的方法，再将参数传给这个方法，就相当于调用了处理异常的方法
        这个方法除了Exception类型参数以外，还能传递其他参数，参考 ExceptionHandler类的描述
         */
        @ExceptionHandler
        @ResponseBody
        public Map<String, Object> exception(NullPointerException exception, HttpServletRequest request) {
            System.out.println(request.getClass().getName());
            Map<String, Object> map = new HashMap<>();
            map.put("code", 500);
            map.put("msg", exception.getMessage());
            return map;
        }

    }


    static class MyController2 {
        @ResponseBody
        public Object test() {
            throw new RuntimeException("runtime exception", new NullPointerException("null pointer"));
        }
        /*
        异常嵌套
        内层嵌套的异常类型也能够处理
         */
        @ExceptionHandler
        @ResponseBody
        public Map<String, Object> exception(NullPointerException exception) {
            Map<String, Object> map = new HashMap<>();
            map.put("code", 500);
            map.put("msg", exception.getMessage());
            return map;
        }
    }
}
