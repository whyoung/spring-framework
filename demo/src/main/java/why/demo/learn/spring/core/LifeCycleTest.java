package why.demo.learn.spring.core;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.Lifecycle;
import org.springframework.context.SmartLifecycle;
import org.springframework.context.support.GenericApplicationContext;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;


public class LifeCycleTest {

    public static void main(String[] args) throws InterruptedException {
        GenericApplicationContext context = new GenericApplicationContext();

        context.refresh();
        System.out.println("refresh...");
        TimeUnit.SECONDS.sleep(3);
        context.start();
        TimeUnit.SECONDS.sleep(3);
//        context.stop();
    }

    static class TestLifeCycle implements Lifecycle {

        private volatile boolean running = false;
        @Override
        public void start() {
            System.out.println("start");
            running = true;
        }

        @Override
        public void stop() {
            System.out.println("stop");
            running = false;
        }

        @Override
        public boolean isRunning() {
            System.out.println("running");
            return running;
        }
    }

    static class TestSmartLifeCycle implements SmartLifecycle {

        private volatile boolean running = false;

        @Override
        public void start() {
            System.out.println("smart start");
            running = true;
        }

        @Override
        public void stop() {
            System.out.println("smart stop");
            running = false;
        }

        @Override
        public boolean isRunning() {
            System.out.println("smart isRunning");
            return running;
        }
    }
}
