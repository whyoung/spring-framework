package why.demo.learn.spring.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConstructorArgumentValues;
import org.springframework.beans.factory.config.RuntimeBeanReference;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.context.annotation.CommonAnnotationBeanPostProcessor;
import org.springframework.context.support.GenericApplicationContext;

import javax.annotation.Resource;
import java.io.IOException;

public class LearnApplicationContext2 {

    public static void main(String[] args) throws IOException, InterruptedException {
        GenericApplicationContext ctx = new GenericApplicationContext();
        ctx.registerBean(AutowiredAnnotationBeanPostProcessor.class);
        ctx.registerBean(CommonAnnotationBeanPostProcessor.class);
        ctx.registerBean("bean22", Bean2.class);
        ctx.registerBean("bean21", Bean2.class);
        ctx.registerBean("bean1", Bean1.class);
        BeanDefinition bd = ctx.getBeanFactory().getBeanDefinition("bean1");
        if (bd instanceof AbstractBeanDefinition) {
            AbstractBeanDefinition abd = (AbstractBeanDefinition) bd;
//            {
//                //不自动装配
//                abd.setAutowireMode(AbstractBeanDefinition.AUTOWIRE_NO);
//            }
//            {
//                //构造方法自动装配
//                abd.setAutowireMode(AbstractBeanDefinition.AUTOWIRE_CONSTRUCTOR);
//            }
//            {
//                //set方法手动装配
//                abd.getPropertyValues().addPropertyValue("bean2", new RuntimeBeanReference("bean2"));
//            }
            {
                //构造函数手动装配
//                ConstructorArgumentValues argumentValues = new ConstructorArgumentValues();
//                argumentValues.addGenericArgumentValue(new ConstructorArgumentValues.ValueHolder(new RuntimeBeanReference("bean2")));
//                abd.getConstructorArgumentValues().addArgumentValues(argumentValues);
            }
            {
                //自动装配，根据类型
                abd.setAutowireMode(AbstractBeanDefinition.AUTOWIRE_BY_TYPE);
            }
//            {
//                //自动装配，根据名字
//                abd.setAutowireMode(AbstractBeanDefinition.AUTOWIRE_BY_NAME);
//            }
        }
        ctx.refresh();
        System.out.println(ctx.getBean("bean1"));
    }

    public static class Bean1 {

//        @Resource
        private Bean2 bean2;

        public Bean1() {
            System.out.println("bean1 constructor");
        }
//
//        public Bean1(Bean2 bean2) {
//            System.out.println("bean1 custom constructor");
//            this.bean2 = bean2;
//        }
//
//        public Bean1(Bean2 bean2, Bean3 bean3) {
//            System.out.println("bean1 custom constructor2");
//            System.out.println(bean3);
//            this.bean2 = bean2;
//        }
//
        @Autowired
        public void setBean(Bean2 bean2) {
            System.out.println("bean1 setter");
            this.bean2 = bean2;
        }

        @Autowired
        public void setBean2(Bean2 bean2) {
            System.out.println("bean1 setter2");
            this.bean2 = bean2;
        }
//
//        public void setBean3(Bean2 bean2) {
//            System.out.println("bean1 setter3");
//        }
//
        @Autowired
        public void setBean2(String text) {
            System.out.println(text);
        }
//
//        public void setBean2(Bean2 bean2, Bean3 bean3) {
//            System.out.println("bean1 setter3");
//            this.bean2 = bean2;
//        }

        @Override
        public String toString() {
            return "Bean1{" +
                    "bean2=" + bean2 +
                    '}';
        }
    }

    public static class Bean2 {
    }
    public static class Bean3 {
    }
}
