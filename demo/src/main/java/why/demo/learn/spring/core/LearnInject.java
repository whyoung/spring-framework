package why.demo.learn.spring.core;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Date;

public class LearnInject {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:config.xml");
        for (String name : context.getBeanDefinitionNames()) {
            BeanDefinition bd = context.getBeanFactory().getBeanDefinition(name);
            System.out.println(bd);
        }
    }

    public static class Bean1 {
        private Bean2 bean2;

        public Bean1() {
            System.out.println("bean1 constructor");
        }
        public Bean1(Bean2 bean2) {
            System.out.println("bean1 custom constructor");
            System.out.println(bean2);
        }
        public void setBean2(Bean2 bean2) {
            this.bean2 = bean2;
            System.out.println("bean1 setter");
        }
    }

    public static class Bean2 {

        public Bean2(Date date) {
            System.out.println(date);
        }
    }
}
