package why.demo.learn.spring.core;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.DefaultEventListenerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.context.event.EventListenerMethodProcessor;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import static why.demo.spring.utils.Utils.printStackTrace;

public class LearnApplicationContext {

    public static void main(String[] args) throws IOException, InterruptedException {
        GenericApplicationContext ctx = new GenericApplicationContext();
        ctx.registerBean(EventListenerMethodProcessor.class);
//        ctx.registerBean(CommonAnnotationBeanPostProcessor.class);
        ctx.registerBean(DefaultEventListenerFactory.class);

        ctx.registerBean(TestEventListener.class);
        ctx.refresh();
//        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
//        ctx.registerBean(TestEventListener.class);
//        ctx.refresh();
//        ctx.addApplicationListener((ApplicationListener<TestEvent>) event -> ctx.getBean(TestEventListener.class).listen(event));
//        System.out.println(ctx.getBean(TestEventListener.class));
//        System.out.println(ctx.getEnvironment().getProperty("java_home"));
//        System.out.println(Arrays.toString(ctx.getEnvironment().getDefaultProfiles()));
        ctx.publishEvent(new TestEvent("hello"));
//        Resource resource = ctx.getResource("classpath:META-INF/spring.factories");
//        System.out.println(resource);

//        Resource[] resources = ctx.getResources("classpath*:/META-INF/spring.factories");
//        for (Resource r : resources) {
//            System.out.println(r);
//        }
//        Iterator<String> iterator = ctx.getBeanFactory().getBeanNamesIterator();
//        while (iterator.hasNext()) {
//            String beanName = iterator.next();
//            Object bean = ctx.getBean(beanName);
//            System.out.println(beanName);
//            System.out.println(bean.getClass());
//        }

        System.out.println(ctx.getApplicationListeners());
        TimeUnit.SECONDS.sleep(10);
    }

    static class TestConfig {

    }

    static class TestEventListener {

        @EventListener(classes = TestEvent.class)
        public void listen(TestEvent event) {
            printStackTrace();
            System.out.println("@EventListener " + event.getSource());
        }
    }

    static class TestEvent extends ApplicationEvent {
        private static final long serialVersionUID = 7099057708183571937L;

        /**
         * Create a new ApplicationEvent.
         *
         * @param source the object on which the event initially occurred (never {@code null})
         */
        public TestEvent(Object source) {
            super(source);
        }
    }
}
