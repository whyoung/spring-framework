package why.demo.learn.spring.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.context.annotation.CommonAnnotationBeanPostProcessor;
import org.springframework.context.support.GenericApplicationContext;

import javax.annotation.Resource;
import java.io.IOException;

public class LearnApplicationContext3 {

    public static void main(String[] args) throws IOException, InterruptedException {
        GenericApplicationContext ctx = new GenericApplicationContext();
        ctx.registerBean(AutowiredAnnotationBeanPostProcessor.class);
//        ctx.registerBean(CommonAnnotationBeanPostProcessor.class);
        ctx.registerBean("bean2", Bean2.class);
        ctx.registerBean("bean22", Bean2.class);
        ctx.registerBean("bean1", Bean1.class);
        ctx.refresh();
        System.out.println(ctx.getBean("bean1"));
    }

    public static class Bean1 {

        @Resource

//        @Autowired
        private Bean2 bean2;

        public Bean1() {
            System.out.println("bean1 constructor");
        }

//        public Bean1(Bean2 bean2) {
//            System.out.println("bean1 custom constructor");
//            this.bean2 = bean2;
//        }
//@Autowired
        public void setBean(Bean2 bean2) {
            System.out.println("bean1 setter");
            this.bean2 = bean2;
        }
//        @Autowired
        public void setBean2(Bean2 bean22) {
            System.out.println("bean1 setter2");
            this.bean2 = bean2;
        }

        @Override
        public String toString() {
            return "Bean1{" +
                    "bean2=" + bean2 +
                    '}';
        }
    }

    public static class Bean2 {
    }
}
