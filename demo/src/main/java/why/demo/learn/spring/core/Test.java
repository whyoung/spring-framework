package why.demo.learn.spring.core;

import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;

import java.util.Arrays;
import java.util.Date;

public class Test {

    public static void main(String[] args) {
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();

        BeanDefinitionRegistry registry = beanFactory;
        registry.registerBeanDefinition("date", BeanDefinitionBuilder.genericBeanDefinition(Date.class).getBeanDefinition());
        registry.registerAlias("date", "current");

        System.out.println(registry.getBeanDefinitionCount());
        System.out.println(Arrays.toString(registry.getBeanDefinitionNames()));
        System.out.println(registry.isBeanNameInUse("date"));
        System.out.println(registry.isBeanNameInUse("current"));
    }
}
