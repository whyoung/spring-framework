package why.demo.learn.spring.core;

import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;

import java.util.Date;

public class LearnBeanFactory {

    public static void main(String[] args) {
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        AbstractBeanDefinition bd1 = BeanDefinitionBuilder.genericBeanDefinition(Date.class)
                .getBeanDefinition();
        beanFactory.registerBeanDefinition("date", bd1);
        if (beanFactory.containsBean("date")) {
            System.out.println(beanFactory.getBean("date"));
        }

        AbstractBeanDefinition bd2 = BeanDefinitionBuilder.genericBeanDefinition(MyBean.class)
                //默认是不自动注入，这种模式下需要提供无参的构造方法，如果是AUTOWIRE_CONSTRUCTOR，可以通过构造方法实现属性的赋值
//                .setAutowireMode(AbstractBeanDefinition.AUTOWIRE_CONSTRUCTOR)
                .setAutowireMode(AbstractBeanDefinition.AUTOWIRE_BY_NAME)
                //如果需要处理依赖注入，需要自己维护bean之间的依赖关系
                .addPropertyReference("date", "date")
                .getBeanDefinition();

        beanFactory.registerBeanDefinition("myBean", bd2);
        MyBean myBean = (MyBean) beanFactory.getBean("myBean");
        System.out.println(myBean.date);
    }

    static class MyBean {
        public MyBean() {
        }

//        public MyBean(Date date) {
//            System.out.println("constructor");
//            this.date = date;
//        }

        private Date date;
        public void setDate2(Date date) {
            System.out.println("setter");
            this.date = date;
        }

        @Override
        public String toString() {
            return "MyBean{" + "date=" + date + '}';
        }
    }
}
