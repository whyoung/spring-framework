项目是从spring [官方源码](https://github.com/spring-projects/spring-framework.git) fork的，加了点个人的注释

## spring 源码编译



1. 下载spring源码，`https://github.com/spring-projects/spring-framework.git`
2. 打开工程目录，执行 `./gradlew :spring-oxm:compileTestJava`
3. 导入工程到idea
4. 执行gradle的`build`，导入之后会自动执行
5. 去掉`spring-aspects`模块
6. 运行各个模块的test，编译代码，例如要使用`spring-context`模块，需要先执行单元测试代码，即使没有测试用例（例如`spring-instrument`）也需要执行，否则会报找不到class
7. 创建自己的模块，依赖spring的模块



例子：创建`demo`模块，依赖`spring-context`

```groovy
dependencies {
    compile(project(":spring-context"))
    testCompile group: 'junit', name: 'junit', version: '4.12'
}
```

测试代码：

```java
@Configuration
public class AppConfig {
	@Bean
	public Date now() {
		return new Date();
	}
}
```

```java
public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		System.out.println(context.getBean(Date.class));
	}
}
```

可以正常打印日期，则表示构建成功





