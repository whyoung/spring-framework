/*
 * Copyright 2002-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.beans.factory.config;

import org.springframework.lang.Nullable;

/**
 * 定义注册共享bean实例的注册表接口，可以由{@link org.springframework.beans.factory.BeanFactory}实现
 * 实现类可以通过这个接口实现统一的单例bean管理。
 *
 * Interface that defines a registry for shared bean instances.
 * Can be implemented by {@link org.springframework.beans.factory.BeanFactory}
 * implementations in order to expose their singleton management facility
 * in a uniform manner.
 *
 * <p>The {@link ConfigurableBeanFactory} interface extends this interface.
 *
 * @author Juergen Hoeller
 * @since 2.0
 * @see ConfigurableBeanFactory
 * @see org.springframework.beans.factory.support.DefaultSingletonBeanRegistry
 * @see org.springframework.beans.factory.support.AbstractBeanFactory
 */
public interface SingletonBeanRegistry {

	/**
	 * 将给定名字的bean作为一个单独对象注册到注册表中，
	 * <p>给定的实例必须是完全初始化的，注册表不会执行任何的初始化回调（特别的，它不会调InitializingBean的{@code afterPropertiesSet}方法）
	 * 给定的实现也不会接收任何销毁回调（例如DisposableBean 的{@code destroy}方法）
	 * <p>当在一个完整的BeanFactory运行时，<b>如果bean需要接收初始化/销毁回调，应当注册一个bean definition而不是bean实例</b>
	 * <p>通常在注册表配置期间执行，但也能在运行时注册单例。因此，注册表的实现对单例访问需要同步，无论如何它是否支持BeanFactory单例的延迟初始化。
	 *
	 * Register the given existing object as singleton in the bean registry,
	 * under the given bean name.
	 * <p>The given instance is supposed to be fully initialized; the registry
	 * will not perform any initialization callbacks (in particular, it won't
	 * call InitializingBean's {@code afterPropertiesSet} method).
	 * The given instance will not receive any destruction callbacks
	 * (like DisposableBean's {@code destroy} method) either.
	 * <p>When running within a full BeanFactory: <b>Register a bean definition
	 * instead of an existing instance if your bean is supposed to receive
	 * initialization and/or destruction callbacks.</b>
	 * <p>Typically invoked during registry configuration, but can also be used
	 * for runtime registration of singletons. As a consequence, a registry
	 * implementation should synchronize singleton access; it will have to do
	 * this anyway if it supports a BeanFactory's lazy initialization of singletons.
	 * @param beanName the name of the bean
	 * @param singletonObject the existing singleton object
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet
	 * @see org.springframework.beans.factory.DisposableBean#destroy
	 * @see org.springframework.beans.factory.support.BeanDefinitionRegistry#registerBeanDefinition
	 */
	void registerSingleton(String beanName, Object singletonObject);

	/**
	 * 返回指定名字的原始的单例对象
	 * <p>只检查已经实例化的单例，不会返回是单例bean definition，但还未实例化的。
	 * <p>这个方法的主要用途是访问手动注册的单例（见{@link #registerSingleton}）。也可以用于访问那些通过bean definition定义已经被创建出来的的单例bean（原始的格式）。
	 * <p><b>注意：</b>这个查找方法不会关注FactoryBean前缀或者别名，在获取单例实例前，你需要处理成规范的名字。
	 *
	 * Return the (raw) singleton object registered under the given name.
	 * <p>Only checks already instantiated singletons; does not return an Object
	 * for singleton bean definitions which have not been instantiated yet.
	 * <p>The main purpose of this method is to access manually registered singletons
	 * (see {@link #registerSingleton}). Can also be used to access a singleton
	 * defined by a bean definition that already been created, in a raw fashion.
	 * <p><b>NOTE:</b> This lookup method is not aware of FactoryBean prefixes or aliases.
	 * You need to resolve the canonical bean name first before obtaining the singleton instance.
	 * @param beanName the name of the bean to look for
	 * @return the registered singleton object, or {@code null} if none found
	 * @see ConfigurableListableBeanFactory#getBeanDefinition
	 */
	@Nullable
	Object getSingleton(String beanName);

	/**
	 * 检查注册表中是否包含指定名字的单例实例。
	 * <p>只检查已经实例化的单例；单例bean definition如果还未实例化则不返回{@code true}。
	 * <p>这个方法的主要目的是检查手动注册的单例（见{@link #registerSingleton}）。也能用于检查由bean definition定义的并且已经初始化的单例bean。
	 * <p>要检查bean factory中是否包含指定名字的bean definition，使用ListableBeanFactory的{@code containsBeanDefinition}。{@code containsBeanDefinition}
	 * 和{@code containsSingleton}都调用会返回指定的bean factory中是否包含一个给定名字的局部 bean实例。
	 * <p>使用BeanFactory的{@code containsBean}用于通用的检查factory中是否包含给定名字的bean（无论是手动注册的单例还是通过bean definition创建的bean），同时会检查祖先factory
	 * <p><b>注意：</b>这个查找方法不关注FactoryBean的前缀或别名，在获取单例实例前，你需要处理成规范的名字。
	 *
	 *
	 * Check if this registry contains a singleton instance with the given name.
	 * <p>Only checks already instantiated singletons; does not return {@code true}
	 * for singleton bean definitions which have not been instantiated yet.
	 * <p>The main purpose of this method is to check manually registered singletons
	 * (see {@link #registerSingleton}). Can also be used to check whether a
	 * singleton defined by a bean definition has already been created.
	 * <p>To check whether a bean factory contains a bean definition with a given name,
	 * use ListableBeanFactory's {@code containsBeanDefinition}. Calling both
	 * {@code containsBeanDefinition} and {@code containsSingleton} answers
	 * whether a specific bean factory contains a local bean instance with the given name.
	 * <p>Use BeanFactory's {@code containsBean} for general checks whether the
	 * factory knows about a bean with a given name (whether manually registered singleton
	 * instance or created by bean definition), also checking ancestor factories.
	 * <p><b>NOTE:</b> This lookup method is not aware of FactoryBean prefixes or aliases.
	 * You need to resolve the canonical bean name first before checking the singleton status.
	 * @param beanName the name of the bean to look for
	 * @return if this bean factory contains a singleton instance with the given name
	 * @see #registerSingleton
	 * @see org.springframework.beans.factory.ListableBeanFactory#containsBeanDefinition
	 * @see org.springframework.beans.factory.BeanFactory#containsBean
	 */
	boolean containsSingleton(String beanName);

	/**
	 * 返回注册表中已经注册的单例bean的名字。
	 * <p>只检查已经实例化的单例，不包含通过bean definition注册的但还未实例化的 bean。
	 * <p>这个接口的主要用途是检查手动注册的单例（见{@link #registerSingleton}），也能用于统计通过bean definition注册的并且已经被创建的单例bean。
	 *
	 * Return the names of singleton beans registered in this registry.
	 * <p>Only checks already instantiated singletons; does not return names
	 * for singleton bean definitions which have not been instantiated yet.
	 * <p>The main purpose of this method is to check manually registered singletons
	 * (see {@link #registerSingleton}). Can also be used to check which singletons
	 * defined by a bean definition have already been created.
	 * @return the list of names as a String array (never {@code null})
	 * @see #registerSingleton
	 * @see org.springframework.beans.factory.support.BeanDefinitionRegistry#getBeanDefinitionNames
	 * @see org.springframework.beans.factory.ListableBeanFactory#getBeanDefinitionNames
	 */
	String[] getSingletonNames();

	/**
	 * 返回注册表中已经注册的单例bean数量。
	 * <p>只检查已经实例化的单例，不包含通过bean definition注册的但还未实例化的 bean。
	 * <p>这个接口的主要用途是检查手动注册的单例（见{@link #registerSingleton}），也能用于统计通过bean definition注册的并且已经被创建的单例bean。
	 *
	 * Return the number of singleton beans registered in this registry.
	 * <p>Only checks already instantiated singletons; does not count
	 * singleton bean definitions which have not been instantiated yet.
	 * <p>The main purpose of this method is to check manually registered singletons
	 * (see {@link #registerSingleton}). Can also be used to count the number of
	 * singletons defined by a bean definition that have already been created.
	 * @return the number of singleton beans
	 * @see #registerSingleton
	 * @see org.springframework.beans.factory.support.BeanDefinitionRegistry#getBeanDefinitionCount
	 * @see org.springframework.beans.factory.ListableBeanFactory#getBeanDefinitionCount
	 */
	int getSingletonCount();

	/**
	 * 返回注册表所使用的互斥锁对象（用于外部合作者）
	 * Return the singleton mutex used by this registry (for external collaborators).
	 * @return the mutex object (never {@code null})
	 * @since 4.2
	 */
	Object getSingletonMutex();

}
